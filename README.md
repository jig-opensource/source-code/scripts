💡 Git Repo:
* GitLab-URL Project: https://gitlab.com/jig-opensource/source-code/scripts
* GitLab-URL File: https://gitlab.com/jig-opensource/source-code/scripts/-/blob/main/README.md


# Scripts
* Tom's Open Source Scripts
* AGPLv3, (c) tom-agplv3@jig.ch

# Lizenz / licence terms
* DE: Siehe die Lizenzbestimmund in DE weiter unten
* EN: See the licence terms hereafter

## EN: License: AGPLv3, i.e. every change must be actively published again
* (c) tom-agplv3@jig.ch
* This software is subject to the AGPLv3 licence:   
  https://choosealicense.com/licenses/agpl-3.0/

The main rights and obligations:

### Rights
- **Commercial use:**  
  The source code and derivatives may be used for commercial purposes.
- **Modifications:**  
  The source code may be modified.
- **Distribution::**  
  The source code may be distributed.

### Duties
- **The product remains open source:**  
  You may not use the software without disclosing the full source code of your own applications under the AGPL licence.  
  All source code must be disclosed, including your own product and your web-based applications.
- **Disclosure of changes:**  
  All changes must be actively disclosed, i.e. not only upon request.
- **Licence notice:**  
  If this SW is used, copyright and the AGPL licence must be clearly stated.
- **Unmodified licence:**  
  Changes must be released under AGPLv3.


## DE: Lizenz: AGPLv3, d.h. jede Änderung muss wieder aktiv publiziert werden
* (c) tom-agplv3@jig.ch
* Dieses Software untersteht der AGPLv3 Lizenz:  
  https://choosealicense.com/licenses/agpl-3.0/

Die wichtigsten Rechte und Pflichten:

### Rechte
- **Kommerzielle Nutzung:**  
  Der Quellcode und Derivate dürfen für kommerzielle Zwecke verwendet werden.
- **Änderungen:**  
  Der Quellcode darf verändert werden.
- **Verbreitung:**  
  Der Quellcode darf weitergegeben werden.

### Pflichten
- **Das Produkt bleibt Open-Source:**  
  Die Software darf nicht genützt werden, ohne den vollständigen Quellcode Ihrer eigenen Anwendungen unter der AGPL-Lizenz offenzulegen.  
  Der gesamte Quellcode muss offengelegt werden, einschließlich Ihres eigenen Produkts und Ihrer webbasierten Anwendungen.
- **Offenlegung der Änderungen:**  
  Alle Änderungen müssen aktiv offengelegt werden, d.h. nicht erst auf Anfrage.
- **Lizenzvermerk:**  
  Wird diese SW verwendet, muss deutlich auf das Urheberrecht und die AGPL-Lizenz hingewiesen werden.
- **Unveränderte Lizenz:**  
  Änderungen müssen unter AGPLv3 freigegeben werden.
