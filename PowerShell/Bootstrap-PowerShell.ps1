# Ablageort:
#	https://gitlab.com/jig-opensource/source-code/scripts/-/tree/main/PowerShell/Bootstrap-PowerShell.ps1
#
#
#
# Script, das als Bootstrap (Starter) dient
#
# Wichtig:
# 	Das Script kann per Kommandozeilen-Parameter genützt werden,
#	oder als Template kopiert und dann die $Cfg… Variablen definiert werden
# 
# 
# Das Script stellt zuerst die Grundkonfiguration fürs Paket-Mgmt sicher:
#	- dass WinGet, Chocolatey, BoxStarter installiert sind
#	- dass zugehörige PS-Module installiert sind
# 	- installiert auf Wunsch / bei Bedarf PS7
#  - aktualisiert auf Wunsch WinGet, Chocolatey, BoxStarter und die PS-Module
#
# Funktionen:
# a) Stellt nur die Grundkonfiguration fürs Paket-Mgmt sicher und stopp dann
# b) Startet nur eine optional elevated PS5 oder PS7 Shell 
# c) Lädt ein PS-Script herunter 
#		und startet es optional in einer Elevated PS5 oder PS7 Shell 
# d) Startet ein PS-Script optional in einer Elevated PS5 oder PS7 Shell 
# e) Startet ein BoxStarter-Script


# Konfiguration dieses Scripts:
# ✅ Immer: Richtet das Pkg-Mgmt mit WinGet, Chocolatey und BoxStarter ein
# ✅ Installiert PS 7
# ✅ Startet eine Shell:
# 		✅ PS 5
# 		❌ PS 7
# 		✅ Startet die Shell elevated
# ✅ Aktualisiert: WinGet, Chocolatey, BoxStarter, PS-Module und wenn vorhanden PS7


# 001, 240714, tom-agplv3@jig.ch
# 002, 240718
# 003, 240726
# 	fixes
# 004, 241128
# 	fix für das WinGet Setup / Update

# Permanent URL:
# https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/Bootstrap-PowerShell.ps1?ref_type=heads


# Starten des Scripts
# •••••••••••••••••••
#	1. Windows >> Run
#		PowerShell.exe -NoExit -ExecutionPolicy Bypass
#
#	2. Paste:
#		# 🚩 Info: 
#		# Starten eines Scripts von einer URL
#		# ➤ Alte Version:
# 			iex "& { $(irm 'https://server.com/script.ps1') }"
#
#		# ➤ Neue Version: 
#		#	  Script herunterladen, speichern und starten:
#			$PsPS = "$env:TEMP\script.ps1"; IRM -Uri 'https://server.com/script.ps1' -OutFile $PsPS; Set-ExecutionPolicy Bypass Process -Force; & $PsPS
#
#		# 🚩 Script starten
#
#		# ➤ Pkg-Mgmt einrichten und am Ende eine elevated PS 5 Shell starten
#			$PsPS="$env:TEMP\Bootstrap-PS.ps1"; IRM -Uri 'https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/Bootstrap-PowerShell.ps1?ref_type=heads' -OutFile $PsPS; Set-ExecutionPolicy Bypass Process -Force; & $PsPS -PreferScriptParams -StartPS5 -StartElevated
#
#		# ➤ Pkg-Mgmt einrichten und das BoxStarter Setup in einer elevated PS 5 Shell starten
#			$PsPS="$env:TEMP\Bootstrap-PS.ps1"; IRM -Uri 'https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/Bootstrap-PowerShell.ps1?ref_type=heads' -OutFile $PsPS; Set-ExecutionPolicy Bypass Process -Force; & $PsPS -PreferScriptParams -StartPS5 -StartElevated -StartBoxStarterScript 'https://www.akros.ch/it/Chocolatey/Install-Boxstarter-Akros-MA-Notebook-Web.ps1'
#
#
# 



# Hintergrund-Infos:
# Weil nicht alle Windows PowerShell 7 installiert haben,
# sorgt dieses Bootstrap-Script dafür, dass:
# - WinGet als Paket-Manager installiert wird, 
#	 weil für WinGet viele Standard-Applikationen verfügbar sind
# - Chocolatey als Paket-Manager installiert wird,
#	 weil wir für Choco selber relativ einfach eigene Pakete machen können
# - PowerShell 7 installiert wird


[CmdletBinding(DefaultParameterSetName = 'Auto')]
Param(
	[Parameter(ParameterSetName = 'StartScriptPath')]
	# Kann in der Script-Config definiert werden
	# oder als Param: Das Script, das gestartet werden soll
	[String]$StartScriptPath,
	
	[Parameter(ParameterSetName = 'StartScriptURL')]
	# Kann in der Script-Config definiert werden
	# oder als Param: Das Script, das heruntergeladen und gestartet werden soll
	[String]$StartScriptURL,
	
	[Parameter(ParameterSetName = 'StartScriptURL')]
	# Kann in der Script-Config definiert werden
	# oder als Param: Wenn ein Script heruntergeladen werden soll, 
	# dann der optionale Speicherpfad
	[String]$ScriptSavePath,
	
	[Parameter(ParameterSetName = 'StartBoxStarterScript')]
	# Das BoxStarter-Script, das gestartet werden soll
	[String]$StartBoxStarterScript,
	
	[Parameter(ParameterSetName = 'Auto')]
	# Kann in der Script-Config definiert werden
	# oder als Param: Einfach nur die Ziel-Shell starten
	[Switch]$JustStartTargetShell,
	
	[Parameter(ParameterSetName = 'Auto')]
	# Kann in der Script-Config definiert werden
	# oder als Param: Einfach nur die SW installieren, kein Script starten
	[Switch]$JustInstallSW,
	
	[Parameter(ParameterSetName = 'StartScriptPath')]
	[Parameter(ParameterSetName = 'StartScriptURL')]
	[Parameter(ParameterSetName = 'StartBoxStarterScript')]
	[Parameter(ParameterSetName = 'Auto')]
	# Das Script soll im Kontext von PS5 laufen
	# Oder eine PS5-Session soll gestartet werden
	[Switch]$StartPS5,

	[Parameter(ParameterSetName = 'StartScriptPath')]
	[Parameter(ParameterSetName = 'StartScriptURL')]
	[Parameter(ParameterSetName = 'StartBoxStarterScript')]
	[Parameter(ParameterSetName = 'Auto')]
	# Das Script soll im Kontext von PS7 laufen
	# Oder eine PS7-Session soll gestartet werden
	[Switch]$StartPS7,
	
	[Parameter(ParameterSetName = 'StartScriptPath')]
	[Parameter(ParameterSetName = 'StartScriptURL')]
	[Parameter(ParameterSetName = 'StartBoxStarterScript')]
	[Parameter(ParameterSetName = 'Auto')]
	# PS7 installieren?
	[Switch]$InstallPS7,
	
	[Parameter(ParameterSetName = 'StartScriptPath')]
	[Parameter(ParameterSetName = 'StartScriptURL')]
	[Parameter(ParameterSetName = 'StartBoxStarterScript')]
	[Parameter(ParameterSetName = 'Auto')]
	# Update WinGet, Choco, BoxStarter und PS7?
	[Switch]$UpdateWinGetChocoBoxStarterPS7,
	
	[Parameter(ParameterSetName = 'StartScriptPath')]
	[Parameter(ParameterSetName = 'StartScriptURL')]
	[Parameter(ParameterSetName = 'StartBoxStarterScript')]
	[Parameter(ParameterSetName = 'Auto')]
	# Update PowerShell-Module
	# ‼ Siehe: $UpdatePSModulesFalse
	[Switch]$UpdatePSModules,

	[Parameter(ParameterSetName = 'StartScriptPath')]
	[Parameter(ParameterSetName = 'StartScriptURL')]
	[Parameter(ParameterSetName = 'StartBoxStarterScript')]
	[Parameter(ParameterSetName = 'Auto')]
	# Nur als Inverser Switch zu UpdatePSModules,
	# weil -UpdatePSModules:$False per Start-Process -ArgumentList nicht übergeben werden kann
	[Switch]$UpdatePSModulesFalse,
	
	[Parameter(ParameterSetName = 'StartScriptPath')]
	[Parameter(ParameterSetName = 'StartScriptURL')]
	[Parameter(ParameterSetName = 'StartBoxStarterScript')]
	[Parameter(ParameterSetName = 'Auto')]
	# Das Script soll im Elevated-Kontext laufen
	# Oder eine PS-Session soll elevated sein
	[Switch]$StartElevated,
	
	# Die Script-Parameter überschreiben die Script $Cfg… Variablen
	[Switch]$PreferScriptParams,
	
	# Die Shell am Ende nicht schliessen
	[Switch]$NoExit
)

$Version = '004, 241128'


## Init
# Den Script-Aufruf zwischenspeichern
$Script:MyInvocation = $MyInvocation
# Allenfalls -UpdatePSModulesFalse auf -UpdatePSModules übertragen
If ($UpdatePSModulesFalse) { $UpdatePSModules = $False }
$ScriptDir = [IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Path)
$ScriptName = [IO.Path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Source)

# Init-Nachricht
Write-Host 'Initialisierung laeuft ...' -Fore Cyan -NoNewline
$Script:IsInitMsg = $True
Function Finalize_InitMsg() {
	If ($Script:IsInitMsg) {
		$Script:IsInitMsg = $False
		Write-Host ("`r{0}`r" -f (' '  * 'Initialisierung läuft …'.Length) ) -NoNewline
	}
}


# C:\Temp erstellen
New-Item -Path 'C:\Temp' -ItemType Directory -Force -EA SilentlyContinue | Out-Null


# Debug
# Finalize_InitMsg
# Write-Host ('MyCommand.Path: {0}' -f $Script:MyInvocation.MyCommand.Path)
# Write-Host ('MyCommand.Name: {0}' -f $Script:MyInvocation.MyCommand.Name)
# Write-Host ('MyCommand.BoundParameters: {0}' -f $Script:MyInvocation.MyCommand.BoundParameters)
# Write-Host ('MyCommand.UnboundArguments: {0}' -f $Script:MyInvocation.MyCommand.UnboundArguments)
# Return $MyInvocation



### Config

# Wenn True, dann werden Sessions nicht automatisch geschlossen
$Script:DebugPSSessions = $False

# Neue oder alte WinGet-Installation?
$Script:UseNewWingetSetup = $True

## Welche SW soll installiert werden?
# Wird immer installiert: WinGet, Choco
[System.Nullable[Bool]]$CfgInstallPS7 = $True
[System.Nullable[Bool]]$CfgUpdateWinGetChocoBoxStarterPS7 = $True
[System.Nullable[Bool]]$CfgUpdatePSModules = $True


## Definition der gewünschten Ziels:
# - Eine Shell?
# - Start eines Scripts?
# - Nur die Standard-SW installieren?

# Soll nur zu gewünschte Ziel-Shell gestartet werden?
[System.Nullable[Bool]]$CfgJustStartTargetShell = $Null
# Soll nur die SW installiert werden?
[System.Nullable[Bool]]$CfgJustInstallSW = $Null

# PS 5 oder 7?
[System.Nullable[Int]]$CfgStartPowerShellVersion = 5
# Elevated starten?
[System.Nullable[Bool]]$CfgStartElevated = $True


## Welches Script soll gestartet werden?
# Ein Script-Pfad  zum Script
# $CfgStartScriptPath = 'C:\Temp\Test-Script.ps1'
$CfgStartScriptPath = $Null

# Eine URL zum Script
$CfgStartScriptURL = $Null
# Bei einer URL den optionalen Speicherpfad
$CfgScriptSavePath = $Null

# Soll ein BoxStarter-Script gestartet werden?
$CfgStartBoxStarterScript = $Null



# PS-Module, die wir installieren
$InstallPSModule = @(
	@{
		# A bash inspired readline implementation for PowerShell
		# https://github.com/PowerShell/PSReadLine
		Name = 'PSReadLine'
		Splat = @{
			Force = $True
		}
	},
	@{
		# PowerShellGet
		# the package manager for PowerShell
		# https://github.com/PowerShell/PowerShellGet
		Name = 'PowerShellGet'
		Splat = @{
			Force = $True
		}
	},
	@{
		# Pester
		# https://github.com/pester/Pester
		Name = 'Pester'
		Splat = @{
			Force = $True
			SkipPublisherCheck = $True
		}
	}
)



# Post-Process Config
If (!([String]::IsNullOrWhiteSpace($CfgStartScriptPath))) {
	# Ist definiert: $CfgStartScriptPath
	# Haben wir einen absoluten / relativen Pfad?
	$HasAbsPathNet = $CfgStartScriptPath.StartsWith('\\')
	$HasAbsPathLocal = $CfgStartScriptPath.StartsWith('\')
	$HasAbsPathDrive = $CfgStartScriptPath -match '[a-z]:.*'
	$HasRelPath = $CfgStartScriptPath.StartsWith('.\')
	
	# Allenfalls den Script-Pfad ergänzen
	If($HasAbsPathNet -or $HasAbsPathLocal -or $HasAbsPathDrive) {
		# Schon OK
	} Else {
		# Wir haben einen relativen Pfad 
		# oder keinen Pfad
		$CfgStartScriptPath = Join-Path $ScriptDir $CfgStartScriptPath
	}

	# Existiert die Datei?
	If (!(Test-Path -LiteralPath $CfgStartScriptPath -PathType Leaf)) {
		Finalize_InitMsg
		Log 4 "`nWarnung, Ziel-Datei existiert nicht:" -Fore Red
		Log 5 $CfgStartScriptPath -Fore Cyan
	}
}



#Region Übersteuerung der Script-Parameter 

# Microsoft arbeitet oft schlampig, weshalb man z.B. den System-Zustand nicht immer prüfen kann,
#	z.B. kann man nicht zuverlässig erkennen, ob ein PowerShell-Modul veraltet ist
# Damit z.B. das Update der PowerShell-Module nicht in jedem Schritt durchgeführt wird,
#	werden Script-Parameter übersteuert und Funktionen deaktiviert

# Beim Start einer neuen Session werden diese Switch-Parameter auf $True gesetzt
$Script:ScriptParams_EnabledSwitches = @()
# Beim Start einer neuen Session werden diese Switch-Parameter auf $False gesetzt
$Script:ScriptParams_DisabledSwitches = @()

#Endregion Übersteuerung der Script-Parameter 



#Region Feststellen, welche Params explizit definiert wurden
$HasParam_StartScriptPath = $PSBoundParameters.ContainsKey('StartScriptPath')
$HasParam_StartScriptURL = $PSBoundParameters.ContainsKey('StartScriptURL')
$HasParam_ScriptSavePath = $PSBoundParameters.ContainsKey('ScriptSavePath')
$HasParam_StartBoxStarterScript = $PSBoundParameters.ContainsKey('StartBoxStarterScript')
$HasParam_JustStartTargetShell = $PSBoundParameters.ContainsKey('JustStartTargetShell')
$HasParam_JustInstallSW = $PSBoundParameters.ContainsKey('JustInstallSW')
$HasParam_StartPS5 = $PSBoundParameters.ContainsKey('StartPS5')
$HasParam_StartPS7 = $PSBoundParameters.ContainsKey('StartPS7')
$HasParam_InstallPS7 = $PSBoundParameters.ContainsKey('InstallPS7')
$HasParam_UpdateWinGetChocoBoxStarterPS7 = $PSBoundParameters.ContainsKey('UpdateWinGetChocoBoxStarterPS7')
$HasParam_UpdatePSModules = $PSBoundParameters.ContainsKey('UpdatePSModules')
$HasParam_UpdatePSModulesFalse = $PSBoundParameters.ContainsKey('UpdatePSModulesFalse')
$HasParam_StartElevated = $PSBoundParameters.ContainsKey('StartElevated')
#Endregion



#Region Aus den Script-Parametern und den $Cfg Werten die gewünschte Config berechnen

$Script:HasConfigErr = $False

Function Show-cfg-Error($ParamName, $VarName) {
	$Script:HasConfigErr = $True
	Finalize_InitMsg
	Log 4 'Fehler: Runtime-Config ist doppelt definiert:' -Fore Red
	Log 5 "Parameter: -$ParamName" -Fore Magenta
	Log 5 "Variable : `$$($VarName)" -Fore Magenta
	Log 6 '> Entweder der Parameter ODER die Variable muss definiert sein' -Fore Yellow
}

If ($HasParam_StartScriptPath) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or [String]::IsNullOrWhiteSpace($CfgStartScriptPath)) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgStartScriptPath = $StartScriptPath
	} Else {
		Show-cfg-Error -ParamName 'StartScriptPath' -VarName 'CfgStartScriptPath'
	}
}

If ($HasParam_StartScriptURL) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or [String]::IsNullOrWhiteSpace($CfgStartScriptURL)) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgStartScriptURL = $StartScriptURL
	} Else {
		Show-cfg-Error -ParamName 'StartScriptURL' -VarName 'CfgStartScriptURL'
	}
}

If ($HasParam_ScriptSavePath) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or [String]::IsNullOrWhiteSpace($CfgScriptSavePath)) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgScriptSavePath = $ScriptSavePath
	} Else {
		Show-cfg-Error -ParamName 'ScriptSavePath' -VarName 'CfgScriptSavePath'
	}
}

If ($HasParam_StartBoxStarterScript) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or [String]::IsNullOrWhiteSpace($CfgStartBoxStarterScript)) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgStartBoxStarterScript = $StartBoxStarterScript
	} Else {
		Show-cfg-Error -ParamName 'StartBoxStarterScript' -VarName 'CfgStartBoxStarterScript'
	}
}

If ($HasParam_JustStartTargetShell) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or $Null -eq $CfgJustStartTargetShell) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgJustStartTargetShell = $JustStartTargetShell
	} Else {
		Show-cfg-Error -ParamName 'JustStartTargetShell' -VarName 'CfgJustStartTargetShell'
	}
}

If ($HasParam_JustInstallSW) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or $Null -eq $CfgJustInstallSW) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgJustInstallSW = $JustInstallSW
	} Else {
		Show-cfg-Error -ParamName 'JustInstallSW' -VarName 'CfgJustInstallSW'
	}
}

# Wenn per Param PS5 aktiviert werden soll
If ($HasParam_StartPS5 -and $StartPS5) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or $Null -eq $CfgStartPowerShellVersion) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgStartPowerShellVersion = 5
	} Else {
		Show-cfg-Error -ParamName 'StartPS5' -VarName 'CfgStartPowerShellVersion'
	}
}

If ($HasParam_InstallPS7) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or $Null -eq $CfgInstallPS7) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgInstallPS7 = $InstallPS7
	} Else {
		Show-cfg-Error -ParamName 'InstallPS7' -VarName 'CfgInstallPS7'
	}
}

# Wenn per Param PS7 aktiviert werden soll
If ($HasParam_StartPS7 -and $StartPS7) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or $Null -eq $CfgStartPowerShellVersion) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgStartPowerShellVersion = 7
		# Sicherstellen, dass PS7 vorhanden ist
		$CfgInstallPS7 = $True
	} Else {
		Show-cfg-Error -ParamName 'StartPS7' -VarName 'CfgStartPowerShellVersion'
	}
}

If ($HasParam_UpdateWinGetChocoBoxStarterPS7) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or $Null -eq $CfgUpdateWinGetChocoBoxStarterPS7) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgUpdateWinGetChocoBoxStarterPS7 = $UpdateWinGetChocoBoxStarterPS7
	} Else {
		Show-cfg-Error -ParamName 'UpdateWinGetChocoBoxStarterPS7' -VarName 'CfgUpdateWinGetChocoBoxStarterPS7'
	}
}

If ($HasParam_UpdatePSModules -or $HasParam_UpdatePSModulesFalse) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or $Null -eq $CfgUpdatePSModules) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		If ($HasParam_UpdatePSModulesFalse) {
			# -UpdatePSModulesFalse ist definiert, hat Vorrang
			$CfgUpdatePSModules = $False
		} Else {
			$CfgUpdatePSModules = $UpdatePSModules
		}
	} Else {
		Show-cfg-Error -ParamName 'UpdatePSModules' -VarName 'CfgUpdatePSModules'
	}
}

If ($HasParam_StartElevated) {
	# Die Cfg-Variable darf nicht definiert sein
	If ($PreferScriptParams -or $Null -eq $CfgStartElevated) {
		# Die Cfg-Variable wird vom Script-Param gesetzt
		$CfgStartElevated = $StartElevated
	} Else {
		Show-cfg-Error -ParamName 'StartElevated' -VarName 'CfgStartElevated'
	}
}

# Bei einem Config-Fehler abbrechen
If ($Script:HasConfigErr) {
	Finalize_InitMsg
	Log 4 'Abbruch' -Fore Red
	# Sicherstellen, dass die Meldung sichtbar ist
	Start-Sleep -Milliseconds 5500
	Break Script
}

#Endregion Aus den Script-Parametern und den $Cfg Werten die gewünschte Config berechnen



#Region Config testen

# Ist ein Ziel definiert?
If ($CfgJustStartTargetShell -ne $True `
	-and $CfgJustInstallSW -ne $True `
	-and [String]::IsNullOrWhiteSpace($CfgStartScriptPath) `
	-and [String]::IsNullOrWhiteSpace($CfgStartScriptURL) `
	-and [String]::IsNullOrWhiteSpace($CfgStartBoxStarterScript)) {
	Finalize_InitMsg
	Log 4 'Konfig-Fehler (4EkVW24)' -Fore Red
	Log 5 'Etwas muss definiert sein:' -Fore Red
	Log 6 '- JustStartTargetShell' -Fore Magenta
	Log 6 '- JustInstallSW' -Fore Magenta
	Log 6 '- StartScriptPath' -Fore Magenta
	Log 6 '- StartScriptURL' -Fore Magenta
	Log 6 '- StartBoxStarterScript' -Fore Magenta
	Log 5 'Abbruch' -Fore Red
	Start-Sleep -Seconds 10
	Break Script
}


# Wahl der PowerShell Version i.O.?
If ($CfgStartPowerShellVersion -ne $Null -And `
	((5,7) -notcontains $CfgStartPowerShellVersion)) {
	Finalize_InitMsg
	Log 4 'Konfig-Fehler (4EkVW2F)' -Fore Red
	Log 5 '$CfgStartPowerShellVersion muss 5 oder 7 sein' -Fore Red
	Log 5 "Ist aber: $CfgStartPowerShellVersion" -Fore Red
	Log 5 'Abbruch' -Fore Red
	Start-Sleep -Seconds 10
	Break Script
}

# Ist das Ziel richtig definiert?
# - Nur SW Install
# - Nur eine PS-Session
# - Start eines Scripts
If ($CfgJustInstallSW -eq $True -Or $CfgJustStartTargetShell -eq $True) {
	# Eine URL zum Script
	$CfgStartScriptURL = $Null
	# Bei einer URL den optionalen Speicherpfad
	$CfgScriptSavePath = $Null
	
	If (![String]::IsNullOrWhiteSpace($CfgStartScriptPath)) {
		Finalize_InitMsg
		Log 4 'Konfig-Fehler (4EkRmZc)' -Fore Red
		Log 5 'Duerfen nicht gleichzeitig definiert sein:' -Fore Red
		Log 5 '- JustInstallSW' -Fore Magenta
		Log 5 '- JustStartTargetShell' -Fore Magenta
		Log 5 '- StartScriptPath' -Fore Magenta
		Log 5 'Abbruch' -Fore Red
		Start-Sleep -Seconds 10
		Break Script
	}

	If (![String]::IsNullOrWhiteSpace($CfgStartScriptURL)) {
		Finalize_InitMsg
		Log 4 'Konfig-Fehler (4EkRmdE)' -Fore Red
		Log 5 'Duerfen nicht gleichzeitig definiert sein:' -Fore Red
		Log 5 '- JustInstallSW' -Fore Magenta
		Log 5 '- JustStartTargetShell' -Fore Magenta
		Log 5 '- CfgStartScriptURL' -Fore Magenta
		Log 5 'Abbruch' -Fore Red
		Start-Sleep -Seconds 10
		Break Script
	}
	
	If (![String]::IsNullOrWhiteSpace($StartBoxStarterScript)) {
		Finalize_InitMsg
		Log 4 'Konfig-Fehler (4EkRmdE)' -Fore Red
		Log 5 'Duerfen nicht gleichzeitig definiert sein:' -Fore Red
		Log 5 '- JustInstallSW' -Fore Magenta
		Log 5 '- JustStartTargetShell' -Fore Magenta
		Log 5 '- StartBoxStarterScript' -Fore Magenta
		Log 5 'Abbruch' -Fore Red
		Start-Sleep -Seconds 10
		Break Script
	}
}


# Ist der URL-DL i.O.?
If ($Null -eq $CfgStartScriptURL -And `
	[String]::IsNullOrWhiteSpace($CfgScriptSavePath) -eq $False) {
		Finalize_InitMsg
		Log 4 'Konfig-Fehler (4EkRmdO)' -Fore Red
		Log 5 'Wenn CfgStartScriptURL $Null ist,' -Fore Magenta
		Log 5 'dann darf nicht definiert sein:' -Fore Magenta
		Log 5 '- ScriptSavePath' -Fore Magenta
		Log 5 'Abbruch' -Fore Red
		Start-Sleep -Seconds 10
		Break Script
}


# Wie viele Scripts sollen gestartet werden?
$ScriptVars = @($CfgStartScriptPath, $CfgStartScriptURL, $CfgStartBoxStarterScript)
$NoOf_ScriptsToStart = $ScriptVars | ? { -not [string]::IsNullOrEmpty($_) } | Measure | Select -ExpandProperty Count

If ($NoOf_ScriptsToStart -gt 1) {
	Finalize_InitMsg
	Log 4 'Konfig-Fehler (4EkS9GJ)' -Fore Red
	Log 5 'Von diesen darf nur eines definiert sein:' -Fore Magenta
	Log 5 '- CfgStartScriptPath' -Fore Magenta
	Log 5 '- CfgStartScriptURL' -Fore Magenta
	Log 5 '- CfgStartBoxStarterScript' -Fore Magenta
	Log 5 'Abbruch' -Fore Red
	Start-Sleep -Seconds 10
	Break Script
}


#Endregion Config testen


#Region Funcs

#Region Tools

# 200806
Function Has-Value($Data) {
	If ($Data -eq $null) { Return $False }
	Switch ($Data.GetType().Name) {
		'String' {
			If ([String]::IsNullOrEmpty($Data)) { Return $False } 
			Else { Return $True }
		}
		Default {
			Return $True
		}
	}
}

Function Is-Empty($Data) {
	Return !(Has-Value $Data)
}

Function Is-Verbose() {
	$VerbosePreference -eq 'Continue'
}

Function Is-WhatIf() {
	$WhatIfPreference
}


# Prüft, ob vom Windows Update ein Reboot hängig ist
Function Is-WindowsUpdate-RebootPending() {
	$RebootPending = $false

	# Überprüfen von "RebootRequired" Schlüssel
	If (Test-Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Component Based Servicing\RebootPending') {
		$RebootPending = $true
	}

	# Überprüfen von "RebootRequired" Schlüssel unter den Windows Update Einstellungen
	If (Test-Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\RebootRequired') {
		$RebootPending = $true
	}

	# Überprüfen von "PendingFileRenameOperations" Schlüssel
	$PendingFileRenameOperations = Get-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager' -Name 'PendingFileRenameOperations' -ErrorAction SilentlyContinue
	If ($PendingFileRenameOperations) {
		$RebootPending = $true
	}
	
	Return $RebootPending
}


#Region Tom-Tools: Log

# Log
# Prüft, ob $Script:LogColors definiert ist und nützt dann dieses zur Farbgebung
# $Script:LogColors =@('Cyan', 'Yellow')
#
# !Ex
# 	Log 1 'Test1' -NoNewline; Log 1 'Test2' -Append
# 	Log 1 'Test1' -NoNewline; Log -Message 'Test2' -Append
#
# 0: Thema - 1: Kapitel - 2: OK - 3: Error
# 200604 175016
# 200805 103305
# 	Neu: Optional BackgroundColor
# 211129 110213
# 	Fix -ClrToEol zusammen mit -ReplaceLine
# 220102 172537
# 	Fix Für PS7: Kommt irgendwie nicht mit NoNewLine zurecht
# 220118 095129
# 	ValueFromPipeline richtig angewendet
# 220123 222224
# 	Fixed: -ReplaceLine handling
# 220926 152200
# Neu: [Switch]IfVerbose
#	Wird nur ausgegeben, wenn -Verbose aktiv ist
# 221116 191858
#  Wenn Get-Host keine echte Console hat (und z.B. BackgroundColor $null ist)
#  dann arbeiten wir mit default Werten
# 230409 000309
# 	Wenn -Indent < 0: Keine Ausgabe
# 240718 215001
#	Bereinigt
$Script:LogColors = @('Green', 'Yellow', 'Cyan', 'White', 'Red')
Function Log() {
	Param (
		[Parameter(Position = 0)]
		[Int]$Indent,

		[Parameter(Position = 1, ValueFromPipeline, ValueFromPipelineByPropertyName)]
		[String]$Message = '',

		[Parameter(Position = 2)]
		[ConsoleColor]$ForegroundColor,

		# Vor der Nachricht eine Leerzeile
		[Parameter(Position = 3)]
		[Switch]$NewLineBefore,

		# True: Die aktuelle Zeile wird gelöscht und neu geschrieben
		[Parameter(Position = 4)]
		[Switch]$ReplaceLine = $false,

		# True: Am Ende keinen Zeilenumbruch
		[Parameter(Position = 5)]
		[Switch]$NoNewline = $false,

		# Append, also kein Präfix mit Ident
		[Parameter(Position = 6)]
		[Switch]$Append = $false,

		# Löscht die Zeile bis zum Zeilenende
		[Parameter(Position = 7)]
		[Switch]$ClrToEol = $false,

		# Ausgabe erfolgt nur, wenn Verbose aktiv ist
		[Switch]$IfVerbose,

		[Parameter(Position = 8)]
		[ConsoleColor]$BackgroundColor
	)

	Begin {
		$PSBoundParametersCopy = $PSBoundParameters

		## Init der Get-Host Config Daten
		# $Script:LogDefaultBackgroundColor
		If ($null -eq $Script:LogDefaultBackgroundColor) {
			If ($null -eq (Get-Host).UI.RawUI.BackgroundColor) {
				$Script:LogDefaultBackgroundColor = [ConsoleColor]::Black
			} Else {
				$Script:LogDefaultBackgroundColor = (Get-Host).UI.RawUI.BackgroundColor
			}
		}
		
		# $Script:LogMaxWindowSizeWidth
		If ($null -eq $Script:LogMaxWindowSizeWidth) {
			If ($null -eq (Get-Host).UI.RawUI.MaxWindowSize) {
				$Script:LogMaxWindowSizeWidth = 132
			} Else {
				$Script:LogMaxWindowSizeWidth = (Get-Host).UI.RawUI.BackgroundColor
			}
		}
		
		# Fix für PS7
		If ($null -eq $Script:IsPS7) { $Script:IsPS7 = ($PSVersionTable).PSVersion.Major -eq 7 }
		If ($null -eq $Script:DefaultBackgroundColor) { $Script:DefaultBackgroundColor = (Get-Host).UI.RawUI.BackgroundColor }

		If ($Indent -eq $null) { $Indent = 0 }
		If ($BackgroundColor -eq $null) { $BackgroundColor = $Script:DefaultBackgroundColor }

		$WriteHostArgs = @{ }
		If ($ForegroundColor -eq $null) {
			If ($null -ne $Script:LogColors -and $Indent -le $Script:LogColors.Count -and $null -ne $Script:LogColors[$Indent]) {
				Try {
					$ForegroundColor = $Script:LogColors[$Indent]
				}
				Catch {
					Write-Host "Ungueltige Farbe: $($Script:LogColors[$Indent])" -ForegroundColor Red
				}
			}
			If ($null -eq $ForegroundColor) {
				$ForegroundColor = [ConsoleColor]::White
			}
		}
		If ($ForegroundColor) {
			$WriteHostArgs += @{ ForegroundColor = $ForegroundColor }
		}
		$WriteHostArgs += @{ BackgroundColor = $BackgroundColor }

		If ($NoNewline) {
			$WriteHostArgs += @{ NoNewline = $true }
		}
	}

	Process {
		$PSBoundParametersCopy = $PSBoundParameters
		
		If ($Script:LogDisabled -eq $true) { Return }
		# Wenn Verbose gewünscht aber nicht aktiv, dann sind wir fertig
		If ($IfVerbose -and (Is-Verbose) -eq $False) { Return }

		# Wenn -Indent < 0, sind wir fertig
		If ($Indent -lt 0) { Return }
		

		If ([String]::IsNullOrEmpty($Message)) { $Message = '' }

		If ($NewLineBefore) { Write-Host '' }
		If ($null -eq $BackgroundColor) { $BackgroundColor = $Script:LogDefaultBackgroundColor }

		If ($Append) {
			$Msg = $Message
			If ($ClrToEol) {
				$Width = $Script:LogMaxWindowSizeWidth
				If ($Msg.Length -lt $Width) {
					$Spaces = $Width - $Msg.Length
					$Msg = "$Msg$(' ' * $Spaces)"
				}
			}
		}
		Else {
			Switch ($Indent) {
				0 {
					$Msg = "* $Message"
					If ($NoNewline -and $ClrToEol) {
						$Width = $Script:LogMaxWindowSizeWidth
						If ($Msg.Length -lt $Width) {
							$Spaces = $Width - $Msg.Length
							$Msg = "$Msg$(' ' * $Spaces)"
						}
					}
					If (!($ReplaceLine)) {
						$Msg = "`n$Msg"
					}
				}
				Default {
					$Msg = $(' ' * ($Indent * 2) + $Message)
					If ($NoNewline -and $ClrToEol) {
						# Rest der Zeile mit Leerzeichen überschreiben
						$Width = $Script:LogMaxWindowSizeWidth
						If ($Msg.Length -lt $Width) {
							$Spaces = $Width - $Msg.Length
							$Msg = "$Msg$(' ' * $Spaces)"
						}
					}
				}
			}
		}

		If ($ReplaceLine) { $Msg = "`r$Msg" }
		Write-Host $Msg @WriteHostArgs
		# Fix für PS7: Den Cursor ans Ende Positionieren
		If ($Script:IsPS7 -and $NoNewline) {
			$CursorPosition = (Get-Host).UI.RawUI.CursorPosition
			$CursorPosition.X = $Msg.Length
			(Get-Host).UI.RawUI.CursorPosition = $CursorPosition
		}

		# if (!([String]::IsNullOrEmpty($LogFile))) {
		# 	"$([DateTime]::Now.ToShortDateString()) $([DateTime]::Now.ToLongTimeString())   $Message" | Out-File $LogFile -Append
		# }
	}
}

#Endregion Tom-Tools: Log


# True, wenn Elevated
# 220813
Function Is-Elevated() {
	([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')
}

Function Assert-Is-Elevated() {
	Param(
		[Parameter(Position=0, Mandatory)]
		[String]$ID
	)
	If (Is-Elevated) { Return }
	Log 4 ('Fehler!, das Script bräuchte eine elevated Session! ({0})' -f $ID) -Fore Red
}


# Liest vom System die aktuellen Systempfade und setzte $Env:Path
Function Refresh-Env-Path() {
	# Den neuen System-Pfad in die aktuelle Shell lesen
	$UserPaths = [Environment]::GetEnvironmentVariable('PATH', 'User')
	$SysPaths = [Environment]::GetEnvironmentVariable('PATH', 'Machine')

	# Pfade sammeln
	$AllPath = $Env:PATH + ";$UserPaths;$SysPaths"

	# Alle Duplikate entfernen
	$AllPathUnique = ($AllPath -split ';' | ? { $_ -ne '' } | Sort-Object -Unique) -Join ';'
	[Environment]::SetEnvironmentVariable('PATH', $AllPathUnique, [EnvironmentVariableTarget]::Process)
}


# Liest vom System die aktuellen PSModulePath und setzte $Env:PSModulePath
Function Refresh-PSModulePath() {
	# Den neuen System-Pfad in die aktuelle Shell lesen
	$SysPaths = [Environment]::GetEnvironmentVariable('PSModulePath', 'Machine')

	# Pfade sammeln
	$AllPath = $Env:PSModulePath + ";$SysPaths"

	# Alle Duplikate entfernen
	$AllPathUnique = ($AllPath -split ';' | ? { $_ -ne '' } | Sort-Object -Unique) -Join ';'
	[Environment]::SetEnvironmentVariable('PSModulePath', $AllPathUnique, [EnvironmentVariableTarget]::Process)
}


# Liest die SN aus / Serie-Nr
# 240908
Function Get-BIOS-SN() {
	$HasWMIC = @(Get-Command wmic.exe -EA SilentlyContinue).Count -ge 1
	If ($HasWMIC) {
		[PSCustomObject][Ordered] @{
			# Liest die SN und Product-No
			SN = ( wmic bios get serialnumber | ? { $_ } | select -Skip 1 -First 1 )
			# Die 4stelligt Product-No
			ProdNoShort = ( (wmic csproduct get name)[2][0..3] -join '' )
			# Die ganze Product-No
			ProdNoLong = ( wmic csproduct get name | ? { $_ } | select -Skip 1 -First 1 ) 
		}
	} Else {
		[PSCustomObject][Ordered] @{
			SN = '(wmic.exe fehlt)'
			ProdNoShort = $Null
			ProdNoLong = $Null
		}
	}
}


# Wichtig, weil das Script nicht in utf8 gespeichert werden kann:
# 	wingt hat für neue Versionen den Spaltennamen 'Verfügbar', 
# der im PS-Code wegen dem SOnderzeichen nicht adressiert werden kann
# Lösung
#	Wir suchen nach dem Property 'Verf*bar'
#	Und ergänzen ein Alias Verfuegbar zum gefundenen Property
# 
# Sucht in $Objects nach einem Property mit einem Muster
# und ergänzt dann ein Alias-Property zu diesem Muster
Function Add-Alias-Property {
	Param (
		[Object[]]$Objects,
		[String]$Pattern,
		[String]$AliasName
	)

	ForEach ($obj in $Objects) {
		$matchingProperty = $obj.PSObject.Properties | ? { $_.Name -like $Pattern }
		If ($matchingProperty) {
			$propertyName = $matchingProperty.Name
			$obj | Add-Member -MemberType AliasProperty -Name $AliasName -Value $propertyName
		}
	}
}


# Deaktiviert beim nächsten Start einer PS-Shell das Flag UpdatePSModules
Function Disable-ScriptStart-UpdatePSModules() {
	$Script:ScriptParams_EnabledSwitches += 'PreferScriptParams'
	$Script:ScriptParams_EnabledSwitches += 'UpdatePSModulesFalse'
	$Script:ScriptParams_DisabledSwitches += 'UpdatePSModules'
}


# Ersetzt die Dateinamen-Erweiterung
Function Replace-FileExt($FileName, $NewFileExt) {
	[IO.Path]::Combine( `
		[IO.Path]::GetDirectoryName($FileName), `
		[IO.path]::GetFileNameWithoutExtension($FileName) + $NewFileExt
	)
}

# 240713
Function Get-TempFile([String]$Ext) {
	If ([String]::IsNullOrEmpty($Ext)) {
		Return New-TemporaryFile
	} Else {
		Return Replace-FileExt (New-TemporaryFile) $Ext
	}
}

# Deinstalliert alte PowerShell-Module
# !Q https://www.ondfisk.dk/removing-duplicate-powershell-modules/
Function Remove-Old-PSModules() {
	# Author: Luke Murray (Luke.Geek.NZ)
	# 0.1
	# Basic function to remove old PowerShell modules which are installed
	$AllLatest = Get-InstalledModule *>$null
	ForEach ($ThisLatest in $AllLatest) {
		$AllModules = Get-InstalledModule $ThisLatest.Name -AllVersions *>$null
		$AllModules | ? {$_.Version -ne $ThisLatest.Version} | % {
			$ObsoleteModule = $_
			Write-Verbose "-  $($ThisLatest.Name): $ObsoleteModule.Version"
			$ObsoleteModule | Uninstall-Module -EA SilentlyContinue *>$null
		}
	}
}

Function Print-Betriebsmode($Betriebsmode) {
	Log 0 'Betriebsmode:' -NewLineBefore
	$Betriebsmode | % {
		Log 1 $_
	}
}

Function Print-Schritt([Int]$Step, [Int]$Steps, [String]$Info) {
	Log 0 ('Schritt {0}/{1}' -f $Step, $Steps) -NewLineBefore
	$Info | % {
		Log 1 $_
	}
}

#Endregion Tools


#Region WinGet Funcs

# !9 Wichtige Infos zu Winget

# Params
#	--disable-interactivity
# 	--accept-source-agreements --accept-package-agreements


# WinGet PS Modul
#	https://github.com/microsoft/winget-cli
#
#
# VORSICHT!, das M$ Modul hat zwar schon 79 Releases, 
# 	hat aber immer noch Bugs wie in einer frühen Beta: 
#	Oft erkennt das PS_Modul nicht einmal die installierte WinGet Applikation
# 
# » Lösung:
# 		Die Basis-Funktionen habe ich selber rund um WinGet.exe gebaut
# 
# 
# Alle CmdLets abrufen
# WinGet PS Modul: Alle CmdLets abrufen
# Import-Module -Name 'Microsoft.Winget.Client'
# (Get-Module microsoft.winget.client).ExportedCommands
#
# Details anzeigen
#	winget show Microsoft.PowerShell
#
# Alle Versionen anzeigen
#	winget show Microsoft.PowerShell --versions
#
#
#
# List installed applications
# 	winget List Microsoft.PowerShell --accept-source-agreements
# 	Get-WinGetPackage Microsoft.PowerShell
#
# Search applications available for installation
# 	winget Search Microsoft.PowerShell --accept-source-agreements 
# 	Find-WinGetPackage Microsoft.PowerShell
#
# Installiert den neusten Release (nicht das Preview)
#	winget Install Microsoft.PowerShell --disable-interactivity --accept-source-agreements --accept-package-agreements
# 
# Exakte Version installieren
# 	» Die Versionsinfo muss genau stimmen!
# 
# 	winget Install Microsoft.PowerShell --disable-interactivity --accept-source-agreements --accept-package-agreements --version 7.4.2.0

# 	$Res = Install-WinGetPackage Microsoft.PowerShell
# 
# Weitere Befehle
# 	Update-WinGetPackage Microsoft.PowerShell
# 	Uninstall-WinGetPackage Microsoft.PowerShell
# 	Export-WinGetPackage Microsoft.PowerShell


# $True, wenn Winget installiert ist
Function Is-WinGet-Installed() {
	Return @(Get-Command WinGet.exe -EA SilentlyContinue).Count -ge 1
}


Function Get-WinGet-Version() {
	If (Is-WinGet-Installed) {
		$WinGetVersion = winget --version
		$Version = $WinGetVersion -Replace '[^0-9.]', ''
		$oVersion = [Version]$Version
		Return $oVersion
	} Else {
		Return [Version]$Null
	}
}


# Eine WinGet Version kleiner als 1.8 macht Probleme und findet nicht alle Pakete!
Function Is-WinGet-Version-OK() {
	$oWinGetVersion = Get-WinGet-Version
	Return $oWinGetVersion -ge [Version]'1.8'
}


# !Q https://stackoverflow.com/a/74297741/4795779
# 
# Note:
#  * Accepts input only via the pipeline, either line by line, 
#    or as a single, multi-line string.
#  * The input is assumed to have a header line whose column names
#    mark the start of each field
#    * Column names are assumed to be *single words* (must not contain spaces).
#  * The header line is assumed to be followed by a separator line
#    (its format doesn't matter).
Function ConvertFrom-FixedColumnTable {
	[CmdletBinding()]
	Param(
		[Parameter(ValueFromPipeline)] [string] $InputObject
	)
  
	Begin {
		Set-StrictMode -Version 1
		$lineNdx = 0
	}
  
  Process {
		$lines = 
			if ($InputObject.Contains("`n")) { $InputObject.TrimEnd("`r", "`n") -split '\r?\n' }
			else { $InputObject }
		ForEach ($line in $lines) {
			++$lineNdx
			If ($lineNdx -eq 1) { 
			  $headerLine = $line 
			}
			ElseIf ($lineNdx -eq 2) {
				# separator line
				# Get the indices where the fields start.
				$fieldStartIndices = [regex]::Matches($headerLine, '\b\S').Index
				# Calculate the field lengths.
				$fieldLengths = foreach ($i in 1..($fieldStartIndices.Count-1)) {
					$fieldStartIndices[$i] - $fieldStartIndices[$i - 1] - 1
				}
				# Get the column names
				$colNames = ForEach ($i in 0..($fieldStartIndices.Count-1)) {
					If ($i -eq $fieldStartIndices.Count-1) {
						$headerLine.Substring($fieldStartIndices[$i]).Trim()
					} Else {
						$headerLine.Substring($fieldStartIndices[$i], $fieldLengths[$i]).Trim()
					}
				} 
			} Else {
				# data line
				$oht = [ordered] @{} # ordered helper hashtable for object constructions.
				$i = 0
				ForEach ($colName in $colNames) {
					$oht[$colName] = 
					If ($fieldStartIndices[$i] -lt $line.Length) {
						If ($fieldLengths[$i] -and $fieldStartIndices[$i] + $fieldLengths[$i] -le $line.Length) {
							$line.Substring($fieldStartIndices[$i], $fieldLengths[$i]).Trim()
						} Else {
							$line.Substring($fieldStartIndices[$i]).Trim()
						}
					}
					++$i
				}
				# Convert the helper hashable to an object and output it.
				[PSCustomObject] $oht
			}
		}
	}
}


# Bereinigt die Winget-Ausgabe
# https://stackoverflow.com/a/77210265/4795779
Function Winget-OutClean () {
	[CmdletBinding()]
	Param (
		[Parameter(ValueFromPipeline)]
		[String[]]$lines
	)
	If ($input.Count -gt 0) { $lines = $PSBoundParameters['Value'] = $input }
	$bInPreamble = $true
	ForEach ($line in $lines) {
		If ($bInPreamble){
			If ($line -like "Name*") {
				$bInPreamble = $false
			}
		}
		If (-not $bInPreamble) {
			Write-Output $line
		}
	}
}


# Get-WinGet-Pkg-Info 7zip.7zip -ExactMatch
# 
# Name      : 7-Zip 22.01 (x64)
# ID        : 7zip.7zip
# Version   : 22.01
# Verfuegbar : 24.07
# Quelle    : winget
Function Get-WinGet-Pkg-Info([String]$PkgID, [Switch]$ExactMatch) {
	If ($ExactMatch) {
		# Param ungültig: --disable-interactivity
		$Res = @(winget list --id $PkgID --exact --accept-source-agreements | Winget-OutClean | ConvertFrom-FixedColumnTable)
	} Else {
		# Param ungültig: --disable-interactivity
		$Res = @(winget list --id $PkgID --accept-source-agreements | Winget-OutClean | ConvertFrom-FixedColumnTable)
	}
	
	# Wichtig, weil das Script nicht als UTF8 BOM gespeichert werden kann:
	# Das Proeprty 'Verfügbar' suchen und für es das Alias Verfuegbar erzeugen
	# » So kann das Proeprty im Code adressiert werden :-)
	Add-Alias-Property -Objects $Res -Pattern 'verf*gbar' -AliasName 'Verfuegbar'
	Return $Res
}


# Liefert True, wenn ein WinGet Paket mit der ID installiert ist
Function Is-WinGet-Pkg-Installed([String]$PkgID, [Switch]$ExactMatch) {
	# Nur die exakte Paket-ID interessiert
	# @(Get-WinGetPackage $PkgID | ? Id -eq $PkgID).Count -ge 1
	$Res = @(Get-WinGet-Pkg-Info -PkgID $PkgID -ExactMatch:$ExactMatch)
	Return $Res.Count -ge 1
}


# Liefert True, wenn ein WinGet Paket mit der ID veraltet ist
Function Has-WinGet-Pkg-Update-Available([String]$PkgID, [Switch]$ExactMatch) {
	# Nur die exakte Paket-ID interessiert
	# @(Get-WinGetPackage $PkgID | ? Id -eq $PkgID | ? IsUpdateAvailable).Count -ge 1
	
	$Res = @(Get-WinGet-Pkg-Info -PkgID $PkgID -ExactMatch:$ExactMatch)
	
	Switch ($Res.Count) {
		0 {
			# Nicht installiert
			Return $False
		}
		1 {
			# 1 installiert
			If ((Has-Value $Res.Version) -and (Has-Value $Res.Verfuegbar)) {
				Try {
					$VVersion = [Version]$Res.Version
					$VVerfuegbar = [Version]$Res.Verfuegbar
					Return $VVerfuegbar -gt $VVersion
				} Catch {}
				# Keine Versions-Info gefunden
				Return $False
			}
			Return $False
		}
		Default {
			# Mehrere gefunden
			Log 4 'Fehler in Has-WinGet-Pkg-Update-Available(): (4ElSEyV)' -Fore Red
			Log 5 "Fuer $PkgID wurden $($Res.Count) Pakete gefunden"
			Log 5 'Abbruch'
			Return $False
		}
	}
}

Function Is-WinGet-Pkg-Outdated($PkgID, [Switch]$ExactMatch) {
	Return Has-WinGet-Pkg-Update-Available -PkgID $PkgID -ExactMatch:$ExactMatch
}


# 241128 Neues Setup via https://winget.run/ und AppxPackage
Function Install-WinGet-Pkg-by-AppxPackage([Int]$Indent = 1) {
	# AppxPackage herunterladen
	
	## Config
	$OutFile_winget_msixbundle = Get-TempFile -Ext '.msixbundle'
	# Log 4 $OutFile_winget_msixbundle -ForegroundColor Red

	# Versuch, das neuste AppxPackage herunterzuladen
	$Url1 = 'https://aka.ms/getwinget'
	# Fallback
	$Url2 = 'https://www.akros.ch/it/Chocolatey/Deploy/WinGet/set/winget.msixbundle'

	## Download
	$DLOK = $False
	
	# Versuch #1
	Try {
		Log $Indent "`nDownload: winget.msixbundle (Versuch #1)`n" -Fore Yellow
		
		$ProgressPreference = 'SilentlyContinue'
		Invoke-WebRequest $Url1 -OutFile $OutFile_winget_msixbundle -EA Stop
		$ProgressPreference = 'Continue'
		
		$DLOK = $True
	} Catch { }
	
	# Versuch #2
	If ($DLOK -eq $False) {
		Try {
			Log $Indent "`nDownload: winget.msixbundle (Versuch #2)`n" -Fore Yellow
			
			$ProgressPreference = 'SilentlyContinue'
			Invoke-WebRequest $Url2 -OutFile $OutFile_winget_msixbundle -EA Stop
			$ProgressPreference = 'Continue'
			
			$DLOK = $True
		} Catch { }
	}
	
	# Installation
	If ($DLOK) {
		Log $Indent "`nInstalliere: winget.msixbundle`n" -Fore Yellow
		Add-AppxPackage -Path $OutFile_winget_msixbundle
	}
}


# Startet die Winget Paket-Installation
# Wiederholt die Installation, so lange bis es klappt
Function Install-WinGet-Pkg_([String]$PkgID, [Switch]$ExactMatch, [Int]$Indent = 1) {
	$InstallCnt = 0
	
	# Ist das Paket schon installiert?
	$IsInstalled = Is-WinGet-Pkg-Installed -PkgID $PkgID -ExactMatch:$ExactMatch
	
	# So lange die Installation wiederholen, bis sie klappt
	While ($IsInstalled -eq $False) {
		$InstallCnt++
		Switch ($InstallCnt) {
			1 {
				Log $Indent "`nInstalliere: $PkgID`n" -Fore Gray
			}
			Default {
				Log $Indent "  Versuch Nr. $InstallCnt, warte vorher 10s" -Fore Magenta
				# 10s warten
				Start-Sleep -Milliseconds 10000
			}
		}
		
		# $Res = Install-WinGetPackage -Id $PkgID -Mode Silent -Force -Scope System
		If ($ExactMatch) {
			If ($InstallCnt -eq 1) {
				# Param ungültig: --disable-interactivity
				$Res = @(winget install --id $PkgID --exact --accept-source-agreements --accept-package-agreements --scope machine | Winget-OutClean | ConvertFrom-FixedColumnTable)
			} Else {
				# Wenn die erste Installation nicht klappte,
				# hat WinGet wohl wieder einen Fehler
				$Res = winget install --id $PkgID --exact --accept-source-agreements --accept-package-agreements --scope machine
				Log $Indent "Resultat WinGet: $Res" -Fore Cyan
			}
		} Else {
			If ($InstallCnt -eq 1) {
				# Param ungültig: --disable-interactivity
				$Res = @(winget install --id $PkgID --accept-source-agreements --accept-package-agreements --scope machine | Winget-OutClean | ConvertFrom-FixedColumnTable)
			} Else {
				# Wenn die erste Installation nicht klappte,
				# hat WinGet wohl wieder einen Fehler
				$Res = winget install --id $PkgID --accept-source-agreements --accept-package-agreements --scope machine
				Log $Indent "Resultat WinGet: $Res" -Fore Cyan
			}
		}

		# Ein paar Sekunden warten und dann testen
		Start-Sleep -Milliseconds 2500
		$IsInstalled = Is-WinGet-Pkg-Installed -PkgID $PkgID -ExactMatch:$ExactMatch
		
		# Installation fehlgeschlagen?
		If ($IsInstalled -eq $False) {
			Log $Indent "  Installation fehlgeschlagen" -Fore Red
			$Res | Out-String | % {
				Log (1+$Indent) $_ -Fore White
			}
		}
		
	}
}


# Installiert ein Paket
# Wenn InstallUpdate, dann wird ein vorhandenes, veraltetes Paket aktualisiert
Function Install-WinGet-Pkg([String]$PkgID, [Switch]$ExactMatch, [Switch]$InstallUpdate, [Int]$Indent = 1) {
	If (Is-WinGet-Pkg-Installed -PkgID $PkgID -ExactMatch:$ExactMatch) {
		If ($InstallUpdate) {
			If (Has-WinGet-Pkg-Update-Available -PkgID $PkgID -ExactMatch:$ExactMatch) {
				Log $Indent "`nAktualisiere: $PkgID`n" -Fore Yellow
				# $Res = Update-WinGetPackage -Id $PkgID -Mode Silent -Force -Scope System
				If ($ExactMatch) {
					# Param ungültig: --disable-interactivity
					$Res = @(winget upgrade --id $PkgID --exact --accept-source-agreements --accept-package-agreements --scope machine | Winget-OutClean | ConvertFrom-FixedColumnTable)
				} Else {
					# Param ungültig: --disable-interactivity
					$Res = @(winget upgrade --id $PkgID --accept-source-agreements --accept-package-agreements --scope machine | Winget-OutClean | ConvertFrom-FixedColumnTable)
				}
			}
		}
	} Else {
		Log $Indent "`nInstalliere WinGet Paket: $PkgID`n" -Fore Yellow
		Install-WinGet-Pkg_ -PkgID $PkgID -ExactMatch:$ExactMatch
	}
}


# Wenn ein Update vorhanden ist, wird es installiert
Function Update-WinGet-Pkg([String]$PkgID, [Switch]$ExactMatch, [Switch]$InstallIfMissing, [Int]$Indent = 1) {
	If (Is-WinGet-Pkg-Installed -PkgID $PkgID -ExactMatch:$ExactMatch) {
		If (Has-WinGet-Pkg-Update-Available -PkgID $PkgID -ExactMatch:$ExactMatch) {
			Log $Indent "`nAktualisiere: $PkgID`n" -Fore Yellow
			# $Res = Update-WinGetPackage -Id $PkgID -Mode Silent -Force -Scope System
			If ($ExactMatch) {
				# Param ungültig: --disable-interactivity
				$Res = @(winget upgrade --id $PkgID --exact --accept-source-agreements --accept-package-agreements | Winget-OutClean | ConvertFrom-FixedColumnTable)
			} Else {
				# Param ungültig: --disable-interactivity
				$Res = @(winget upgrade --id $PkgID --accept-source-agreements --accept-package-agreements | Winget-OutClean | ConvertFrom-FixedColumnTable)
			}
		}
	} Else {
		If ($InstallIfMissing) {
			# $Res = Install-WinGetPackage -Id $PkgID -Mode Silent -Force -Scope System
			Install-WinGet-Pkg_ -PkgID $PkgID -ExactMatch:$ExactMatch
		}
	}
}


# Liefert alle Versionen eines WinGet Pakets mit der ID
# Die Versionen sind absteigend sortiert
Function Get-WinGet-Pkg-Versionen([String]$PkgID, [Switch]$ExactMatch) {
	# Nur die exakte Paket-ID interessiert
	# $Versionen = Get-WinGetPackage $PkgID | ? Id -eq $PkgID | select -ExpandProperty AvailableVersions

	# Alle Versionen eines Pakets suchen
	If ($ExactMatch) {
		# Param ungültig: --disable-interactivity
		$Res = @(winget search --id $PkgID --exact --accept-source-agreements | Winget-OutClean | ConvertFrom-FixedColumnTable)
	} Else {
		# Param ungültig: --disable-interactivity
		$Res = @(winget search --id $PkgID --accept-source-agreements | Winget-OutClean | ConvertFrom-FixedColumnTable)
	}
	
	$oVersionen = $Res | Select -ExpandProperty Version | % { 
		New-Object PSCustomObject -Property @{ 
			Version = $_
			oVersion = [Version]::Parse($_)
		}
	}
	Return $oVersionen | Sort oVersion -Descending
}


# Sucht für eine gekürzte Versionsnummer 
# die exakte Versionsnummer
# Will man Version 7.4.2 installieren, muss man WinGet beauftragen, 7.4.2.0 zu installieren
# Eg:		Get-WinGet-Pkg-Exact-Version Microsoft.PowerShell '7.4.2'
# Gibt: 	'7.4.2.0'
Function Get-WinGet-Pkg-Exact-Version() {
	Param(
		[Parameter(Position=0, Mandatory)]
		[String]$PkgID,
		[Parameter(Position=1, Mandatory)]
		[String]$VersStr,
		[Switch]$ExactMatch
	)
	$oVersionen = Get-WinGet-Pkg-Versionen $PkgID -ExactMatch:$ExactMatch
	$oVersionen | ? Version -like "$VersStr*" | Select -ExpandProperty Version
}

#Endregion WinGet Funcs


#Region BoxStarter Funcs

Function Delete-BoxStarter-DesktopIcon() {
	Get-ChildItem -LiteralPath ([System.Environment]::GetFolderPath('CommonDesktopDirectory')) -Filter 'Boxstarter*.lnk' | Remove-Item -Force -EA SilentlyContinue
}

Function Is-BoxStarter-Installed() {
	# Allenfalls die neu installierten Module erfassen
	Refresh-PSModulePath
	$IsBoxStarterInstalled = @(Get-Module -ListAvailable -Name 'Boxstarter.Chocolatey').Count -ge 1
	Return $IsBoxStarterInstalled
}

# Wenn BoxStarter installiert ist, die Module laden
Function Load-BoxStarter-Module() {
	If (Is-BoxStarter-Installed) {
		# !M https://boxstarter.org/usingboxstarter#importing-the-boxstarter-modules
		Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process -Force
		Import-Module Boxstarter.Chocolatey
	}
}

#Endregion BoxStarter Funcs


#Region Chocolatey Funcs

Function Is-Choco-Pkg-Installed([String]$PkgID) {
	$Res = @(choco list --limitoutput --exact $PkgID)
	# Boxstarter|3.0.3
	Return $Res.Count -ge 1
}

Function Is-Choco-Pkg-Outdated([String]$PkgID) {
	$Res = @(choco outdated --limitoutput --exact $PkgID)
	# Boxstarter|3.0.3
	Return $Res.Count -ge 1
}


# Installiert ein Paket
# Wenn InstallUpdate, dann wird ein vorhandenes, veraltetes Paket aktualisiert
Function Install-Choco-Pkg([String]$PkgID, [Switch]$InstallUpdate, [Int]$Indent = 1) {
	If (Is-Choco-Pkg-Installed -PkgID $PkgID) {
		If ($InstallUpdate) {
			If (Is-Choco-Pkg-Outdated -PkgID $PkgID) {
				Log $Indent "`nAktualisiere: $PkgID`n" -Fore Yellow
				$Res = choco upgrade --limitoutput --yes --exact $PkgID
			}
		}
	} Else {
		Log $Indent "`nInstalliere Chocolatey Paket: $PkgID`n" -Fore Yellow
		$Res = choco install --limitoutput --yes --exact $PkgID
	}
	
	# Klappte die Installation?
	If (!(Is-Choco-Pkg-Installed -PkgID $PkgID)) {
		Log 4 '  Chocolatey Fehler: (4EkRyhc)' -Fore Red
		Log 5 "  Konnte Paket nicht installieren: $PkgID" -Fore Red
		Start-Sleep -Milliseconds 4500
	}
}


# Wenn ein Update vorhanden ist, wird es installiert
Function Update-Choco-Pkg([String]$PkgID, [Switch]$InstallIfMissing, [Int]$Indent = 1) {
	If (Is-Choco-Pkg-Installed -PkgID $PkgID) {
		If (Is-Choco-Pkg-Outdated $PkgID) {
			Log $Indent "`nAktualisiere: $PkgID`n" -Fore Yellow
			$Res = choco upgrade --limitoutput --yes --exact $PkgID
		}
	} Else {
		If ($InstallIfMissing) {
			$Res = Install-Choco-Pkg -PkgID $PkgID
		}
	}
}


#Endregion Chocolatey Funcs


#Region WinGet Hilfsfunktionen

# Ist Choco vorhanden?
# (Egal, wie es installiert wurde)
Function Is-Choco-Installed() {
	Return @(Get-Command Choco.exe -EA SilentlyContinue).Count -ge 1
}

# Ist PowerShell 7 vorhanden?
# (Egal, wie es installiert wurde)
Function Is-PS7-Installed() {
	Return @(Get-Command PWSH.exe -EA SilentlyContinue).Count -ge 1
}


# Liefert True, wenn alle WinGet Tools installiert sind
# - Microsoft.Winget.Source
# - NuGet
# - PS Modul Microsoft.WinGet.Client
Function Is-WinGet-Tools-Installed() {
	Switch($PSVersionTable.PSVersion.Major) {
		5 {
			# 240708 M$ Bug:
			# Failed in attempting to update the source: winget
			# Failed when searching source; results will not be included: winget 
			# Das App-Paket Microsoft.Winget.Source installieren
			$IsAppPkgInstalled = @(Get-AppPackage -Name 'Microsoft.Winget.Source').Count -ge 1
			$IsPkgProviderInstalled = @(Get-PackageProvider | ? Name -eq NuGet).Count -ge 1
			$IsPSModuleInstalled = @(Get-Module -ListAvailable -Name 'Microsoft.WinGet.Client').Count -ge 1
		}
		7 { 
			# Die AppPackage-Befehle funktionieren nur in PS 5
			$Res = & PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command "@(Get-AppPackage -Name 'Microsoft.Winget.Source').Count -ge 1"
			$IsAppPkgInstalled = $Res.Trim().ToLower() -eq 'true'
			
			$Res = & PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command "@(Get-PackageProvider | ? Name -eq NuGet).Count -ge 1"
			$IsPkgProviderInstalled = $Res.Trim().ToLower() -eq 'true'
			
			$Res = & PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command "@(Get-Module -ListAvailable -Name 'Microsoft.WinGet.Client').Count -ge 1"
			$IsPSModuleInstalled = $Res.Trim().ToLower() -eq 'true'
		}
	}
	
	# Wenn das PS-Modul installiert ist, ist das wesentliche Ziel erreicht
	Return $IsPSModuleInstalled
}


# Wurde Choco durch WinGet installiert?
Function Is-Choco-Installed-ByWinGet() {
	# Ist Choco grundsätzlich installiert (egal wie)?
	If (!(Is-Choco-Installed)) {
		Return $False
	}
	
	If (!(Is-WinGet-Version-OK)) { Return $False }
	If (!(Is-WinGet-Tools-Installed)) { Return $False }
	
	# Ist Choco mit WinGet installiert?
	# Return @(Get-WinGetPackage Chocolatey).Count -ge 1
	Return Is-WinGet-Pkg-Installed -PkgID 'Chocolatey.Chocolatey' -ExactMatch
}


# Wurde PowerShell 7 durch WinGet installiert?
Function Is-PS7-Installed-ByWinGet() {
	# Wenn PS7 mit dem MSI installiert wurde:
	# - erkennt es WinGet 
	# - kann WinGet es aktualisieren
	
	# Jedoch haben manch WinGet PS-Modul Befehle eine intern abgefangene Exception, wenn die MSI-Installation gemacht wurde,
	# aber am Handling scheint sich nichts zu ändern
	
	# Zu erkennen, dass es nicht mit WinGet installiert wurde,
	# ist knifflig, weil die WinGet-Installation sich gleich
	# zu verhalten scheint, wie die MSI-Installation
	
	# Ist PS7 installiert, 
	# dann nehmen wir also immer an, dass es mit WinGet installiert wurde
	Return Is-PS7-Installed
}


# Ist das WinGet-Install installiert?
Function Is-WinGetInstall-Script-Installed() {
	Return @(Get-Command WinGet-Install -EA SilentlyContinue | ? CommandType -eq 'ExternalScript').Count -ge 1
}


# Winget auf dem ganzen System installieren
Function Install-WinGet([Int]$Indent = 1) {
	If (Is-WinGet-Version-OK) {
		Return
	}

	Log $Indent "`nInstalliere: WinGet`n" -Fore Yellow
	
	# !Q https://github.com/asheroto/winget-install

	# Vorbereiten
	Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process -Force

	# NuGet als Paketquelle zufügen
	If ( @(Get-PackageProvider | ? Name -eq NuGet).Count -eq 0 ) {
		Install-PackageProvider -Name NuGet -Scope AllUsers -Confirm:$False -Force
	}
	If ( @(Get-PSRepository -Name 'PSGallery' | ? InstallationPolicy -eq 'Trusted').Count -eq 0 ) {
		Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted
	}

	# Allenfalls das Script installieren: WinGet-Install
	If (-not(Is-WinGetInstall-Script-Installed)) {
		Install-Script WinGet-Install -Force
		# Wir müssen die Shell neu starten, um die Komponenten zu haben
		# $Script:SetupNeedsNewPSSession = $True
	}
	
	
	If ($Script:UseNewWingetSetup) {
		# WinGet via AppxPackage installieren
		Install-WinGet-Pkg-by-AppxPackage
		
	} Else {
		# WinGet installieren - klappt leider nicht immer
		WinGet-Install -Force
	}

	# Wir müssen die Shell neu starten, um die Komponenten zu haben
	$Script:SetupNeedsNewPSSession = $True
}


# Installiert die Winget-Tools
# - Microsoft.Winget.Source
# - NuGet
# - PS Modul Microsoft.WinGet.Client
Function Install-WinGet-Tools([Int]$Indent = 1) {
	If (Is-WinGet-Tools-Installed) {
		# Schon installiert
		Return
	}

	Log $Indent "`nInstalliere: WinGet-Tools`n" -Fore Yellow
	
	# Vorbereiten
	Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process -Force
	
	Switch($PSVersionTable.PSVersion.Major) {
		5 {
			# 240708 M$ Bug:
			# Failed in attempting to update the source: winget
			# Failed when searching source; results will not be included: winget 
			# Das App-Paket Microsoft.Winget.Source installieren
			If ( @(Get-AppPackage -Name 'Microsoft.Winget.Source').Count -eq 0 ) {
				Try {
					Add-AppPackage -Path 'https://cdn.winget.microsoft.com/cache/source.msix' -EA Stop
				} Catch {
					$MessageId = ('{0:x}' -f $_.Exception.HResult).Trim([char]0)
					$ErrorMessage = ($_.Exception.Message).Trim([char]0) # The network path was not found.
					Log 4 "`nFehler, ev. nicht relevant (4ElKbRC)" -Fore Red
					Log 5 "Befehl: Add-AppPackage -Path 'https://cdn.winget.microsoft.com/cache/source.msix'"
					Log 5 "ID : $MessageId"
					Log 5 "Msg: $ErrorMessage"
				}
			}
			
			# NuGet als Paketquelle zufügen
			# Wird für WinGet benötigt
			If ( @(Get-PackageProvider | ? Name -eq NuGet).Count -eq 0 ) {
				Install-PackageProvider -Name NuGet -Scope AllUsers -Confirm:$False -Force
			}
			Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted

			# Das WinGet PowerShell Modul installieren
			If ( @(Get-Module -ListAvailable -Name 'Microsoft.WinGet.Client').Count -eq 0 ) {
				Install-Module -Name Microsoft.WinGet.Client -Scope AllUsers -Confirm:$False -Force
			}
		}
		7 {
			# Die AppPackage-Befehle funktionieren nur in PS 5
			
			# 240708 M$ Bug:
			# Failed in attempting to update the source: winget
			# Failed when searching source; results will not be included: winget 
			# Das App-Paket Microsoft.Winget.Source installieren
			$Cmd = @"
			If ( @(Get-AppPackage -Name 'Microsoft.Winget.Source').Count -eq 0 ) {
				Add-AppPackage -path 'https://cdn.winget.microsoft.com/cache/source.msix'
			}
"@
			$Res = & PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command $Cmd
			
			# NuGet als Paketquelle zufügen
			# Wird für WinGet benötigt
			$Cmd = @"
			If ( @(Get-PackageProvider | ? Name -eq NuGet).Count -eq 0 ) {
				Install-PackageProvider -Name NuGet -Scope AllUsers -Confirm:$False -Force
			}
			Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted
"@
			$Res = & PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command $Cmd
			
			# Das WinGet PowerShell Modul installieren
			$Cmd = @"
			If ( @(Get-Module -ListAvailable -Name 'Microsoft.WinGet.Client').Count -eq 0 ) {
				Install-Module -Name Microsoft.WinGet.Client -Scope AllUsers -Confirm:$False -Force
			}
"@
			$Res = & PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command $Cmd
		}
	}
	
	# Wir müssen die Shell neu starten, um die Komponenten zu haben
	$Script:SetupNeedsNewPSSession = $True
}

#Endregion WinGet Hilfsfunktionen


#Region Tools: Neue PS-Session starten

# Startet eine neue PowerShell Shell
# - Wenn $StartShellIfTrue <> $Null, bricht das Script ab, wenn $False
# - Automatisch die gleiche PS-Version oder explizit PS5 oder PS7
# - Automatisch Elevated wie diese Session oder forciert Elevated
Function Test-AndRestart-PowerShell() {
	Param(
		# Explizit PS5 starten?
		[Switch]$StartPS5,
		# Explizit PS7 starten?
		[Switch]$StartPS7,
		# Soll ein Script gestartet werden?
		[String]$ScriptToStart,
		# Es soll kein Script gestartet werden
		[Switch]$DontStartScript,
		# Das Script, das gestartet werden soll
		[Switch]$NoExitNewSession,
		# Wenn definiert und True, wird die neue Session elevated gestartet
		[Switch]$ForceElevated,
		# Wenn definiert, wird das Script abgebrochen, wenn $False
		[System.Nullable[Bool]]$StartShellIfTrue,
		[String]$Msg,
		# Die aktuelle Shell am Ende nicht schliessen
		[Switch]$NoExit,
		[Int]$Indent = 1
	)

	## Config
	$DbgFunc = $False
	
	## Init
	If ($DontStartScript) {
		If ($DbgFunc) { Log $Indent "DontStartScript: $DontStartScript" -Fore magenta }
		$CfgStartScriptPath = $Null
	} Else {
		# Wenn kein Script definiert ist,
		If ([String]::IsNullOrWhiteSpace($ScriptToStart)) {
			# dann das Script selber nochmals starten
			$ScriptToStart = $Script:MyInvocation.MyCommand.Path
			If ($DbgFunc) { Log $Indent "ScriptToStart: $ScriptToStart" -Fore magenta }
		}
	}
	

	# Wenn für $StartShellIfTrue ein Parameter übergeben wurde
	If ($StartShellIfTrue -ne $Null) {
		If ($StartShellIfTrue -eq $False) {
			If ($DbgFunc) { 
				Log $Indent 'StartShellIfTrue: Ist false' -Fore magenta 
				Start-Sleep -Milliseconds 3500
			}
			Return
		}
	}
	
	# Abbruch, wenn das Script in ConEmu gestartet wurde
	# !9 Aus ConEmu heraus funktioniert Start-Process nicht richtig,
	#		weil es einen nested Aufruf via cmd gibt
	# Analyse
	$SessionFistHistory = Get-History | Sort id | Select -First 1
	$Shell_StartedBy_ConEmu = $SessionFistHistory.CommandLine -Match 'conemu'
	$Shell_StartedBy_ThisScript = $SessionFistHistory.CommandLine -match $Script:MyInvocation.MyCommand.Name
	
	# Die Shell wurde von ConEmu und nicht indirekt durch das Script selber gestartet
	$Shell_StartedBy_ConEmu = $Shell_StartedBy_ConEmu -and -not $Shell_StartedBy_ThisScript
	
	# Abbruch, wenn das Script in ConEmu gestartet wurde
	# !9 Aus ConEmu heraus funktioniert Start-Process nicht richtig,
	#		weil es einen nested Aufruf via cmd gibt
	If ($Shell_StartedBy_ConEmu) {
		Log 4 'Das Script muss in einer elevated PowerShell 7 Session gestartet werden' -ForegroundColor Red
		Start-Sleep -Milliseconds 4500
		Break Script
	}


	# Allenfalls die Nachricht anzeigen
	If (-not ([String]::IsNullOrWhiteSpace($Msg))) {
		Log $Indent "`n$($Msg)" -Fore Yellow
		Start-Sleep -Milliseconds 1500
	}
	
	
	# Runtime Status bestimmen
	$ThisDir = (Get-Location).Path
	
	# Elevated Starten?
	If ($ForceElevated) {
		$CfgStartElevated = $True
	} Else {
		$CfgStartElevated = Is-Elevated
	}
	
	# Welche PS Version?
	If ($StartPS5) {
		$SelectPSVersion = 5
	} ElseIf ($StartPS7) {
		$SelectPSVersion = 7
	} Else {
		# Nichts angegeben: die aktuelle PS-Version wieder starten
		$SelectPSVersion = $PSVersionTable.PSVersion.Major
	}

	Switch ($SelectPSVersion) {
		5 {
			$PS_exe = Get-Command PowerShell.exe -EA SilentlyContinue| Sort Version -Descending | select -First 1 -ExpandProperty Source
			If ($DbgFunc) { Log $Indent "61: $($PS_exe)" -Fore magenta }
		}
		7 {
			$PS_exe = Get-Command pwsh.exe -EA SilentlyContinue | Sort Version -Descending | select -First 1 -ExpandProperty Source
			If ($DbgFunc) { Log $Indent "62: $($PS_exe)" -Fore magenta }
		}
		Default {
			Log 4 'Fehler in: Test-AndRestart-PowerShell()' -Fore Red
			Log 5 "Unbekannte PowerShell-Version: $($PSVersionTable.PSVersion.Major)" -Fore Red
			Start-Sleep -Seconds 10
			Break Script
		}
	}
	

	## Die Script-Parameter der Session weitergeben
	
	# Aus den BoundParameters die global deaktivierten Swiches entfernen
	ForEach($DisabledParam in $Script:ScriptParams_DisabledSwitches) {
		If ($Script:MyInvocation.BoundParameters.ContainsKey($DisabledParam)) {
			$Null = $Script:MyInvocation.BoundParameters.Remove($DisabledParam)
		}
	}
	# Den BoundParameters die global aktivierten Swiches zufügen
	ForEach($EnabledParam in $Script:ScriptParams_EnabledSwitches) {
		# Den Switch entfernen, wenn er existiert, weil er $False sein könnte
		If ($Script:MyInvocation.BoundParameters.ContainsKey($EnabledParam)) {
			$Null = $Script:MyInvocation.BoundParameters.Remove($EnabledParam)
		}
		# Den Switch ergänzen
		$SwitchParamTrue = [System.Management.Automation.SwitchParameter]::new($True)
		$Null = $Script:MyInvocation.BoundParameters.Add($EnabledParam, $SwitchParamTrue)
	}
	
	[String[]]$InnerArguments = $Script:MyInvocation.BoundParameters.GetEnumerator() | 
	ForEach-Object {
		If ($_.Value -is [Switch]) { 
			"-$($_.Key)" 
		} Else { 
			# "-$($_.Key)", "$($_.Value)" 
			# Value mit " umschliessen
			"-$($_.Key)", "`"$($_.Value)`"" 
		}
   }
	$InnerArguments += $Script:MyInvocation.UnboundArguments
	
	# Die $OuterArguments initialisieren
	[String[]]$OuterArguments = @('-NoProfile', '-NoLogo', '-ExecutionPolicy Bypass')
	
	If ($NoExitNewSession) { $OuterArguments += '-NoExit' }
	If ($DontStartScript -or [String]::IsNullOrWhiteSpace($ScriptToStart)) {
		# Kein Script starten
		If ($DbgFunc) { Log $Indent "Kein Script starten (4EkVQk8)" -Fore Yellow }
	} ELse {
		$OuterArguments += '-File'
		$OuterArguments += "`"$($ScriptToStart)`""
	}
	
	$OuterArguments += $InnerArguments
	If ($DbgFunc) { Log $Indent "OuterArguments: $OuterArguments" -Fore DarkYellow }
	
	
	# Neue Session starten
	If ($CfgStartElevated) {
		Start-Process -FilePath $PS_exe -Verb RunAs -WorkingDirectory $ThisDir -ArgumentList $OuterArguments
		Start-Sleep -Milliseconds 3500
	} Else {
		Start-Process -FilePath $PS_exe -WorkingDirectory $ThisDir -ArgumentList $OuterArguments
		Start-Sleep -Milliseconds 3500
	}


	# Diese Session killen?
	If ($NoExit -or $Script:DebugPSSessions -or $DbgFunc) {
		Log $Indent "Schritt fertig, weiter geht's in der naechsten PS-Shell" -ForegroundColor Green
		If ($DbgFunc) { Log (1+$Indent) 'Ende, kein Prozess-Kill' -Fore magenta }
	} Else {
		If ($DbgFunc) { Log $Indent 'Kille den Prozess' -Fore magenta }
		Log (1+$Indent) "Schritt fertig, weiter geht's in der naechsten PS-Shell" -ForegroundColor Green
		Log (1+$Indent) '(Das Fenster schliess sich selber in 3500ms)' -ForegroundColor Gray
		Start-Sleep -Milliseconds 3500
		# Exit
		Stop-Process -ID $PID
	}
}

#Endregion Tools: Neue PS-Session starten


#Region Win32 Tools

Add-Type -AssemblyName System.Windows.Forms -ErrorAction SilentlyContinue

If (![bool]('ApiFuncs' -as [Type])) {
	Add-Type  @"
		// https://www.pinvoke.net/index.aspx
		using System;
		using System.Runtime.InteropServices;
		using System.Text;
		public class ApiFuncs {
			[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
			public static extern Int32 GetWindowText(IntPtr hwnd, StringBuilder lpString, Int32 cch);

			// Retrieves the handle to the foreground window
			[DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
			public static extern IntPtr GetForegroundWindow();
			
			[DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
			public static extern Int32 GetWindowThreadProcessId(IntPtr hWnd, out Int32 lpdwProcessId);

			[DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
			public static extern Int32 GetWindowTextLength(IntPtr hWnd);

			[DllImport("user32.dll")]
			[return: MarshalAs(UnmanagedType.Bool)]
			public static extern bool SetForegroundWindow(IntPtr hWnd);		
			
			[DllImport("user32.dll")]
			public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);

			public static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
			public const uint SWP_NOSIZE = 0x0001;
			public const uint SWP_NOMOVE = 0x0002;			
			
			// FORCEMINIMIZE = 11
			// HIDE            = 0
			// MAXIMIZE        = 3
			// MINIMIZE        = 6
			// RESTORE         = 9
			// SHOW            = 5
			// SHOWDEFAULT     = 10
			// SHOWMAXIMIZED   = 3
			// SHOWMINIMIZED   = 2
			// SHOWMINNOACTIVE = 7
			// SHOWNA          = 8
			// SHOWNOACTIVATE  = 4
			// SHOWNORMAL      = 1
			[DllImport("user32.dll")]
			public static extern bool ShowWindowAsync(IntPtr hWnd, Int32 nCmdShow); 
			
			// more here: http://www.pinvoke.net/default.aspx/user32.showwindow
			[DllImport("user32.dll")]
			[return: MarshalAs(UnmanagedType.Bool)]
			public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);			

			[DllImport("user32.dll")]
			[return: MarshalAs(UnmanagedType.Bool)]
			public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);
			
			[DllImport("User32.dll")]
			public static extern bool MoveWindow(IntPtr handle, Int32 x, Int32 y, Int32 width, Int32 height, bool redraw);
			
		}

		public struct RECT
		{
			public Int32 Left;        // x position of upper-left corner
			public Int32 Top;         // y position of upper-left corner
			public Int32 Right;       // x position of lower-right corner
			public Int32 Bottom;      // y position of lower-right corner
		}
"@
} else {
	Finalize_InitMsg
	Write-Verbose 'Type already registered: ApiFuncs'
}


# Liefert vom Primary Screen das WorkingArea
Function Get-Screen-Primary-WorkingArea() {
	# [System.Windows.Forms.Screen]::AllScreens
	
	# Location : {X=0,Y=0}
	# Size     : {Width=2048, Height=1112}
	# X        : 0
	# Y        : 0
	# Width    : 2048
	# Height   : 1112
	# Left     : 0
	# Top      : 0
	# Right    : 2048
	# Bottom   : 1112
	# IsEmpty  : False
	([System.Windows.Forms.Screen]::AllScreens | ? Primary).WorkingArea
}


# Get-Process-Window 'PowerShell'
Function Get-Process-Window {
    <#
        .SYNOPSIS
            Retrieve the window size (height,width) and coordinates (x,y) of
            a process window.

        .PARAMETER ProcessName
            Name of the process to determine the window characteristics

        .OUTPUT
            System.Automation.WindowInfo

        .EXAMPLE
            Get-Process-Window PowerShell | Get-Window
    #>
    [OutputType('System.Automation.WindowInfo')]
    [CmdletBinding()]
    Param (
        [Parameter(ValueFromPipelineByPropertyName=$True)]
        $ProcessName = $Null,
		[System.Nullable[Int]]$ProcessID = $Null,
		[System.Nullable[Int]]$MainWindowHandle = $Null
    )
    Begin { 
		$GetProcessSplat = @{}
		If (Has-Value $ProcessName) { $GetProcessSplat += @{ Name = $ProcessName} }
		If ($ProcessID -ne $Null) { $GetProcessSplat += @{ ID = $ProcessID} }
	}
    Process {
		Function Get-Window($MainWindowHandle) {
            $Rectangle = New-Object RECT
            $Return = [ApiFuncs]::GetWindowRect($MainWindowHandle, [ref]$Rectangle)
            If ($Return) {
                $Height = $Rectangle.Bottom - $Rectangle.Top
                $Width = $Rectangle.Right - $Rectangle.Left
                $Size = New-Object System.Management.Automation.Host.Size -ArgumentList $Width, $Height
                $TopLeft = New-Object System.Management.Automation.Host.Coordinates -ArgumentList $Rectangle.Left, $Rectangle.Top
                $BottomRight = New-Object System.Management.Automation.Host.Coordinates -ArgumentList $Rectangle.Right, $Rectangle.Bottom
                # If ($Rectangle.Top -lt 0 -AND $Rectangle.LEft -lt 0) {
                #     Write-Warning "Window is minimized! Coordinates will not be accurate."
                # }
                $Object = [PSCustomObject]@{
                    ProcessName = $ProcessName
					Width = $Width
					Height = $Height
					Left = $Rectangle.Left
					Top = $Rectangle.Top
					Right = $Rectangle.Right
					Bottom = $Rectangle.Bottom
                }
                $Object.PSTypeNames.insert(0,'System.Automation.WindowInfo')
                $Object
            }
		}
	
		If ($MainWindowHandle) {
			Get-Window $MainWindowHandle
		} Else {
			Get-Process @GetProcessSplat | ForEach {
				Get-Window ((Get-Process @GetProcessSplat).MainWindowHandle)
			}
		}
    }
}


# Set-WindowStyle MAXIMIZE $MainWindowHandle
Function Set-WindowStyle {
	Param(
		[Parameter()]
		$MainWindowHandle,
		[Parameter()]
		[ValidateSet('FORCEMINIMIZE', 'HIDE', 'MAXIMIZE', 'MINIMIZE', 'RESTORE', 
		'SHOW', 'SHOWDEFAULT', 'SHOWMAXIMIZED', 'SHOWMINIMIZED', 
		'SHOWMINNOACTIVE', 'SHOWNA', 'SHOWNOACTIVATE', 'SHOWNORMAL')]
		$Style = 'SHOW'
	)
	
	$WindowStates = @{
		FORCEMINIMIZE   = 11; HIDE            = 0
		MAXIMIZE        = 3;  MINIMIZE        = 6
		RESTORE         = 9;  SHOW            = 5
		SHOWDEFAULT     = 10; SHOWMAXIMIZED   = 3
		SHOWMINIMIZED   = 2;  SHOWMINNOACTIVE = 7
		SHOWNA          = 8;  SHOWNOACTIVATE  = 4
		SHOWNORMAL      = 1
	}
	Write-Verbose ("Set Window Style {1} on handle {0}" -f $MainWindowHandle, $($WindowStates[$style]))
	
	[ApiFuncs]::ShowWindow($MainWindowHandle, $WindowStates[$Style]) | Out-Null
	Start-Sleep -Milliseconds 250
}


# Set-Process-Window PowerShell -X 2040 -Y 142 -Passthru
# Get-Process PowerShell | Set-Process-Window  -X 2040 -Y 142 -Passthru
Function Set-Process-Window {
    <#
        .SYNOPSIS
            Sets the window size (height,width) and coordinates (x,y) of
            a process window.

        .PARAMETER ProcessName
            Name of the process to determine the window characteristics

        .PARAMETER X
            Set the position of the window in pixels from the top.

        .PARAMETER Y
            Set the position of the window in pixels from the left.

        .PARAMETER Width
            Set the width of the window.

        .PARAMETER Height
            Set the height of the window.

        .PARAMETER Passthru
            Display the output object of the window.
        .EXAMPLE
            Get-Process PowerShell | Set-Window -X 2040 -Y 142 -Passthru

            ProcessName Size     TopLeft  BottomRight
            ----------- ----     -------  -----------
            PowerShell  1262,642 2040,142 3302,784   
    #>
    [OutputType('System.Automation.WindowInfo')]
    [CmdletBinding()]
    Param (
        [parameter(ValueFromPipelineByPropertyName=$True)]
        $ProcessName = $Null,
		[System.Nullable[Int]]$ProcessID = $Null,
		[System.Nullable[Int]]$MainWindowHandle = $Null,
        [Int]$X,
        [Int]$Y,
        [Int]$Width,
        [Int]$Height,
        [switch]$Passthru
    )
    Begin { 
		$GetProcessSplat = @{}
		If (Has-Value $ProcessName) { $GetProcessSplat += @{ Name = $ProcessName} }
		If ($ProcessID -ne $Null) { $GetProcessSplat += @{ ID = $ProcessID} }
	}
    Process {
        $Rectangle = New-Object RECT
		If ($MainWindowHandle -eq $null) {
			$Handle = (Get-Process @GetProcessSplat).MainWindowHandle
		} Else {
			$Handle = $MainWindowHandle
		}

		# Sicherheitshalber:
		Set-WindowStyle $Handle RESTORE
		
        $Return = [ApiFuncs]::GetWindowRect($Handle,[ref]$Rectangle)
        If (-NOT $PSBoundParameters.ContainsKey('Width')) {            
            $Width = $Rectangle.Right - $Rectangle.Left            
        }
        If (-NOT $PSBoundParameters.ContainsKey('Height')) {
            $Height = $Rectangle.Bottom - $Rectangle.Top
        }
        If ($Return) {
            $Return = [ApiFuncs]::MoveWindow($Handle, $x, $y, $Width, $Height,$True)
        }
        If ($PSBoundParameters.ContainsKey('Passthru')) {
            $Rectangle = New-Object RECT
            $Return = [ApiFuncs]::GetWindowRect($Handle,[ref]$Rectangle)
            If ($Return) {
                $Height = $Rectangle.Bottom - $Rectangle.Top
                $Width = $Rectangle.Right - $Rectangle.Left
                $Size = New-Object System.Management.Automation.Host.Size -ArgumentList $Width, $Height
                $TopLeft = New-Object System.Management.Automation.Host.Coordinates -ArgumentList $Rectangle.Left, $Rectangle.Top
                $BottomRight = New-Object System.Management.Automation.Host.Coordinates -ArgumentList $Rectangle.Right, $Rectangle.Bottom
                # If ($Rectangle.Top -lt 0 -AND $Rectangle.LEft -lt 0) {
                #     Write-Warning "Window is minimized! Coordinates will not be accurate."
                # }
                $Object = [PSCustomObject]@{
                    ProcessName = $ProcessName
                    Size = $Size
                    TopLeft = $TopLeft
                    BottomRight = $BottomRight
                }
                $Object.PSTypeNames.Insert(0,'System.Automation.WindowInfo')
                $Object            
            }
        }
    }
}


# Berechnet das Delta der Applikations-Fenster
Function Calc-Process-Window-Deltas() {
	# Mit dem aktuellen PowerShell-Prozess arbeiten
	$WindowHandle = (Get-Process -Id $PID).MainWindowHandle

	# Die Bildschirmgrösse
	$ScreenSize = Get-Screen-Primary-WorkingArea
	# PowerShell: die aktuelle Grösse holen
	$PowerShellSizeOri = Get-Process-Window -ProcessID $PID
	# PowerShell: maximieren
	Set-WindowStyle $WindowHandle RESTORE
	Set-WindowStyle $WindowHandle MAXIMIZE
	# PowerShell: die maximierte Grösse holen
	$PowerShellSizeMax = Get-Process-Window -ProcessID $PID
	# PowerShell: Grösse wieder herstellen
	Set-Process-Window -ProcessID $PID -X $PowerShellSizeOri.Left -Y $PowerShellSizeOri.Top -Width $PowerShellSizeOri.Width -Height $PowerShellSizeOri.Height
	
	# Die Delta berechnen
	$DeltaX = [Math]::Abs(0 - $PowerShellSizeMax.Left)
	$DeltaY = [Math]::Abs(0 - $PowerShellSizeMax.Top)
	$DeltaW = [Math]::Abs($PowerShellSizeMax.Width - $ScreenSize.Width)
	$DeltaH = [Math]::Abs($PowerShellSizeMax.Height - $ScreenSize.Height)
	
	$ObjDelta = [PSCustomObject]@{
		DeltaX = $DeltaX
		DeltaY = $DeltaY
		DeltaW = $DeltaW
		DeltaH = $DeltaH
	}
	$ObjDelta.PSTypeNames.Insert(0, 'System.Automation.WindowDeltaInfo' )
	$ObjDelta
}


# Positioniert ein Applikationsfenster links oder rechts
# Als App-Referenz kann übergeben werden:
# - Das MainWindowHandle
# - Die ProcessID
# - Der ProcessName
Function Set-Window-LeftRight() {
	[CmdletBinding(SupportsShouldProcess)]
	Param (
		$WindowDeltas,
        [parameter(ValueFromPipelineByPropertyName)]
        $ProcessName = $Null,
		[System.Nullable[Int]]$ProcessID = $Null,
		[System.Nullable[Int]]$MainWindowHandle = $Null,
		[Switch]$PositionLeft,
		[Switch]$PositionRight
    )
	
	If ($PositionLeft -eq $False -and $PositionRight -eq $False) {
		Return
	}

	# Allenfalls das MainWindowHandle bestimmen
	If ($MainWindowHandle -eq $null -and [String]::IsNullOrEmpty($ProcessName) -and $ProcessID -eq $Null) {
		Log 4 'Set-Window-LeftRight(): Einer dieser Parameter muss definiert sein!:' -ForegroundColor Red
		Log 5 '-MainWindowHandle'
		Log 5 '-ProcessName'
		Log 5 '-ProcessID'
		Log 4 'Abbruch.'
		Return
	}
	
	If ($MainWindowHandle -eq $null) {
		$GetProcessSplat = @{}
		If (Has-Value $ProcessName) { $GetProcessSplat += @{ Name = $ProcessName} }
		If ($ProcessID -ne $Null) { $GetProcessSplat += @{ ID = $ProcessID} }
		$MainWindowHandle = (Get-Process @GetProcessSplat).MainWindowHandle
	}
	
	$ScreenSize = Get-Screen-Primary-WorkingArea
	
	# Neue Position berechnen
	If ($PositionLeft) {
		$NewPosX = 0 - $WindowDeltas.DeltaX
		$NewPosY = 0 - $WindowDeltas.DeltaY
		$NewPosR = ($ScreenSize.Width / 2) + $WindowDeltas.DeltaX
		$NewPosB = $ScreenSize.Height + $WindowDeltas.DeltaY
		$NewPosW = $NewPosR - $NewPosX
		$NewPosH = $NewPosB - $NewPosY
	}
	If ($PositionRight) {
		$NewPosX = ($ScreenSize.Width / 2) - $WindowDeltas.DeltaX
		$NewPosY = 0 - $WindowDeltas.DeltaY
		$NewPosR = $ScreenSize.Width  + $WindowDeltas.DeltaX
		$NewPosB = $ScreenSize.Height + $WindowDeltas.DeltaY
		$NewPosW = $NewPosR - $NewPosX
		$NewPosH = $NewPosB - $NewPosY
	}

	# Log 4 "NewPosX: $NewPosX"
	# Log 4 "NewPosY: $NewPosY"
	# Log 4 "NewPosW: $NewPosW"
	# Log 4 "NewPosH: $NewPosH"
	
	Set-Process-Window -MainWindowHandle $MainWindowHandle -X $NewPosX -Y $NewPosY -Width $NewPosW -Height $NewPosH
}


# Das PowerShell-Fenster links positionieren
Function Set-PowerShell-Window-Left() {
	# Die Applikationsfenster-Deltas genüber dem Bildschirm berechnen
	$WindowDeltas = Calc-Process-Window-Deltas
	Set-Window-LeftRight -WindowDeltas $WindowDeltas -ProcessID $PID -PositionLeft
}

#Endregion Win32 Tools

#Endregion Funcs


#Region Script-Logik Init


#Region Betriebsmodus bestimmen
$MsgBetriebsmodus = @('Setup Grund-Konfig Pkg-Mgmt')
If ($CfgStartElevated) {
	$MsgBetriebsmodus += ('Startet PS {0}, elavated' -f $CfgStartPowerShellVersion)
} Else {
	$MsgBetriebsmodus += ('Startet PS {0}, nicht elavated' -f $CfgStartPowerShellVersion)
}
If ($CfgJustStartTargetShell) {
	# $MsgBetriebsmodus = ''
}
If ($CfgJustInstallSW) {
	# $MsgBetriebsmodus = ''
}
If ($CfgStartScriptPath) {
	$MsgBetriebsmodus += 'Startet Script:'
	$MsgBetriebsmodus += (' > {0}' -f $CfgStartScriptPath)
}
If ($CfgStartScriptURL) {
	$MsgBetriebsmodus += 'Startet Script:'
	$MsgBetriebsmodus += (' > {0}' -f $CfgStartScriptURL)
}
If ($CfgStartBoxStarterScript) {
	$MsgBetriebsmodus += 'Startet BoxStarter-Setup:'
	$MsgBetriebsmodus += (' > {0}' -f $CfgStartBoxStarterScript)
}
#Endregion


## Runtime Infos sammeln
$IsWindowsUpdateRebootPending = Is-WindowsUpdate-RebootPending
$IsElevated = Is-Elevated
$IsRunningPS5 = $PSVersionTable.PSVersion.Major -eq 5
$IsRunningPS7 = !$IsRunningPS5
$IsPS7Installed = Is-PS7-Installed
$IsChocoInstalled = Is-Choco-Installed-ByWinGet
$IsBoxStarterInstalled = Is-BoxStarter-Installed

# WinGet infos
$IsWinGetInstalled = Is-WinGet-Installed
# Eine WinGet Version kleiner als 1.8 macht Probleme und findet nicht alle Pakete!
$IsWinGetVersionOK = Is-WinGet-Version-OK
$IsWinGetToolsInstalled = Is-WinGet-Tools-Installed

# Müssen wir PowerShell Module installieren?
$HasToInstallPSModules = ($InstallPSModule | ? { (Get-Module $_.Name -ListAvailable).Count -eq 0 }).Count -gt 0



# Wir brauchen das WinGet PS Modul
If (Is-WinGet-Tools-Installed) {
	Import-Module -Name 'Microsoft.Winget.Client'
}

#Endregion Script-Logik Init



#Region Script-Logik: Prepare
# Das PowerShell-Fenster links positionieren
Set-PowerShell-Window-Left

# Anzeige des Betriebsmode
Finalize_InitMsg
Print-Betriebsmode -Betriebsmode $MsgBetriebsmodus

# Anzeige der SN
Log 0 'BIOS Infos'
(Get-BIOS-SN | FL | Out-String).Split("`n") `
	| ? { [String]::isNullOrWhiteSpace($_) -eq $False } `
	| % {
	Log 1 $_
}

#Endregion


# Braucht Windows ein Reboot?
If ($IsWindowsUpdateRebootPending) {
	Log 0 'Windows Update braucht ZWINGEND einen Neustart von Windows' -Fore Red
	Do {
		$Response = Read-Host -Prompt 'Windows jetzt neu starten (sonst wird hier abgebrochen) [jy/n]?'
		$Response = $Response.Trim()
	} While ($Response -match '[^jJyYoOnN]')
	If ($Response -match '[JjYyoO]') {
		# Neustart des Systems
		Restart-Computer -Force
	} Else {
		Log 0 'Abbruch' -Fore Red
		Start-Sleep -Milliseconds 4500
		Break Script
	}
}


#Region SW-Installation
#Region SW-Installation: Prepare

# Merken, wenn wir die Shell neu starten müssen, 
# um installierte Komponenten zu laden
$Script:SetupNeedsNewPSSession = $False


# Müssen wir SW installieren?
# Ja, wenn nicht alle Tools installiert sind

$RequireSWSetup = $IsWinGetInstalled -eq $False `
						-or $IsWinGetToolsInstalled -eq $false `
						-or $IsChocoInstalled -eq $false `
						-or $IsWinGetVersionOK -eq $false



# Ja, wenn PS7 installiert oder aktualisiert werden soll und es fehlt
$RequirePS7Setup = ($InstallPS7 -or $CfgUpdateWinGetChocoBoxStarterPS7) -and -Not $IsPS7Installed


# Brauchen wir eine Elevated Session?
If (-Not $IsElevated -and ($CfgUpdateWinGetChocoBoxStarterPS7 `
									-or $RequireSWSetup `
									-or $RequirePS7Setup `
									-or $HasToInstallPSModules)) {
	# Elevated Session starten
	# Wir Starten fürs Setup immer eine PS5 Session
	# Bricht das aktuelle Script ab:
	Test-AndRestart-PowerShell -NoExit:$NoExit `
										-StartPS5 `
										-ForceElevated `
										-NoExitNewSession `
										-Msg 'Starte eine neue PowerShell Sitzung, um SW zu installieren'
	Return
}

#Endregion SW-Installation: Prepare



#Region SW-Installation: Setup der Pakete
$SetupSWSteps = 8
$SetupSWStep = 0

#Region SW-Setup Level 1: WinGet installieren
$SetupSWStep++
If ($IsWinGetInstalled -eq $False -or $IsWinGetVersionOK -eq $False) {
	# Winget ist nicht installiert oder zu alt, 
	# also installieren wir winget auf dem ganzen System
	If ($IsWinGetInstalled) {
		Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Aktualisiere WinGet'
	} Else {
		Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Installiere WinGet'
	}
	
	Install-WinGet
	Refresh-Env-Path
} ElseIf ($CfgUpdateWinGetChocoBoxStarterPS7) {
	# Aktualisieren, wenn veraltet: winget-install Script
	If (Is-WinGetInstall-Script-Installed) {
		winget-install -UpdateSelf | Out-Null
	}
}

# Allenfalls die Session neu starten und mit der Installation weiterfahren
If ($Script:SetupNeedsNewPSSession) {
	# Bricht das aktuelle Script ab:
	Test-AndRestart-PowerShell -NoExit:$NoExit `
										-NoExitNewSession `
										-Msg 'Starte eine neue PowerShell Sitzung, um WinGet zu nuetzen'
	Return
}
#Endregion 


#Region SW-Setup Level 2: Winget-Tools
$SetupSWStep++
If (-Not $IsWinGetToolsInstalled) {
	# WinGet-Tools installieren
	Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Installiere WinGet-Tools'
	Install-WinGet-Tools
	Refresh-Env-Path
}

# Allenfalls die Session neu starten und mit der Installation weiterfahren
If ($Script:SetupNeedsNewPSSession) {
	# Bricht das aktuelle Script ab:
	Test-AndRestart-PowerShell -NoExit:$NoExit `
										-NoExitNewSession `
										-Msg 'Starte eine neue PowerShell Sitzung, um die WinGet-Tools zu nuetzen'
	Return
}
#Endregion 


#Region SW-Setup Level 3: PS-Module
$SetupSWStep++

# Die PS-Module installieren
If ($HasToInstallPSModules) {
	# Installiere PS-Module
	Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Installiere PowerShell-Module'
	Log 0 'Installiere PowerShell-Module' -Fore Yellow

	$InstallPSModule | % {
		$ThisModule = $_
		Log 1 "- $($ThisModule.Name)" -ForegroundColor Yellow
		# Modul entfernen
		Remove-Module -Name $ThisModule.Name -Confirm:$False -Force -EA SilentlyContinue
		Start-Sleep -Milliseconds 200
		If (@(Get-Module $ThisModule.Name -ListAvailable).Count -eq 0) {
			# Modul installieren
			$ThisSplat = $ThisModule.Splat
			# Modul installieren
			Install-Module $ThisModule.Name @ThisSplat -Scope AllUsers -AcceptLicense -Confirm:$False -EA SilentlyContinue
		} Else {
			# Modul haben wir schon
			# Anstatt: 2>&1 Alle Pipelines umleiten: *>$null
			Update-Module $ThisModule.Name -Confirm:$False -EA SilentlyContinue *>$null
		}
		# Neustes Modul laden
		Import-Module -Name $ThisModule.Name
	}
	# Warten, bis die Module initialisiert sind
	Start-Sleep -Milliseconds 7000
	Refresh-Env-Path
}

#Region SW-Setup Level 4: PowerShell-Module aktualisieren
$SetupSWStep++
If ($CfgUpdatePSModules) {
	# Die PS-Module aktualisieren
	Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Aktualisiere PowerShell-Module'
	Log 0 'Aktualisiere alle PowerShell-Module' -Fore Yellow
	# Anstatt: 2>&1 Alle Pipelines umleiten: *>$null
	Update-Module -Confirm:$False -EA SilentlyContinue *>$null

	# Alte PS-Module entfernen
	Log 1 'Loesche alte PowerShell-Module' -Fore Yellow
	Remove-Old-PSModules

	# Sicherstellen, dass UpdatePSModules
	# nun in keinem Folgeaufruf des Scripts mehr ausgeführt wird
	Disable-ScriptStart-UpdatePSModules
}

#Endregion 


#Region SW-Setup Level 5: Chocolatey
$SetupSWStep++
If (-Not $IsChocoInstalled) {
	# Installiert Chocolatey mit WinGet
	# Der Vorteil der WinGet-Installation ist,
	# 	dass das Upgrade von Choco sehr einfach mit WinGet gemacht werden kann
	#
	# !9 Wird einfach über eine bestehende MSI-Installation darüber installiert,
	# 		weil das MSI-Setup Choco nicht in der Liste der installierten Apps registriert
	# 		und weil deshalb auch kein Uninstall-String existiert
	#		und weil die Installations-Dirs identisch sind, wie wenn man Choco mit WinGet installiert

	Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Installiere Chocolatey'
	Install-WinGet-Pkg -PkgID 'Chocolatey.Chocolatey'
	Refresh-Env-Path
	
	# Neue Shell starten
	# Bricht das aktuelle Script ab:
	Test-AndRestart-PowerShell -NoExit:$NoExit `
										-NoExitNewSession `
										-Msg 'Starte eine neue PowerShell Sitzung, um Choco zu nuetzen'
	Return

} ElseIf ($CfgUpdateWinGetChocoBoxStarterPS7) {
	Update-WinGet-Pkg -PkgID 'Chocolatey.Chocolatey'
}
#Endregion 


#Region SW-Setup Level 5b: Chocolatey Config
# Choco soll Installations-Parameter bei Updates wiederverwenden
choco feature enable -n=useRememberedArgumentsForUpgrades *>$null
#Endregion 


#Region SW-Setup Level 6: BoxStarter
$SetupSWStep++
If (!(Is-Choco-Pkg-Installed -PkgID 'boxstarter')) {
	Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Installiere BoxStarter'
	Install-Choco-Pkg -PkgID 'boxstarter'
	Delete-BoxStarter-DesktopIcon
	Refresh-PSModulePath
	Refresh-Env-Path

	# Session neu starten, damit Boxstarter genützt werden kann
	Test-AndRestart-PowerShell -NoExit:$NoExit `
										-NoExitNewSession `
										-Msg 'Starte eine neue PowerShell Sitzung, um BoxStarter zu nuetzen'
	Return
} ElseIf ($CfgUpdateWinGetChocoBoxStarterPS7) {
	Update-Choco-Pkg 'boxstarter'
}
#Endregion 


#Region SW-Setup Level 7: PS7
$SetupSWStep++

# Winget-Infos
# winget --info

# Muss PS7 aktualisiert oder installiert werden?
If (-not $IsPS7Installed -and $CfgInstallPS7) {
	Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Installiere PowerShell 7'
	Install-WinGet-Pkg -PkgID 'Microsoft.PowerShell'
	Refresh-Env-Path
	# Konnte sich PS7 in die aktuelle Shell integrieren?
	$IsPS7Installed = Is-PS7-Installed
} ElseIf ($IsPS7Installed -and $CfgUpdateWinGetChocoBoxStarterPS7) {
	Update-WinGet-Pkg -PkgID 'Microsoft.PowerShell'
}
	
#Endregion 


#Region SW-Setup Level 8: PS-Repositories ergänzen
$SetupSWStep++
$RepoName = 'snexus.jig.ch'
$RepoLocation = 'https://snexus.jig.ch/repository/tom-powershell-modules/'

If (-Not(Get-PSRepository | ? Name -eq $RepoName)) {
	Print-Schritt -Step $SetupSWStep -Steps $SetupSWSteps -Info 'Installiere PowerShell Repositories'
	Log 0 'Aktualisiere PowerShell-Repositories' -Fore Yellow
	
	## Einrichten
	#	!9 In PS5 ist der Befehl Register-PSRepository buggy,
	#		Deshalb wird es in PS7 installiert
	If ($PSVersionTable.PSVersion.Major -lt 7) {
		pwsh -c "`$Res = Register-PSRepository -Name $RepoName -InstallationPolicy 'Trusted' -SourceLocation $RepoLocation; `$Res"
	} Else {
		Register-PSRepository -Name $RepoName -InstallationPolicy 'Trusted' -SourceLocation $RepoLocation
	}
	Refresh-Env-Path
	
	## !TT
	# Prüfen
	# Get-PSRepository | select *
	# Repo entfernen
	# Unregister-PSRepository -Name $RepoName
}

#Endregion 

#Endregion SW-Installation: Setup der Pakete

#Endregion SW-Installation
Log 0 'Alle SW fertig installiert' -Fore Green


#Region Ziel-PS-Session und allenfalls das Script starten

#Region Prepare

# Wollten wir nur die SW installieren?
If ($CfgJustInstallSW) {
	Break Script
}


# Ein kurzes Delay, damit Fehler allenfalls gesehen werden können
Start-Sleep -Milliseconds 3500
CLS


# Muss ein Script heruntergeladen werden?
If (!([String]::IsNullOrWhiteSpace($CfgStartScriptURL))) {
	# Das Script herunterladen
	$TmpScriptFile = Get-TempFile -Ext '.ps1'
	$DLOK = $False
	Try {
		$ProgressPreference = 'SilentlyContinue'
		Invoke-WebRequest $CfgStartScriptURL -OutFile $TmpScriptFile -EA Stop
		$ProgressPreference = 'Continue'
		
		$DLOK = $True
	} Catch { }
	
	# Wird direkt die Temp-Datei gestartet?
	If ([String]::IsNullOrWhiteSpace($CfgScriptSavePath)) {
		# Wir werden das Temp-File starten
		$CfgStartScriptPath = $TmpScriptFile
	} Else {
		# Wir haben eine Zieldatei
		# Versuchen, die Zieldatei zu Aktualisieren / erstellen
		Try {
			$Null = Copy-Item -LiteralPath $TmpScriptFile -Destination $CfgScriptSavePath -Force -EA Stop
			# Wir arbeiten mit dem richtigen Script-File
			$CfgStartScriptPath = $CfgScriptSavePath
		} Catch {
			# Wir arbeiten mit dem Temp Script-File
			$CfgStartScriptPath = $TmpScriptFile
		}
	}
}


# Haben wir die richtige PS-Session Version?
$IsPSSessionVersionOK = ($CfgStartPowerShellVersion -eq 5 -and $IsRunningPS5) `
							-or ($CfgStartPowerShellVersion -eq 7 -and $IsRunningPS7)


# Ist eine Elevated-Sitzung gewünscht?
$IsPSSessionElevationOK = If ($CfgStartElevated) { $IsElevated } else { $true }

#Endregion Prepare


#Region Wir haben bereits die richtige Shell

# Stimmt die PS Shell bereits und wir können einfach das Ziel-Script-Starten?
If ($IsPSSessionVersionOK -and $IsPSSessionElevationOK) {
	# Wir haben schon die richtige Shell
	If ($CfgJustStartTargetShell) {
		# Wir wollen nur die richtige Shell gestartet haben
		Log 0 "`nHier ist die gewuenschte PowerShell-Shell:" -Fore Green
		If ($CfgStartElevated) {
			Log 1 ('PowerShell {0}, Elevated' -f $CfgStartPowerShellVersion) -Fore Green
		} Else {
			Log 1 ('PowerShell {0}, nicht Elevated' -f $CfgStartPowerShellVersion) -Fore Green
		}
		Break Script
	} Else {
		
		If (Has-Value $CfgStartScriptPath) {
			# Wir wollen in der richtigen Shell das Script starten
			& $CfgStartScriptPath
		}
		If (Has-Value $CfgStartBoxStarterScript) {
			# Wir wollen ein BoxStarter-Script starten
			Load-BoxStarter-Module
			Install-BoxstarterPackage -Package $CfgStartBoxStarterScript -DisableReboots -Verbose:$False
		}
		Break Script
	}
}

#Endregion Wir haben bereits die richtige Shell


#Region Wir müssen eine neue Shell starten

If ($CfgJustStartTargetShell) {
	$Msg = 'Starte die benoetigte PowerShell-Shell'
} Else {
	$Msg = 'Starte das definitive PowerShell-Script'
}

# Die neue Shell starten
Test-AndRestart-PowerShell -NoExit:$NoExit `
									-StartPS5:($CfgStartPowerShellVersion -eq 5) `
									-StartPS7:($CfgStartPowerShellVersion -eq 7) `
									-NoExitNewSession `
									-ScriptToStart $CfgStartScriptPath `
									-DontStartScript:($CfgJustStartTargetShell -eq $True) `
									-ForceElevated:$CfgStartElevated `
									-Msg $Msg


# Diese Session killen?
If (!($Script:DebugPSSessions)) {
	# Exit
	Stop-Process -ID $PID
}

#Endregion Wir müssen eine neue Shell starten

#Endregion Ziel-PS-Session und allenfalls das Script starten

