# Darf *nicht* als BOM gespeichert sein,
# 	weil iex / irm sonsz nicht klarkommt
# 
# Permanent URL:
# 	https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/Init-PowerShell.ps1


# 240713	OUTDATED, neu: Bootstrap-PowerShell.ps1



# Starten des Scripts
# •••••••••••••••••••
#	1. Windows >> Run
#		PowerShell.exe -NoExit -ExecutionPolicy Bypass
#
#	2. Paste:
#		# Die Shell am Ende wieder schliessen:
# 			iex "& { $(irm 'https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/Init-PowerShell.ps1') }"
#
#		# Optional:
#		# Die Shell am Ende offen behalten:
#			iex "& { $(irm 'https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/Init-PowerShell.ps1') } -NoExit"
#
#
# Führt diese Aufgaben duch
# 	Konfiguriert PowerShell 5
# 		Add PSRepository snexus.jig.ch
# 		Install basic Modules
# 		Update all Modules
# 		Uninstall old Modules
# 	Installiert pwsh / PowerShell 7
# 	Konfiguriert pwsh / PowerShell 7
#	Installiert Chocolatey
#	Installiert BoxStarter
#	Startet eine neue BoxStarter Shell für den nächsten Installations-Schritt
#
# 
# 001, 220813, tom-agplv3@jig.ch
# 002, 220814
# 003, 220925
# 		Testet ob Server, mit denen das Script arbeitet, erreichbar sind
# 

[CmdletBinding()]
Param(
	# Die elevated Shell nicht schliessen
	[Switch]$NoExit
)


$Version = '003, 220814'


## Pre-Conditions
If ($PSVersionTable.PSVersion.Major -gt 5) {
	Write-Host "`nDieses Script muss in PowerShell 5 gestartet werden" -ForegroundColor Red
	Write-Host 'und es initialisiert PowerShell 5 und 7' -ForegroundColor Red

	Start-Sleep -MilliS 3500
	Write-Host -NoNewLine "`nPress any key to continue…" -ForegroundColor Green
	$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
	Return
}



### Config

# Perma Link zum eigenen Script
$InitPowerShellUri = 'https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/Init-PowerShell.ps1'

$InstalledBoxstarterShell_ps1 = 'C:\ProgramData\Boxstarter\BoxstarterShell.ps1'

$RepoName = 'snexus.jig.ch'
$RepoLocation = 'https://snexus.jig.ch/repository/tom-powershell-modules/'
$SetupPS7Url = 'https://aka.ms/install-powershell.ps1'
$SetupChocoUrl = 'https://community.chocolatey.org/install.ps1'
$SetupBoxstarterUrl = 'https://boxstarter.org/bootstrapper.ps1'
$SetupMaNotebookBoxstarterUrl =  'https://www.akros.ch/it/Chocolatey/Install-Boxstarter-Akros-MA-Notebook-Web.ps1'

$InstallPackages = @(
	'PSReadLine'
	# A bash inspired readline implementation for PowerShell
	# https://github.com/PowerShell/PSReadLine
)



# Deinstalliert alte Module
# !Q https://www.ondfisk.dk/removing-duplicate-powershell-modules/
Function Remove-OldModules() {
	# Author: Luke Murray (Luke.Geek.NZ)
	# 0.1
	# Basic function to remove old PowerShell modules which are installed
	$AllLatest = Get-InstalledModule 
	ForEach ($ThisLatest in $AllLatest) {
		Get-InstalledModule $ThisLatest.Name -AllVersions | ? {$_.Version -ne $ThisLatest.Version} | % {
			$ObsoleteModule = $_
			Write-Verbose "Uninstalling $($ThisLatest.Name): $ObsoleteModule.Version"
			$ObsoleteModule | Uninstall-Module -Verbose
		}
	}
}


# True, wenn Elevated
# 220813
Function Is-Elevated() {
	([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')
}


# Testet, ob eine Netzwerk-Verbindung auf Port 443 aufgebaut werden kann
# Vorsicht!, ist sehr träge :-(
Function Wait-For-ServerConnection($Url, $Port = 443) {
	If ([String]::IsNullOrWhiteSpace($Url)) { Return $True }
	[URI]$Uri = $Url
	$TestSleepS = 30
	$FirstMsgDisplayed = $False
	
	$Found = $False
	Do {
		$Res = Test-NetConnection -Port $Port -ComputerName $Uri.Host
		If ($Res.TcpTestSucceeded -eq $False) {
			If ($FirstMsgDisplayed -eq $False) {
				$FirstMsgDisplayed = $True
				Write-Host ('Erreiche den Server nicht: `n{0}' -f $Url) -ForegroundColor Red
				Write-Host ('Re-Test alle {0}s' -f $TestSleepS) -ForegroundColor Yellow
			} Else {
				Write-Host ('  Erreiche den Server nicht: `n{0}' -f $Url) -ForegroundColor Red
			}
			Start-Sleep -Seconds $TestSleepS
		}
	} While ($Found -eq $False)
	
	If ($FirstMsgDisplayed -eq $True) {
		Write-Host '  Verbindung OK' -ForegroundColor Green
	}
}


# Testet, ob ein Ping auf einen Server funktioniert
# Ping geht auf:
# 	[URI]$Uri = $Url
# 	$Uri.Host
Function Wait-For-PingOK($Url) {
	If ([String]::IsNullOrWhiteSpace($Url)) { Return $True }
	$TestSleepS = 30
	$FirstMsgDisplayed = $False
	[URI]$Uri = $Url
	
	$Found = $False
	Do {
		$Found = Test-Connection $Uri.Host -Count 1 -Quiet
		If ($Found -eq $False) {
			If ($FirstMsgDisplayed -eq $False) {
				$FirstMsgDisplayed = $True
				Write-Host ('Erreiche den Server nicht: `n{0}' -f $Url) -ForegroundColor Red
				Write-Host ('Re-Test alle {0}s' -f $TestSleepS) -ForegroundColor Yellow
			} Else {
				Write-Host ('  Erreiche den Server nicht: `n{0}' -f $Url) -ForegroundColor Red
			}
			Start-Sleep -Seconds $TestSleepS
		}
	} While ($Found -eq $False)
	
	If ($FirstMsgDisplayed -eq $True) {
		Write-Host '  Verbindung OK' -ForegroundColor Green
	}
}


### Prepare

## Testet, ob die URLs erreichbar sind
# > Ist sehr langsam und nur via IP > Fehler bei reverse Proxies
# Wait-For-ServerConnection $InitPowerShellUri
# Wait-For-ServerConnection $RepoLocation
# Wait-For-ServerConnection $SetupPS7Url
# Wait-For-ServerConnection $SetupChocoUrl


# Start Elevated
if (!(Is-Elevated)) { 
	Write-Host "`n`nInitialisiere PowerShell $($Version)`n`n" -ForegroundColor Green
	Write-Host ">> Ich starte PowerShell als Administrator (Elevated)`n`n" -ForegroundColor Red
	Start-Sleep -Seconds 4

	$Command = "Invoke-Expression -Command (Invoke-RestMethod -Uri `"$InitPowerShellUri`")"
	
	If ($NoExit) {
		Start-Process PowerShell.exe -Verb RunAs -ArgumentList "-ExecutionPolicy Bypass -NoExit -Command $Command"
	} Else {
		Start-Process PowerShell.exe -Verb RunAs -ArgumentList "-ExecutionPolicy Bypass -Command $Command"
	}

	# Exit from the current, unelevated, process
	Start-Sleep -MilliS 2500
	Exit
	
} Else {
	$Host.UI.RawUI.WindowTitle = $MyInvocation.MyCommand.Definition + ' (Elevated)'
	$Host.UI.RawUI.BackgroundColor = 'DarkBlue'
	Clear-Host
	Write-Host "`n`nInitialisiere PowerShell $($Version)`n`n" -ForegroundColor Green
}


If ( (Is-Elevated) -eq $False) {
	Write-Host "`nDas Script muss als Administrator / Elevated ausgeführt werden" -ForegroundColor Red
	Start-Sleep -MilliS 3500
	Write-Host -NoNewLine "`nPress any key to continue…" -ForegroundColor Green
	$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
	Return
}


# Set TLS 1.2 (3072) as that is the minimum required by Chocolatey.org.
# Use integers because the enumeration value for TLS 1.2 won't always exist
# [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072


# Tastatur konfigurieren
Set-WinUserLanguageList -Force de-ch



### Main


# Ins C:\Temp wechseln
$TempDir = 'C:\Temp'
If (-Not(Test-Path -LiteralPath $TempDir)) {
	Write-Host "Setting working Dir: $($TempDir)" -ForegroundColor Yellow
	New-Item -Path $TempDir -ItemType Directory | Out-Null
}
CD $TempDir


## PowerShell help aktualisieren
Write-Host 'Updating PowerShell help in a background job' -ForegroundColor Yellow
[void](Start-Job {Update-Help -force})


# PackageProvider einrichten
Write-Host 'Installing the PackageProvider Nuget' -ForegroundColor Yellow
Install-PackageProvider Nuget -Force | Out-Null


## PS7 installieren
If (@(Get-Command pwsh -ErrorAction SilentlyContinue).Count -eq 0) {
	Write-Host 'Installing PowerShell 7' -ForegroundColor Yellow -NoNewLine
	Write-Host ' (this will take a while)' -ForegroundColor Cyan
	# !Q https://www.thomasmaurer.ch/2019/07/how-to-install-and-update-powershell-7/
	iex "& { $(irm $SetupPS7Url) } -UseMSI -Quiet"
	# Umgebungsvariablen aktualisieren / Refresh Environment Variables
	# Nur Machine
	$Env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")
	# Machine + User
	# $Env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")  
}


## Das PSRepository 'snexus.jig.ch' einrichten
Write-Host "Configuring the PSRepository $RepoName" -ForegroundColor Yellow
If (-Not(Get-PSRepository | ? Name -eq $RepoName)) {
	
	## Einrichten
	#	!9 In PS5 ist der Befehl Register-PSRepository buggy,
	#		Deshalb wird es in PS7 installiert

	If ($PSVersionTable.PSVersion.Major -lt 7) {
		pwsh -c "`$Res = Register-PSRepository -Name $RepoName -InstallationPolicy 'Trusted' -SourceLocation $RepoLocation; `$Res"
	} Else {
		Register-PSRepository -Name $RepoName -InstallationPolicy 'Trusted' -SourceLocation $RepoLocation
	}
	
	## !TT
	# Prüfen
	# Get-PSRepository | select *
	# Repo entfernen
	# Unregister-PSRepository -Name $RepoName
}



## Module installieren, die generell Probleme bei der Installation machen


# PowerShellGet
# the package manager for PowerShell
# https://github.com/PowerShell/PowerShellGet
Write-Host 'Installing PowerShellGet' -ForegroundColor Yellow
If (@(Get-Module PowerShellGet -ListAvailable).Count -eq 0) {
	Install-Module PowerShellGet -Force
}


# Pester
# https://github.com/pester/Pester
Write-Host 'Installing Pester' -ForegroundColor Yellow
If (@(Get-Module Pester -ListAvailable).Count -eq 0) {
	Install-Module Pester -Force -skipPublisherCheck
}



## Chocolatey

Write-Host 'Installing Chocolatey' -ForegroundColor Yellow
Set-ExecutionPolicy Bypass -Scope Process -Force
iex ((New-Object System.Net.WebClient).DownloadString($SetupChocoUrl))

# Choco soll Installations-Parameter bei Updates wiederverwenden
choco feature enable -n=useRememberedArgumentsForUpgrades


Write-Host 'Installing BoxStarter' -ForegroundColor Yellow
. { iwr -useb $SetupBoxstarterUrl } | iex; Get-Boxstarter -Force



## Die übrigen Module installieren
$InstallPackages | % {
	$ThisModule = $_
	Write-Host "Installing $ThisModule" -ForegroundColor Yellow
	If (@(Get-Module $ThisModule -ListAvailable).Count -eq 0) {
		Install-Module $ThisModule -Force
	}
}



## Alle Module aktualisieren
Write-Host 'Update all Modules' -ForegroundColor Yellow
Update-Module -Force


## Veraltete Module entfernen
Write-Host 'Remove old Modules' -ForegroundColor Yellow
Remove-OldModules



## BoxStarter Shell starten, wenn vorhanden
## Sonst: eine neue PS5 Shell für die Installation starten
#	!KH https://stackoverflow.com/a/68068534/4795779
$HasBoxStarter = (Test-Path -LiteralPath $InstalledBoxstarterShell_ps1)
If ($HasBoxStarter) {
	Write-Host 'Starte eine neue BoxStarter Shell für den nächsten Installations-Schritt' -ForegroundColor Green
	$Msg1 = 'Wenn gewuenscht Schritt 2 mit BoxStarter ausfuehren:'

	# 002: BoxStarter starten, ohne Fehler-Check
	# $StartBoxStarterCmd = 'Install-BoxstarterPackage -Package ' + $SetupMaNotebookBoxstarterUrl + ' -DisableReboots; CLS; Write-Host "`nInstallation fertig.`nSystem wird in 45s neu gestartet." -Fore green'

	# 003: BoxStarter starten, wenn der Fehler-Check i.O. ist
	$StartBoxStarterCmd = 'If (@(Get-Command Install-BoxstarterPackage -EA Silent).Count -eq 0) { Write-Host "`nPowerShell-Paket fehlt:`nInstall-BoxstarterPackage" -Fore Red } Else { Install-BoxstarterPackage -Package ' + $SetupMaNotebookBoxstarterUrl + ' -DisableReboots; Write-Host "`n`n`n`nInstallation fertig.`nSystem wird in 45s neu gestartet." -Fore green}'
	$Msg2 = "(Ist im Clipboard)"

	# BoxStarter Shell starten wenn vorhanden
	# -NoProfile
	# Start-Process PowerShell "-NoExit -NoLogo  -ExecutionPolicy ByPass -Command &'$InstalledBoxstarterShell_ps1';"
	Start-Process PowerShell "-NoExit -NoLogo -ExecutionPolicy ByPass -Command CD 'C:\Temp'; &'$InstalledBoxstarterShell_ps1'; CLS; Write-Host 'Welcome to BoxStarter';Write-Host''; Write-Host '$Msg1' -Fore Yellow;Write-Host''; Write-Host '$StartBoxStarterCmd'; Write-Host '$Msg2' -Fore Green;Write-Host;"; $StartBoxStarterCmd | Set-Clipboard;
}


If ($NoExit) {
	Write-Host 'done!' -ForegroundColor Green
} Else {
	Write-Host 'done!' -ForegroundColor Green -NoNewLine
	Write-Host ' (The window closes itself)' -ForegroundColor Gray
	Start-Sleep -MilliS 2500
}
