﻿

# Ideen

## Radio-Buttons
- In einem zusätzlichen Hashtable-Property kann die Zuordnung für Radio-Buttons definiert werden und Ask-User-Select-CheckBoxes.ps1 stellt den Fragebogen dann richtig dar und handelt die Logik
Beispiel:
	


	$Selectable = [Ordered]@{
			KeyOpt1 = @{
			Name = 'Checkbox 1'
			Enabled = $True
			Help = 'Hilfe zu Option 1'
		},
		KeyOpt2 = @{
			Name = 'Checkbox 2'
			Enabled = $False
			Help = ''
		},
		
		# Radio Gruppe 1
		KeyOpt2 = @{
			Name = 'Radio Gruppe 1, Option 1'
			RadioGrp = '1'
			Enabled = $True
			Help = ''
		},
		KeyOpt2 = @{
			Name = 'Radio Gruppe 1, Option 2'
			RadioGrp = '1'
			Enabled = $False
			Help = ''
		},

		# Radio Gruppe 2
		KeyOpt2 = @{
			Name = 'Radio Gruppe 2, Option 1'
			RadioGrp = '2'
			Enabled = $False
			Help = ''
		},
		KeyOpt2 = @{
			Name = 'Radio Gruppe 2, Option 1'
			Enabled = $False
			RadioGrp = '2'
			Help = ''
		}
	}






