## Suppress PSScriptAnalyzer Warning
[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingCmdletAliases', '')]
[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseApprovedVerbs', '')]
[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidDefaultValueSwitchParameter', '')]
# [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSShouldProcess', '')]
# [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]

# 💡 Git Repo:
# • GitLab-URL Project: https://gitlab.com/jig-opensource/source-code/scripts
# • GitLab-URL File: https://gitlab.com/jig-opensource/source-code/scripts/-/blob/main/PowerShell/TomLib/functions/public/Download-Webserver-Filelisting.ps1


# Lädt von einem File-Webserver wie https://www.akros.ch/it/
# ein Verzeichnis herunter und speichert die Files lokal
# 	Optional Rekursiv
# 	Aktualisiert nur veraltete / fehlende Files
# 	Optional mit Progress-Info

# 001, 240624 tom@jig.ch

# !Ex
# 	Herunterladen: https://server.com/files
#  Mit Progress-Info
# .\Download-Webserver-Filelisting.ps1 -FileListingURL 'https://server.com/files' -Recursive -ZielDir 'c:\Temp\Files'


[CmdletBinding(DefaultParameterSetName='ListOnly')]
Param(
	[Parameter(Mandatory, ParameterSetName='ListOnly')]
	[Parameter(Mandatory, ParameterSetName='Download')]
	# eg 'https://server.com/files'
	[String]$FileListingURL,
	[Parameter(Mandatory, ParameterSetName='Download')]
	# eg 'C:\Temp\Files'
	[String]$ZielDir,
	[Parameter(ParameterSetName='Download')]
	[Parameter(ParameterSetName='ListOnly')]
	[Switch]$Recursive,
	[Parameter(ParameterSetName='Download')]
	[Switch]$ProgressInfo,
	[Parameter(ParameterSetName='ListOnly')]
	[Switch]$ListOnly
)

# Write-Host $PsCmdlet.ParameterSetName



## Config


## Functions


# Liest von einer WebServer-Dateiliste 
# nur die wichtigsten File-Informationen, 
# also OHNE Dateigrösse und last modified Datum
# Optional: Rekursiv
#
# Siehe auch: Get-Webserver-FileList
Function Get-WebServerDir-Files-Simple() {
	[CmdletBinding()]
	Param(
		[String]$FileListingURL, 
		[Switch]$Recursive = $False, 
		# Der Relative Pfad zur initial aufgerufenen Webseite, ohne URL-Codierung
		[String]$SubDirWindows = ''
	)

	## Init
	$SubDirWindows = $SubDirWindows.TrimEnd('/\')

	# Sicherstellen, dass wir am Ende einen / haben
	$FileListingURL = $FileListingURL.TrimEnd('/') + '/'
	$SiteUri = [System.Uri]::new($FileListingURL)
	
	$Website = Invoke-WebRequest -UseBasicParsing -Uri $FileListingURL
	$Links = $Website.Links 
	# Header entfernen
	$FileListing = $Links | ? href -NotMatch '\?.*' 
	
	# Die Liste durchlaufen
	$Res = @()
	$FileListing | Select -ExpandProperty href | % {
		$Href = $_
		## Analyse
		# Start und Ende mit /
		$IsParentDir = $Href -match '^/.*/$'
		# Ende mit /
		$IsDir = $Href -match '.*/$'
		$IsFile = !$IsParentDir -and !$IsDir
		## Verarbeiten
		If ($IsParentDir) {
			Return	# Nächstes Element
		}
		If ($IsDir) {
			# Write-Host ('Dir : {0}' -f $href)
			$Name = [System.Web.HttpUtility]::UrlDecode($Href).TrimEnd('/')
			$PathURL = [System.Uri]::new($SiteUri, $Href).AbsoluteUri
			$RelPath = $SubDirWindows + $Name
			$Res += [PSCustomObject][Ordered]@{
				IsDir = $True
				IsFile = $False
				Name = $Name
				Ext = [System.IO.Path]::GetExtension($Name)
				NameURL = $Href
				PathURL = $PathURL
				SubDirWindows = $RelPath
			}
			If ($Recursive) {
				$Res += Get-WebServerDir-Files-Simple -FileListingURL $PathURL -Recursive:$Recursive -SubDirWindows $RelPath
			}
		}
		If ($IsFile) {
			# Write-Host ('File: {0}' -f $href)
			$Name = [System.Web.HttpUtility]::UrlDecode($Href)
			$Res += [PSCustomObject][Ordered]@{
				IsDir = $False
				IsFile = $True
				Name = $Name
				Ext = [System.IO.Path]::GetExtension($Name)
				NameURL = $Href
				PathURL = [System.Uri]::new($SiteUri, $Href).AbsoluteUri
				SubDirWindows = $SubDirWindows
			}
		}
	}
	Return $Res
}


# Lädt eine Datei von eiuner URL und speichert sie in der Zieldatei
# Vergleicht LastModified NICHT
Function Download-WebsiteFiles-Einfach($WebsiteFiles, $ZielDir) {
	# ZielDir erzeugen
	ForEach ($WebsiteFile in $WebsiteFiles | Sort RelativePathWindows) {
		If ($WebsiteFile.IsFile) {
			$ZielSubDir = Join-Path $ZielDir $WebsiteFile.SubDirWindows
			# Dir erzeugen
			If (!(Test-Path -LiteralPath $ZielSubDir -PathType Container)) {
				If ($ProgressInfo) {
					Write-Host ('  Erzeuge: {0}' -f $ZielSubDir) -ForegroundColor Cyan
				}
				$Null = New-Item -Path $ZielSubDir -ItemType Directory -Force -EA SilentlyContinue
			}
			
			# File herunterladen
			
			# Write-Host $WebsiteFile.Name
			# Return $WebsiteFile
			# Natürlich klappt UrlDecode nicht zu 100%, manche Zeichen werden ignoriert
			# $LocalFileName = [System.Web.HttpUtility]::UrlDecode($WebsiteFile.Name)
			$ZielDateiName = Join-Path $ZielSubDir $WebsiteFile.Name
			If ($ProgressInfo) {
				Write-Host ('  Lade: .\{0}\{1}' -f $WebsiteFile.SubDirWindows, $WebsiteFile.Name)
			}
			
			# Download der Datei
			Invoke-WebRequest -Uri $WebsiteFile.PathURL -OutFile $ZielDateiName
		}
	}
}


Function Is-File-Aktuell() {
	Param (
		[System.IO.FileSystemInfo]$oFileInfo,
		[DateTime]$ModificationTime,
		[Int]$deltaTSec = 5
 	)

	# Berechne das Zeitfenster
	$StartTime = $ModificationTime.AddSeconds(-$deltaTSec)
	$EndTime = $ModificationTime.AddSeconds($deltaTSec)

	If ($oFileInfo.LastWriteTime -ge $startTime -and $oFileInfo.LastWriteTime -le $endTime) {
		# Write-Host "Das Änderungsdatum der Datei '$FilePfad' liegt innerhalb des erlaubten Zeitfensters."
		Return $True
  	} Else {
		# Write-Host "Das Änderungsdatum der Datei '$FilePfad' liegt außerhalb des erlaubten Zeitfensters."
		Return $False
  }
}


# Lädt eine Datei von eiuner URL und speichert sie in der Zieldatei
# Vergleicht LastModified
Function Download-WebsiteFiles-Advanced($WebsiteFiles, $ZielDir) {
	# Alle Files durchlaufen 
	ForEach ($WebsiteFile in $WebsiteFiles | Sort RelativePathWindows) {
		If ($WebsiteFile.IsFile) {
			$ZielSubDir = Join-Path $ZielDir $WebsiteFile.SubDirWindows
			# Dir erzeugen
			If (!(Test-Path -LiteralPath $ZielSubDir -PathType Container)) {
				If ($ProgressInfo) {
					Write-Host ('  Erzeuge: {0}' -f $ZielSubDir) -ForegroundColor Cyan
				}
				$Null = New-Item -Path $ZielSubDir -ItemType Directory -Force -EA SilentlyContinue
			}

			If ($ProgressInfo) {
				Write-Host ('  .\{0}' -f $WebsiteFile.RelativePathWindows) -NoNewline
			}
			
			# Existiert die Datei?
			$ZielDateiName = Join-Path $ZielSubDir $WebsiteFile.Name

			If (Test-Path -Path $ZielDateiName) {
				$ZielFileInfo = Get-Item -LiteralPath $ZielDateiName

				# Stimmt die Websever-Zeit mit dem Timestamp der lokalen Datei überein?
				$IsAktuell = Is-File-Aktuell -oFileInfo $ZielFileInfo -ModificationTime $WebsiteFile.LastModified

				If ($IsAktuell) {
					If ($ProgressInfo) {
						Write-Host (' > ist Aktuell') -ForegroundColor Green
					}
					# Alles OK, nächste Datei
					Continue
				}
			}

			# File herunterladen
			If ($ProgressInfo) {
				Write-Host (' > Download') -ForegroundColor Magenta
			}
			
			# Download der Datei
			# Return $WebsiteFile
			Invoke-WebRequest -Uri $WebsiteFile.PathURL -OutFile $ZielDateiName

			# Das Datum vom Webserver übernehmen
			If (Test-Path -Path $ZielDateiName) {
				$ZielFileInfo = Get-Item -LiteralPath $ZielDateiName
				$ZielFileInfo.LastWriteTime = $WebsiteFile.LastModified
			} Else {
				Write-Host 'Fehler: Die Datei hat komische Sonderzeichen:' -Fore Red
				Write-Host $WebsiteFile.Name
			}
		}
	}
}




# Wandelt einen String mit einer Zahl und einem optionalen Suffix um
Function Parse-ISOSuffixNumber {
	Param(
		 [String]$InputStr
	)

	## Config
	# Definiere ein Hash-Table für Suffixe und ihre Multiplikatoren
	$suffixes = @{
		 'K' = 1e3;		'M' = 1e6;	'G' = 1e9
		 'T' = 1e12;	'P' = 1e15;	'E' = 1e18
	}

	## Prepare

	# Finde das Suffix am Ende des Strings
	if ($InputStr -match '^(.*?)([KMGTPE])?$') {
		 $numberPart = $matches[1]

		# Haben wir eine Zahl?
		$Number = 0.0
		If (![double]::TryParse($numberPart, [ref]$Number)) {
			Return $null  # Gib $null zurück, wenn $numberPart keine gültige Zahl ist
		}
		$Res = $Number

		# Das Suffix auflösen
		$Suffix = $matches[2]
		If ($Suffix) {
			# Multipliziere mit dem entsprechenden Faktor aus der Hash-Table
			$Res = $Number * $suffixes[$Suffix]
		}

		return $Res
	}
}



# Parser für:
# 	href="https://www.w3schools.com" target="_blank"
#  href=`"https://www.w3schools.com`" target='_blank'
# Liefert ein Array mir Key / Value Werten
Function Parse-HTMLAttributes {
	param(
		 [string]$AttrStr
	)

	# Definiere einen regulären Ausdruck, um die Attribute und Werte zu extrahieren
	$AttrPtrn = '(\w+)\s*=\s*("([^"]*)"|''([^'']*)'')'

	# Erstelle ein Dictionary, um die Attribute und ihre Werte zu speichern
	$attributes = @{}

	# Finde alle Attribute und Werte
	$Mtchs = [regex]::Matches($AttrStr, $AttrPtrn)
	ForEach ($match in $Mtchs) {
		 $key = $match.Groups[1].Value
		 $value = If ($match.Groups[3].Value) { $match.Groups[3].Value } else { $match.Groups[4].Value }
		 $attributes[$key] = $value
	}

	Return $attributes
}


# Liefert den Text einer named regex group
Function Get-RgxMatch-GrpVal([System.Text.RegularExpressions.Group]$Match, $GrpName) {
	If ($Match -and $Match.Success) {
		If ($Match.Groups[$GrpName].Success) {
			$Val = $Match.Groups[$GrpName].Value
			Return $Val.Trim()
		}
	}
	Return $Null
}


# Trennt:	'<a href="https://www.w3schools.com" target="_blank">Visit W3Schools</a>'
# In:
#	href="https://www.w3schools.com" target="_blank"
#	Visit W3Schools
Function Extract-HTML-AttrAndText($Text) {
	$Rgx = '<a\b(?<Attrs>.*?)>(?<Text>(.*?))<\/a>'
	$Mtchs = [regex]::Matches($Text.Trim(), $Rgx)
	If ($Mtchs.Success) {
		$Attrs = Get-RgxMatch-GrpVal $Mtchs[0] 'Attrs'
		$Text = Get-RgxMatch-GrpVal $Mtchs[0] 'Text'
		Return @($Attrs, $Text)
	}
	$Stopper = 1
	Return @($Null, $Null)
}



# Liest von einer WebServer-Dateiliste 
# alle File-Informationen, inkl. Dateigrösse und last modified Datum
# Optional: Rekursiv
# 
# Siehe auch: Get-WebServerDir-Files-Simple
Function Get-Webserver-FileList() {
	[CmdletBinding()]
	Param(
		[String]$FileListingURL,
		[Switch]$Recursive = $False, 
		# Der Relative Pfad zur initial aufgerufenen Webseite, ohne URL-Codierung
		[String]$SubDirWindows = ''
	)


	## Config
	$WebServerFileListDateFormat = 'yyyy-MM-dd HH:mm'
	# Finde alle <tr>...</tr> Tags
	$trPtrn = '<tr\b.*?>(.*?)<\/tr>'
	$tdPtrn = '<td\b.*?>(.*?)<\/td>'
	$isIconPtrn = '<img\b.*>'
	$isLinkPtrn = '<a\b.*?>(.*?)<\/a>'


	## Init
	$SiteUri = [System.Uri]::new($FileListingURL)
	$SubDirWindows = $SubDirWindows.TrimEnd('/\')

	## Main
	# Hole den Inhalt der URL
	$response = Invoke-WebRequest -Uri $FileListingURL -UseBasicParsing

	# Der HTML-Content
	$htmlContent = $response.Content

	## Alle Tabellen-Zeilen verarbeiten
	# Finde alle <tr> Elemente
	$trMatches = [regex]::Matches($htmlContent, $trPtrn)

	$AllFilesArr = @()
	ForEach ($trMatch in $trMatches) {
		$trContent = $trMatch.Groups[1].Value.Trim()
		
		## Init der Datei-Info
		$ThisFileInfo = [PSCustomObject][Ordered]@{
			IsDir = $Null
			IsFile = $Null
			Name = $Null
			Ext = $Null
			NameURL = $Null
			PathURL = $Null
			SubDirWindows = $Null
			RelativePathWindows = $Null
			Size = $Null
			LastModified = $Null
		}

		## Alle Tabellen-Spalten verarbeiten
		# Finde alle <td> Elemente innerhalb der <tr>
		$tdMatches = [regex]::Matches($trContent, $tdPtrn)
		ForEach ($tdMatch in $tdMatches) {
			$tdContent = $tdMatch.Groups[1].Value.Trim()
			
			# Entferne HTML-Tags innerhalb der <td>
			# $tdContent: <a href="/it/">Parent Directory</a>
			# $tdText   : Parent Directory
			$tdText = [regex]::Replace($tdContent, '<.*?>', '')

			$isIcon  = [Regex]::Matches($tdContent, $isIconPtrn).Success
			If ($isIcon) {
				Continue		# Nächste Spalte
			}

			# Haben wir eine URL?
			$isLink  = [Regex]::Matches($tdContent, $isLinkPtrn).Success
			If ($isLink) {
				$Stopper = 1
				$Attrs, $Text = Extract-HTML-AttrAndText $tdContent
				$LinkAttrs = Parse-HTMLAttributes $Attrs
				$Href = $LinkAttrs.Href

				## Analyse
				# Start und Ende mit /
				$IsParentDir = $Href -match '^/.*/$'
				# Ende mit /
				$IsDir = $Href -match '.*/$'
				$IsFile = !$IsParentDir -and !$IsDir

				## Verarbeiten
				If ($IsParentDir) {
					Break	# Nächste Datei
				}

				$Name = [System.Web.HttpUtility]::UrlDecode($Href)
				# Write-Host "Name: $Name"
				$PathURL = [System.Uri]::new($SiteUri, $Href).AbsoluteUri

				$ThisFileInfo.IsDir = $IsDir
				$ThisFileInfo.IsFile = $IsFile
				$ThisFileInfo.NameURL = $Href
				$ThisFileInfo.Name = $Name
				$ThisFileInfo.Ext = [System.IO.Path]::GetExtension($Name)
				If ([String]::IsNullOrWhiteSpace($SubDirWindows)) {
					$ThisFileInfo.RelativePathWindows = $Name
					} Else {
					$ThisFileInfo.RelativePathWindows = ('{0}\{1}' -f $SubDirWindows, $Name)
				}
				$ThisFileInfo.PathURL = $PathURL

				If ($IsDir) {
					# Beim Dir den / entfernen
					$ThisFileInfo.Name = $ThisFileInfo.Name.TrimEnd('/\')
					# Den relativen Pfad zum SubDir erzeugen
					$RelPath = ('{0}\{1}' -f $SubDirWindows, $ThisFileInfo.Name).TrimStart('\')
					$ThisFileInfo.SubDirWindows = $RelPath
	
					If ($Recursive) {
						$AllFilesArr += Get-Webserver-FileList -FileListingURL $PathURL -Recursive:$Recursive -SubDirWindows $RelPath
					}
		
					$Stopper = 1
				}
				If ($IsFile) {
					$ThisFileInfo.SubDirWindows = $SubDirWindows
				}

				Continue		# Nächste Spalte
			}

			# Haben wir die Dateigrösse
			$FileSize = Parse-ISOSuffixNumber $tdContent
			If ($FileSize) {
				$ThisFileInfo.Size = $FileSize
			}

			# Haben wir die Spalte Last modified?
			[DateTime]$LastModified = New-Object DateTime
			If ([DateTime]::TryParseExact($tdContent, $WebServerFileListDateFormat, [CultureInfo]::InvariantCulture, [System.Globalization.DateTimeStyles]::None, [ref]$LastModified)) {
				$ThisFileInfo.LastModified = $LastModified
			}
		}

		## Validieren, ob der Record Sinn macht
		If ($Null -ne $ThisFileInfo.Name) {
			$AllFilesArr += $ThisFileInfo
		}
		
	}

	Return $AllFilesArr | Sort RelativePathWindows
}


## Prepare


## Main


# Files lesen

# Einfache Variante

If ($False) {
	# Variante ohne LastModified Datum vom Webserver
	$WebsiteFiles = Get-WebServerDir-Files-Einfach -FileListingURL $FileListingURL -Recursive:$Recursive
	Switch ($PsCmdlet.ParameterSetName) {
		'ListOnly' {
			Return $WebsiteFiles
		}
		'Download' {
			Download-WebsiteFiles -WebsiteFiles $WebsiteFiles -ZielDir $ZielDir 
		}
	}
	
} Else {

	# Variante mit LastModified Datum vom Webserver
	$WebsiteFiles = Get-Webserver-FileList -FileListingURL $FileListingURL -Recursive:$Recursive
	Switch ($PsCmdlet.ParameterSetName) {
		'ListOnly' {
			Return $WebsiteFiles
		}
		'Download' {
			Download-WebsiteFiles-Advanced -WebsiteFiles $WebsiteFiles -ZielDir $ZielDir 
		}
	}

}



$Stopper = 1

