# Zeigt eine Liste von Checkboxen an,
# die der User aktivieren/deaktivieren kann
#
# Liefert die HashTable mit den ausgewählten Optionen,
#	wenn mind. eine Option aktiviert wurde
#
#
# Permalinks
# ••••••••••
#	Script
#		https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/TomLib/functions/public/Ask-User-Select-CheckBoxes.ps1
#
#	Func	https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/TomLib/functions/public/Ask-User-Select-CheckBoxes-Func.ps1
#
#
#
#	# Function in die aktuelle Sitzung laden: Gitlab-Restapi
#  # ••••••••••••••••••••••••••••••••••••••••••••••••••••••
#		$func = 'https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/TomLib/functions/public/Ask-User-Select-CheckBoxes-Func.ps1'
#		iex ((New-Object System.Net.WebClient).DownloadString( $func ))
#
#
#
#
# 001, 220805, tom-agplv3@jig.ch
# 002, 220814, tom-agplv3@jig.ch
# 003, 240728, Aktive ELemente sind Cyan
#
#
# Ex
#
#	# .Enabled und .Help müssen nicht zwingend definiert sein!:
#	 $Selectable = [Ordered]@{
# 		KeyOpt1 = @{
# 			Name = 'Option 1'
#	 		Enabled = $True
# 			Help = 'Hilfe zu Option 1'
# 		}
# 		KeyOpt2 = @{
# 			Name = 'Option 2'
# 			Enabled = $False
# 			Help = ''
# 		}
# 	}
#
#  # Default-Texte:
#	. 'C:\Scripts\PowerShell\Console-UI\Ask-User-Select-CheckBoxes.ps1' -Selectable $Selectable
#
#  # Eigene Texte:
#	. 'C:\Scripts\PowerShell\Console-UI\Ask-User-Select-CheckBoxes.ps1' -Selectable $Selectable -TextTitle 'Bitte auswählen, was installiert werden soll:' -TextGoOn 'Auswahl OK, Installation starten!'
#
[CmdletBinding()]
Param(
	[Parameter(Mandatory)]
	# HashTable, siehe im Beispiel
	[Object]$Selectable,
	[String]$TextTitle = 'Aktuelle Auswahl:',
	[String]$TextGoOn = 'Auswahl OK, weiter!',
	# Wenn Timeout $null, dann arbeitet der Code nicht mit $Host.UI.RawUI.ReadKey,
	# was in BoxStarter nicht funktioniert
	[Nullable[Int]]$TimeoutS,
	# Noch nicht implementiert:
	# [Switch]$SingleItemOnlySelectable
	[Switch]$NoCLS
)


$Debug = $False
Function Dbg($Log) { If ($Debug) { Write-Host -ForegroundColor Yellow -Object $Log } }



### Config
Dbg '»»»»»» 9000'

$HotKeyColor = [ConsoleColor]::Green

$QuickKeys = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
Enum eAction { Unknown; Go; Abort }


# BoxStarter hat Probleme mit CLS / Clear-Host
# Das hier klappt
$Esc = "$([char]0x1b)"
Function Cls-Ansi() {
	Write-Host "$Esc[2J$Esc[1;1H$Esc[0m"
}


# Wir wollen mit einer Ordered HashTable arbeiten,
# deshalb konvertieren wir eine HashTable um sicher zu sein
Dbg '»»»»»» 9030'
Function Create-OrderedHashTable($Src) {
	$Dst = [Ordered]@{}
	ForEach ($Item in $Src.GetEnumerator()) { 
		$Dst[$Item.Key] = $Item.Value 
	}
	Return $Dst
	
	# Archiv
	# Sind die Elemente bereits HashTable-Objekte?
	$FirstItem = $Src | select -first 1
	If ($FirstItem) {
		If ( $FirstItem.GetType().Name -eq 'Hashtable' ) {
			ForEach ($Item in $Src.GetEnumerator()) { 
				$Dst[$Item.Key] = $Item.Value 
			}
		} Else {
		}
	} Else {
		# Wir haben keine Elemente
		Return $Dst
	}
}


# Das Menü mit den Checkboxen anzeigen
Dbg '»»»»»» 9040'
Function Display-SelectableItems() {
	[CmdletBinding()]
	Param(
		[Parameter(Mandatory)]
		[Collections.Specialized.OrderedDictionary]$Selectable,
		[ConsoleColor]$HotKeyColor = [ConsoleColor]::Green,
		[Switch]$ShowHelp
	)
	$QuickKeys = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	$Cnt = -1
	ForEach ($Item in $Selectable.GetEnumerator()) {
		$Cnt++
		# Write-Host "$($Item.Name): $($Item.Value)"
		$ItemProps = $Item.Value
		Write-Host ('{0}' -f $QuickKeys[$Cnt]) -ForegroundColor $HotKeyColor -NoNewline
		If ($ItemProps.Enabled) {
			Write-Host ':  [x]' -NoNewline
			Write-Host (' {0}' -f $ItemProps.Name) -ForegroundColor Green
		} Else {
			Write-Host ':  [ ]' -NoNewline
			Write-Host (' {0}' -f $ItemProps.Name)
		}

		If ($ShowHelp -and ([String]::IsNullOrWhiteSpace($ItemProps.Help)) -eq $False) {
			Write-Host ('{0}{1}' -f (' '*8), $ItemProps.Help)
		}
	}
}


# Toggelt das Enable-Propery des gewälten Elements
Dbg '»»»»»» 9050'
Function Switch-SelectableItem() {
	[CmdletBinding()]
	Param(
		[Parameter(Mandatory)]
		[Collections.Specialized.OrderedDictionary]$Selectable,
		[Nullable[Int]]$Idx
	)
	$Selectable[$Idx].Enabled = !$Selectable[$Idx].Enabled
}


# Liefert true, wenn ein Element in $Selectable ein .Help-Prop definiert hat
Dbg '»»»»»» 9060'
Function Has-SelectableItems-HelpTexts($Selectable) {
	ForEach($Key in $Selectable.Keys) {
		If ([String]::IsNullOrWhiteSpace($Selectable[$Key].Help) -eq $False) {
			Return $True
		}
	}
	Return $False
}


# Liefert true,
# wenn mind. ein Element in $Selectable aktiviert ist
Dbg '»»»»»» 9070'
Function Has-ItemsSelected($Selectable) {
	ForEach($Key in $Selectable.Keys) {
		If ($Selectable[$Key].Enabled -eq $True) {
			Return $True
		}
	}
	Return $False
}

Function Count-ItemsSelected($Selectable) {
	$AnzahlEnabled = $Selectable.Values | ? { $_.Enabled -eq $True } | Measure-Object | Select -ExpandProperty Count
	Return $AnzahlEnabled
}


# Stellt sicher, dass max. 1 Element aktiviert ist
Function Assert-OnlyOne-ItemsSelected {
	Param (
		[Parameter(Mandatory)]
		[hashtable]$Selectable
	)
	$firstEnabledFound = $false
	ForEach ($key in $Selectable.Keys) {
		If ($Selectable[$key].Enabled -eq $True) {
			If (-not $firstEnabledFound) {
				$firstEnabledFound = $True
			} Else {
				$Selectable[$key].Enabled = $False
			}
		}
	}
	return $Selectable
}


Function Toggle-All($Selectable) {
	If (Has-ItemsSelected -Selectable $Selectable) {
		# Mind. 1 Element ist aktiviert - alle deaktivieren
		ForEach($Key in $Selectable.Keys) {
			$Selectable[$Key].Enabled = $False
		}
	} Else {
		# Alle aktivieren
		ForEach($Key in $Selectable.Keys) {
			$Selectable[$Key].Enabled = $True
		}
	}
}


### Prepare
Dbg '»»»»»» 9080'
$ScriptSelectable = Create-OrderedHashTable $Selectable

# Die Zeichen bestimmen, die der User wählen darf
Dbg '»»»»»» 9090'
$SelectableQuickKeys = $QuickKeys.Substring(0, $ScriptSelectable.Count)

# Noch nicht implementiert:
	# Wenn nur ein Element gewählt werden darf und mehrere aktiviert sind,
	# dann nur das erste auswählen
	# If ($SingleItemOnlySelectable) {
	# 	$ScriptSelectable = Assert-OnlyOne-ItemsSelected $ScriptSelectable
	# }


# Wartet eine bestimmte Zeit auf einen Tastendruck
# und liefert $null oder [KeyInfo] zurück
# !Q https://stackoverflow.com/a/52546471/4795779
Function GetKeyPress([Nullable[Int]]$TimeOutSec) {
	If ($TimeOutSec -ne $null) {
		$Key = $null
		Start-Sleep -MilliS 150
		$Host.UI.RawUI.FlushInputBuffer()
		$Cnt = $TimeOutSec * 1000 / 250
		While($Key -eq $null -and ($TimeOutSec -eq 0 -or $Cnt-- -gt 0)) {
			If (($TimeOutSec -eq 0) -or $Host.UI.RawUI.KeyAvailable) {
				$Key = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyUp')
			} Else { Start-Sleep -MilliS 250 }
		}
		Return $Key
	} Else {
		Return [Console]::ReadKey($True)
	}
}



### Die Functions in den globalen Space exportieren, wenn sie noch nicht existieren,
### so dass diese Function auf mit Ask-User-Select-CheckBoxes-Func.ps1 nutzbar sind

#	# Prüfe, ob die Funktion im globalen Bereich existiert
#	If (-not (Get-Command -Name 'Has-ItemsSelected' -CommandType Function -ErrorAction SilentlyContinue)) {
#		# Exportiere die Funktion in den globalen Geltungsbereich
#		Set-Item -Path Function:\Has-ItemsSelected -Value ${Function:Has-ItemsSelected} -Option Constant
#	}
#
#	# Prüfe, ob die Funktion im globalen Bereich existiert
#	If (-not (Get-Command -Name 'Count-ItemsSelected' -CommandType Function -ErrorAction SilentlyContinue)) {
#		# Exportiere die Funktion in den globalen Geltungsbereich
#		Set-Item -Path Function:\Count-ItemsSelected -Value ${Function:Count-ItemsSelected} -Option Constant
#	}



### Main

# Prüfen, ob Hilfstexte vorhanden sind
Dbg '»»»»»» 9100'
$Offerhelp = Has-SelectableItems-HelpTexts -Selectable $ScriptSelectable
Dbg '»»»»»» 9110'
$ShowHelpTexts = $False


Do {
	If ($NoCLS) { 
		Write-Host "`n"
	} Else {
		Cls-Ansi 
	}
	
	Dbg '»»»»»» 9120'
	Write-Host $TextTitle -ForegroundColor Yellow
	If ($TimeOutSec -ne $null) {
		Write-Host -ForegroundColor Red "Nach $($TimeoutS)s wird die Standardinstallation gestartet"
	}

	Dbg '»»»»»» 9130'
	Display-SelectableItems -Selectable $ScriptSelectable -ShowHelp:$ShowHelpTexts -HotKeyColor $HotKeyColor

	Dbg '»»»»»» 9140'
	Write-Host "`nBitte wählen zum Toggeln: " -NoNewline
	Write-Host "$($SelectableQuickKeys.ToCharArray() -join ',')" -ForegroundColor $HotKeyColor
	If ($Offerhelp) {
		Write-Host '{F1}' -ForegroundColor $HotKeyColor -NoNewline
		Write-Host ': Hilfstexte anzeigen | ' -NoNewline
	}
	Write-Host '{Esc}' -ForegroundColor $HotKeyColor -NoNewline
	Write-Host ': Abbruch | ' -NoNewline
	Write-Host '*' -ForegroundColor $HotKeyColor -NoNewline
	Write-Host (': Alle wählen/abwählen | ' -f $TextGoOn) -NoNewline
	Write-Host '{Enter}' -ForegroundColor $HotKeyColor -NoNewline
	Write-Host (': {0} ' -f $TextGoOn) -NoNewline

	# Erkennt auch Shift & Co
	# $Host.UI.RawUI.ReadKey()

	# Start-Sleep -MilliS 750
	# $Host.UI.RawUI.FlushInputBuffer()
	Dbg '»»»»»» 9150'
	# $Key = [Console]::ReadKey($True)
	$Key = GetKeyPress -TimeOutSec $TimeoutS

	Dbg '»»»»»» 9160'
	$Action = [eAction]::Unknown
	If ($TimeOutSec -ne $null) {
		Switch($Key.VirtualKeyCode) {
			{ @(([ConsoleKey]::OemPeriod), ([ConsoleKey]::Enter)) -contains $_ } {
				# OK, Weiter
				Dbg '»»»»»» 9170'
				$Action = [eAction]::Go
			}
			([ConsoleKey]::Multiply) {
				# Alle aktivieren / deaktivieren
				Dbg '»»»»»» 9175'
				Toggle-All -Selectable $Selectable
			}
			([ConsoleKey]::Escape) {
				# OK, Abbruch
				Dbg '»»»»»» 9180'
				$Action = [eAction]::Abort
			}
			Default {
				# Hilfe?
				Dbg '»»»»»» 9190'
				If ($Offerhelp -and
						($Key.Character -eq '?' -or $Key.VirtualKeyCode -eq ([ConsoleKey]::F1))) {
					$ShowHelpTexts = !$ShowHelpTexts
				}

				# Menü-Wahl?
				# Key gültig?
				If ([Int[]]($SelectableQuickKeys.ToCharArray()) -Contains [Int]"$($Key.Character)".ToUpper().ToCharArray()[0]) {
					$SelectedIndex = $SelectableQuickKeys.IndexOf( "$([Char]$Key.Character)".ToUpper() )
					# Write-Host $SelectedIndex
					Switch-SelectableItem -Selectable $ScriptSelectable -Idx $SelectedIndex
				}
			}
		}
	} Else {
		Switch($Key.Key) {
			{ @(([ConsoleKey]::OemPeriod), ([ConsoleKey]::Enter)) -contains $_ } {
				# OK, Weiter
				Dbg '»»»»»» 9170'
				$Action = [eAction]::Go
			}
			([ConsoleKey]::Multiply) {
				# Alle aktivieren / deaktivieren
				Dbg '»»»»»» 9175'
				Toggle-All -Selectable $Selectable
			}
			([ConsoleKey]::Escape) {
				# OK, Abbruch
				Dbg '»»»»»» 9180'
				$Action = [eAction]::Abort
			}
			Default {
				# Hilfe?
				Dbg '»»»»»» 9190'
				If ($Offerhelp -and
					($Key.KeyChar -eq '?' -or $Key.Key -eq ([ConsoleKey]::F1))) {
					$ShowHelpTexts = !$ShowHelpTexts
				}

				# Menü-Wahl?
				# Key gültig?
				# If ([Int[]]($SelectableQuickKeys.ToCharArray()) -Contains [Int]$Key.KeyChar) {
				If ([Int[]]($SelectableQuickKeys.ToCharArray()) -Contains [Int]"$($Key.KeyChar)".ToUpper().ToCharArray()[0]) {
					$SelectedIndex = $SelectableQuickKeys.IndexOf( "$([Char]$Key.KeyChar)".ToUpper() )
					# Write-Host $SelectedIndex
					Switch-SelectableItem -Selectable $ScriptSelectable -Idx $SelectedIndex
				}
			}
		}
	}
} While ( @( ([eAction]::Go), ([eAction]::Abort) -notcontains $Action ) )
Write-Host ''


Dbg '»»»»»» 9200'
Switch ($Action) {
	([eAction]::Go) {
		# Auswahl fertig
		# Wenn irgend ein Element aktiviert ist
		If (Has-ItemsSelected $ScriptSelectable) {
			Return $ScriptSelectable
		} Else {
			# Kein Element aktiviert
			Return $Null
		}
	}
	([eAction]::Abort) {
		Write-Host 'Abbruch' -ForegroundColor Red
		Return $Null
	}
	Default {
		Write-Host "Unbekannter Befehl: $($Action)"
		Return $Null
	}
}

