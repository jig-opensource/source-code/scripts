
Getting started
	Diese Files ins Verzeichnis vom Script kopieren, das die LibGists nützen will:
		c:\Scripts\PowerShell\-Gist\LibGist-Get-Gist.ps1
		c:\Scripts\PowerShell\-Gist\LibGist-TomBasicTools.ps1
		c:\Scripts\PowerShell\-Gist\LibGist-Use-Gists.ps1

	Am Beispiel von
		Ex-LibGist-Usage.ps1
		LibGist einbinden
		
	Allenfalls die LibGist manuell aktualisieren
		(wird ohnehin automatisch gemacht werden)
		
		cd ins Script-Verzeichnis
		.\LibGist-Get-Gist.ps1 -UpdateLocalFiles -Verbose
		
	Allenfalls alle vorhandenen LibGist Files herunterladen
		cd ins Script-Verzeichnis
		.\LibGist-Get-Gist.ps1 -DownloadFile -Verbose



Idee
	In den Gists sind ps2 Files mit standard PowerShell Funktionen
	Scripts können diese Standardfunktionen einfach einbinden
	Sobald die Gists Files aktualisiert werden,
		aktualisieren sich die lokalen Kopien automatisch
	

Lösung
	Ex-LibGist-Usage.ps1
		Dieses PS Script nützt die Gist Standardfunktionen
		
		Dazu bindet es das Script ein:
			LibGist-Use-Gists.ps1
		
		Und nützt dann diese Funktionen:
			LibGist-Update-GistFiles
				Aktualisiert alle 30 Tage die lokelen Kopien der Gists
			LibGist-Load-Gists
				Bindet die Gists als Standardfunktionen ins aktuelle Script ein


	LibGist-Get-Gist.ps1
		Hilfs-Script
		Listet Gists auf
		Lädt Gists herunter
		Aktualisiert veraltete lokale Kopien von Gists-Files
				
	
	LibGist-Use-Gists.ps1
		Bietet verschiedene Funktionen
			. 'c:\Scripts\PowerShell\-Gist\LibGist-Use-Gists.ps1'

			#   • LibGist-Update-GistFiles
			#       Aktualisiert die LibGists alle (per default) 30 Tage
			#
			#   • LibGist-Load-Gists
			#       Bindet die LibGists in ein Script ein
			#
			#   • LibGist-List-Files
			#       Listet die verfügbaren LibGist Files
			#
			#   • LibGist-Download-Files
			#       Lädt alle verfügbaren LibGist Files herunter

	



