# KH Gist
- Q: https://help.github.com/en/articles/about-gists
- **Meine Gists**: https://gist.github.com/schittli

# Security
https://help.github.com/en/articles/about-gists#secret-gists

Secret gists
- **Secret gists don't show up in Discover and are not searchable.**
- Use them to jot down an idea that came to you in a dream, create a to-do list, or prepare some code or prose that's not ready to be shared with the world.
- **After creating a gist, you cannot convert it from public to secret.**

- **Secret gists aren't private**
- If you send the URL of a secret gist to a friend, they'll be able to see it.
- However, if someone you don't know discovers the URL, they'll also be able to see your gist.
- » If you need to keep your code away from prying eyes, you may want to create a private repository instead.
