﻿# Aktualisiert alle im aktuellen Dir gefundenen LibGist Files
#	oder im angegebenen Zielverzeichnis
#
# Wenn im Zielverzeichnis keine LibGist Files vorhanden sind
#	dann werden alle heruntergeladen
#
#
# Das Script-Dir aktualisieren
#	c:\Scripts\PowerShell\-Gist\Update-LibGist-Files.ps1 -UpdateScriptDir
#
# Das Workingdir aktualisieren
#	c:\Scripts\PowerShell\-Gist\Update-LibGist-Files.ps1 -UpdateWorkingDir
#
# Das Zielverzeichnis aktualisieren
#	c:\Scripts\PowerShell\-Gist\Update-LibGist-Files.ps1 -ZielDir 'c:\Scripts\PowerShell\-Gist\220206 140922-test\'
#
#
# 001, 220127, tom-agplv3@jig.ch
#

[CmdletBinding()]
Param (
	[Parameter(Mandatory, ParameterSetName='ZielDir')]
	[String]$ZielDir,
	[Parameter(Mandatory, ParameterSetName='UpdateScriptDir')]
	[Switch]$UpdateScriptDir,
	[Parameter(Mandatory, ParameterSetName='UpdateWorkingDir')]
	[Switch]$UpdateWorkingDir
)



### Config

$ScriptDir = [IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Path)
$WorkingDir = (Get-Location).Path

If ($UpdateScriptDir) {
	$ZielDir = $ScriptDir
} ElseIf ($UpdateWorkingDir) {
	$ZielDir = $WorkingDir
} Else {
	If ([String]::IsNullOrEmpty($ZielDir)) {
		$ZielDir = $ScriptDir
		Write-Error '-ScriptDir darf nicht leer sein'
	} Else {
		$null = New-Item -ItemType Directory -Path $ZielDir -Force -ErrorAction SilentlyContinue
	}
}



### Prepare

# Scriptname für Use-Gists berechnen
$LibGistUseGists_ps1 = 'LibGist-Use-Gists.ps1'
$LibGistUseGists_ps1 = Join-Path $ScriptDir $LibGistUseGists_ps1

# Use-Gists einbinden
. $LibGistUseGists_ps1



## Main

# Alle lokalen LibGist Files aktualisieren
$ResUpdate = LibGist-Update-GistFiles -Now -ZielDir $ZielDir -Verbose
