# Lädt Gists herunter
#	!9 Der Download wird nur gestartet,
#		Wenn die lokale Kopie veraltet ist
#
# -force forciert den Download
#
# Ex
#
#	List
#		Alle public Gists von schittli
#			c:\Scripts\PowerShell\-Gist\LibGist-Get-Gist.ps1 -ListPublicGistsFiles
#
#		… eines Users
#			c:\Scripts\PowerShell\-Gist\LibGist-Get-Gist.ps1 -ListPublicGistsFiles -GitHubUserName 'username'
#
#		Die Namen aller public Gists
#			c:\Scripts\PowerShell\-Gist\LibGist-Get-Gist.ps1 -ListPublicGistsFiles | select Descr*
#
#		Die Files aller public Gists
#			c:\Scripts\PowerShell\-Gist\LibGist-Get-Gist.ps1 -ListPublicGistsFiles | select -ExpandProperty File*
#
#	Download von Files
#
#		Alle Files
#			aller public Gists von schittli
#			ins Arbeitsverzeichnis
#				c:\Scripts\PowerShell\-Gist\LibGist-Get-Gist.ps1 -DownloadFile -Verbose
#
#		Die Datei File1.txt
#			ins Arbeitsverzeichnis
#		!9	Wenn ein Gist mehrere Files hat, dann werden immer alle Files heruntergeladen
#				Auch wenn nur ein Dateiname angegeben wird
#				c:\Scripts\PowerShell\-Gist\LibGist-Get-Gist.ps1 -DownloadFile -FileNames File1.txt -Verbose
#
#		… in ein Zielverzeichnis
#			c:\Scripts\PowerShell\-Gist\LibGist-Get-Gist.ps1 -DownloadFile -FileNames File1.txt -ZielDir 'C:\…\' -Verbose
#
#
# 001, 220126, tom-agplv3@jig.ch
# 002, 220127, tom-agplv3@jig.ch
#	ZielDir wird bei Bedarf erzeugt
# 003, 220127
#	DLFile gibt neu true zurück, wenn Files aktualisiert wurden
# 004, 220127
#	Neu: -UpdateLocalFiles
# 005, 220206
#   Download konvertiert in Windows EOL
# 006
#	Kommentiert
# 007
#	Fixed: Created / Updated DateTime handling
# 008
#	Fixed
#		Method invocation failed because [System.String] does not contain a method named 'ToLocalTime'
#
#		$Script:GistsUrl_ListPublicGists
#		> $GistsUrl_ListPublicGists
#
#		# Wenn das Script ScriptDir schon definiert, nützen wir es
#		If ([String]::IsNullOrWhiteSpace($Script:ScriptDir)) {
#			$ScriptDir = [IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Path)
#		}
# 009
# 	Neu: NoReturnInfo
# 	Neu: ReturnErrorCode
# 		Error
# 			0	Bereits aktuell / Alles OK
# 			1	Aktualisiert
# 			2	GitHub nicht erreichbar
# 		 100	Allgemeiner Fehler
#
#	Anstatt » neu >



# Resultat
#	$True		Files wurden aktualisiert
#	$Files		Files waren bereits aktuell
[CmdletBinding(DefaultParameterSetName = 'None')]
Param (
	[Parameter(ParameterSetName = 'UpdateLocalFiles')]
	# Aktualisiert nur bereits existierende Files
	[Switch]$UpdateLocalFiles,

	[Parameter(ParameterSetName = 'List')]
	# Listet alle public Gists
	[Switch]$ListPublicGistsFiles,

	[Parameter(Position = 0, Mandatory, ParameterSetName = 'GetFileData')]
	# Holt für die Files in $FileNames die Daten
	[Switch]$GetFile_GistInfo,

	[Parameter(Position = 0, Mandatory, ParameterSetName = 'DLFiles')]
	[Alias('UpdateFile')]
	[Switch]$DownloadFile,

	[Parameter(ParameterSetName = 'DLFiles')]
	[Parameter(ParameterSetName = 'GetFileData')]
	[String[]]$FileNames,

	[Parameter(ParameterSetName = 'DLFiles')]
	[String]$ZielDir,

	[Parameter(ParameterSetName = 'DLFiles')]
	# Die Files zwingend neu herunteröaden
	# Sonst werden sie nur dann heruntergeladen,
	# wenn die lokalen Files veraltet sind
	[Switch]$Force,

	[Parameter(ParameterSetName = 'DLFiles')]
	# Wenn Verfügbar, dann können vorhandene GistInfos bereits mitgegeben werden
	[PSCustomObject[]]$GistInfo,

	[Parameter(ParameterSetName = 'DLFiles')]
	[Parameter(ParameterSetName = 'GetFileData')]
	[Parameter(ParameterSetName = 'List')]
	# eg '*#DownloadLib*' '*#LibGist*'
	[String]$DescriptionFilter,

	[String]$GitHubUserName = 'schittli',

	[Switch]$ReturnErrorCode,
	[Switch]$NoReturnInfo
)



### Config

$GistsUrl_ListPublicGists = "https://api.github.com/users/$GitHubUserName/gists"

# Wenn das Script ScriptDir schon definiert, nützen wir es
If ([String]::IsNullOrWhiteSpace($Script:ScriptDir)) {
	$ScriptDir = [IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Path)
}
$GistsFilesPattern = 'LibGist-*.ps1'


Function IsNullOrEmpty([String]$Str) {
	[String]::IsNullOrWhiteSpace($Str)
}

Function Has-Value($Test) {
	-not [String]::IsNullOrWhiteSpace($Test)
}


# Ein schnelles Ping
# 220127
Function Test-Connection-Fast {
	<#
	.DESCRIPTION
		Test-ComputerConnection sends a ping to the specified computer or IP Address specified in the ComputerName parameter. Leverages the System.Net object for ping
		and measures out multiple seconds faster than Test-Connection -Count 1 -Quiet.
	.PARAMETER ComputerName
		The name or IP Address of the computer to ping.
	.EXAMPLE
		Test-ComputerConnection -ComputerName "THATPC"
		Tests if THATPC is online and returns a custom object to the pipeline.
	.EXAMPLE
		$MachineState = Import-CSV .\computers.csv | Test-ComputerConnection -Verbose

		Test each computer listed under a header of ComputerName, MachineName, CN, or Device Name in computers.csv and
		and stores the results in the $MachineState variable.
	.NOTES
		001, 220127
	#>
	[CmdletBinding()]
	Param (
		[Parameter(Mandatory, ValueFromPipeline, ValueFromPipelinebyPropertyName)]
		[Alias('CN', 'MachineName', 'Device Name')]
		[String]$ComputerName,
		[Int]$TimeoutMs = 500,
		[Int]$NoOfPings = 5,
		# Versucht stilldie Pings und bricht bei Erfolg ab
		[Switch]$TestForSuccess,
		# Maximum number of times the ICMP echo message can be forwarded before reaching its destination.
		# Results in: TtlExpired
		# Range is 1-255. Default is 64
		[Int]$TTL = 64,
		# Buffer used with this command. Default 32
		[Int]$Buffersize = 32,
		# Wenn true und das Paket ist für einen Router oder gateway zum Host
		# grösser als die MTU: Status: PacketTooBig
		[Switch]$DontFragment = $false,
		[Switch]$PassThru
	)

	Begin {
		$options = New-Object System.Net.Networkinformation.PingOptions
		$options.TTL = $TTL
		$options.DontFragment = $DontFragment
		$buffer=([System.Text.Encoding]::ASCII).getbytes('a' * $Buffersize)
		$ping = New-Object System.Net.NetworkInformation.Ping

		# mind. 1 Ping
		$NoOfPings = [Math]::Max($NoOfPings, 1)
		$DestinationReachedOnce = $False
		$ResPing = @()
	}

	Process {
		For ($Cnt = 0; $Cnt -lt $NoOfPings; $Cnt++) {
			Try {
				$reply = $ping.Send($ComputerName, $TimeoutMs, $buffer, $options)
			} Catch {
				$ErrorMessage = $_.Exception.Message
				# 230409 225955
				# Write-Host ($_ | Out-String)
				$Res = [PSCustomObject][Ordered]@{
					# 230409 225955
					# Message = ($_.ToString())
					ComputerName = $ComputerName
					Success = $False
					Timeout = $True
					Status = $ErrorMessage
				}
			}
			If ($reply.status -eq 'Success') {
				$Res = @{
					ComputerName = $ComputerName
					Success = $True
					Timeout = $False
					Status = $reply.status
				}
			} Else {
				$Res = [PSCustomObject][Ordered]@{
					ComputerName = $ComputerName
					Success = $False
					Timeout = $True
					Status = $reply.status
				}
			}
			If ($Res.Success) { $DestinationReachedOnce = $True }

			If ($TestForSuccess) {
				# Die Resultate sammeln
				$ResPing +=$Res
				# Bei Erfolg stoppen
				If ($DestinationReachedOnce) {
					If ($PassThru) {
						Return $ResPing
					} Else {
						Return $True
					}
				}
			} Else {
				If ($PassThru) {
					$Res
				} Else {
					$Res.Success
				}
			}
		}
		If ($TestForSuccess) {
			If ($PassThru) {
				Return $ResPing
			} Else {
				Return $DestinationReachedOnce
			}
		}
	}
	End{}
}


# Verarbeitet in der Gists-Liste
# das .files Property,
#	damit die Daten PS-like zur Verfügung stehen
Function Patch-Gist-FilesProp($GistList) {
	$Res = @()
	ForEach($GistInfo in $GistList) {
		$FileNames = $GistInfo.files | Get-Member -MemberType NoteProperty | select -ExpandProperty Name
		$Files = @()
		ForEach($FileName in $FileNames) {
			# Die HashTable mit den Datei-Eigenschaften
			$oFileData = ($GistInfo.files | Select -ExpandProperty $FileName)

			# Alle Dateieigenschaften abrufen
			$FilePropertyNames = $oFileData | Get-Member -MemberType NoteProperty | select -ExpandProperty Name

			# Die Dateieigenschaften sammeln
			$ResFileDetails = [Ordered]@{}
			ForEach($FilePropertyName in $FilePropertyNames) {
				$ResFileDetails += @{ $FilePropertyName = ($oFileData | select -ExpandProperty $FilePropertyName) }
			}
			$Files += [PSCustomObject]$ResFileDetails
		}

		# $GistInfo erweitern
		$GistInfo | Add-Member -MemberType NoteProperty -Name CreatedLocalTime -Value ((Get-Date $GistInfo.created_at).ToLocalTime())
		$GistInfo | Add-Member -MemberType NoteProperty -Name UpdatedLocalTime -Value ((Get-Date $GistInfo.updated_at).ToLocalTime())
		# Das dumm definierte Files property überschreiben
		$GistInfo | Add-Member -MemberType NoteProperty -Name Files -Value $Files -Force
		$Res += $GistInfo
	}
	Return $Res
}



# Holt die Liste der Gists
# Patcht das .files Property,
#	damit die Daten PS-like zur Verfügung stehen
Function Get-Gist-List() {
	[CmdletBinding()]
	Param (
		# eg '*#DownloadLib*' '*#LibGist*'
		[String]$DescriptionFilter
	)

	# https://docs.github.com/en/rest/reference/gists
	$Body = @{
		per_page = 100
	}

	$GistsInfo = Invoke-RestMethod -Method Get -Uri $GistsUrl_ListPublicGists -Verbose:$False -Body $Body
	If (Has-Value $DescriptionFilter) {
		$GistsInfo = $GistsInfo | ? Description -like $DescriptionFilter
	}
	$GistsInfo = Patch-Gist-FilesProp -GistList $GistsInfo
	$GistsInfo
}



# Lädt alle Gists herunter
# Filtert das Resultat allenfalls
# 	nach dem $DescriptionFilter
# 	nach dem $FileName
Function Get-Gists-FileMetadata() {
	[CmdletBinding()]
	Param(
		# eg '*#DownloadLib*' '*#LibGist*'
		[String]$DescriptionFilter,
		[String[]]$FileNames
	)
	$GistList = Get-Gist-List -DescriptionFilter:$DescriptionFilter -Verbose:$Verbose
	If ($null -ne $FileNames) {
		$Filtered = @()
		ForEach($FileName in $FileNames) {
			# Ein Gist kann mehrere Files haben
			# Hier wird das Gist zurückgegeben, das das File enthält
			$Filtered += $GistList | ? { $_.Files.filename -eq $FileName }
		}
		$GistList = $Filtered
	}
	Return $GistList
}



# Liefert True, wenn $Path relativ ist
Function Is-Path-Relative($Path) {
	If ([String]::IsNullOrEmpty($Path)) { Return }

	If ($Path.StartsWith('\\')) { Return $False }
	If ($Path.StartsWith('.\')) { Return $True }
	If ($Path.Contains(':')) { Return $False }
	Return $true
}

Function Is-Path($Path) {
	If ([String]::IsNullOrEmpty($Path)) { Return }
	If ($Path.StartsWith('\\')) { Return $True }
	If ($Path.StartsWith('.\')) { Return $True }
	If ($Path.Contains(':')) { Return $True }
	Return $False
}


# Überschreiben für Verbose, weil ich kein Prefix und Farben will
Function Write-Verbose() {
	If ($VerbosePreference) { Write-Host @Args }
}


Function Test-Host-Reachable([URI]$Uri) {
	Test-Connection-Fast $Uri.Host -TimeoutMs 500 -TestForSuccess -NoOfPings 3
}


# berechnet das absolute Zielverzeichnis
# und stellt sicher, dass es existiert
Function Calc-ZielDir($ZielDir, $DefaultAbsoluteDir) {
	# Allenfalls das Zielverzeichnis berechnen
	If (Has-Value $ZielDir) {
		If (Is-Path-Relative $ZielDir) {
			$ZielDir = Join-Path $DefaultAbsoluteDir $ZielDir
		}
	} Else {
		# Das File ins Arbeitsverzeichnis laden
		$ZielDir = $DefaultAbsoluteDir
	}

	# Sicherstellen, dass das Verzeichnis existiert
	New-Item -Path $ZielDir -ItemType Directory -ErrorAction SilentlyContinue | Out-Null

	Write-Verbose "Arbeitsverzeichnis:" -ForegroundColor Yellow
	Write-Verbose "$($ZielDir)`n"
	Return $ZielDir
}



# Sehr schnell, optional mit der expliziten Codierung
# http://www.happysysadm.com/2014/10/reading-large-text-files-with-powershell.html
Function Get-Content-Fast($LiteralPath, [Text.Encoding]$Encoding = $null) {
	If ($Encoding) {
		[System.IO.File]::ReadAllText($LiteralPath, $Encoding)
	} Else {
		[System.IO.File]::ReadAllText($LiteralPath)
	}
}


# Sehr schnell, optional mit der expliziten Codierung
# 191028
# 220812: Neu: -WriteBOM
Function Write-Content-Fast() {
	Param (
		[Parameter(Position=0, Mandatory)]
		$LiteralPath,

		[Parameter(Position=1, Mandatory, ValueFromPipeline)]
		$Content,

		[Parameter(Position=2)]
		[Text.Encoding]$Encoding,
		[Switch]$WriteBOM,
		[Switch]$Overwrite=$True
	)

	Begin {
		If (Test-Path -LiteralPath $LiteralPath) {
			If ($Overwrite) {
				Remove-Item -Force -LiteralPath $LiteralPath
			} Else {
				Return
			}
		}
	}

	Process {
		If ($Encoding -eq [Text.Encoding]::UTF8) {
			If ($WriteBOM) {
				$Encoding = [System.Text.UTF8Encoding]::new($true)
			} Else {
				$Encoding = [System.Text.UTF8Encoding]::new($false)
			}
		}
		
		Switch ($Content.GetType()) {
			([Object[]]) {
				If ($Encoding) {
					[System.IO.File]::WriteAllLines($LiteralPath, $Content, $Encoding)
				} Else {
					[System.IO.File]::WriteAllLines($LiteralPath, $Content)
				}
			}
			([String]) {
				If ($Encoding) {
					[System.IO.File]::WriteAllText($LiteralPath, $Content, $Encoding)
				} Else {
					[System.IO.File]::WriteAllText($LiteralPath, $Content)
				}
			}
		}
	}

	End {}
}


# !Q
# 	https://en.wikipedia.org/wiki/List_of_file_signatures
# 	https://www.garykessler.net/library/file_sigs.html
# 210406 222019
# 	Gibt direkt das Encoing-Objekt zurück
#
# c:\Scripts\PowerShell\-Tests\Get-Encodings.ps1
Function Get-File-Encoding([String]$LiteralPath) {
<#
.SYNOPSIS
	Bestimmt die Codierung der angegebenen Datei
.PARAMETER FileName
	Der Name der Datei, deren Codierung bestimmt werden soll
.EXAMPLE
	$Encoding = Get-FileEncoding 'c:\temp\abc.txt'
#>

	If (!(Test-Path -LiteralPath $LiteralPath) -or (Get-Item -Path $LiteralPath).Length -eq 0) {
		# Datei existiert nicht oder ist leer
		Return $null
	}

	Switch ($PSVersionTable.PSVersion.Major) {
		5 {
			[Byte[]]$byte = Get-Content -Encoding byte -ReadCount 4 -TotalCount 4 -Path $LiteralPath
		}
		Default {
			[Byte[]]$byte = Get-Content -AsByteStream -ReadCount 4 -TotalCount 4 -Path $LiteralPath
		}
	}

	# Debug: $byte als Hex anzeigen
	# [System.BitConverter]::ToString($byte)
	If ($byte[0] -eq 0xef -and $byte[1] -eq 0xbb -and $byte[2] -eq 0xbf) {
		# UTF8, BOM
		# Return 'UTF8' mit BOM
		# Testen der BOM:
		# 	(New-Object System.Text.UTF8Encoding $True).GetPreamble()
		Return (New-Object System.Text.UTF8Encoding $True)
	} ElseIf ($byte[0] -eq 0xfe -and $byte[1] -eq 0xff) {
		# Return 'BigEndianUnicode'
		# UTF16 or UnicodeEncoding BigEndian, BOM
		Return (New-Object System.Text.UnicodeEncoding $True, $True)
	} ElseIf ($byte[0] -eq 0xff -and $byte[1] -eq 0xfe) {
		# Return 'Unicode'
		# UTF16 or UnicodeEncoding LittleEndian, BOM
		Return (New-Object System.Text.UnicodeEncoding $False, $True)
	} ElseIf ($byte[0] -eq 0 -and $byte[1] -eq 0 -and $byte[2] -eq 0xfe -and $byte[3] -eq 0xff) {
		# Return 'UTF32'
		# UTF32Encoding BigEndian, BOM
		Return (New-Object System.Text.UTF32Encoding $True, $True)
		# } ElseIf ($byte[0] -eq 0x2b -and $byte[1] -eq 0x2f -and $byte[2] -eq 0x76) {
		# 	Return 'UTF7'
	}
	# Default
	# Return 'ASCII'
	# 	Return (New-Object System.Text.ASCIIEncoding)

	# 'UTF8, NoBOM'
	Return (New-Object System.Text.UTF8Encoding $False)
}



# Konvertiert das File-EOL
Function Convert-File-EOL() {
	[CmdletBinding()]
	Param(
		[Parameter(Mandatory)]
		[String]$Filename,
		[Parameter(Mandatory,Position=1)]
		[ValidateSet(IgnoreCase, 'Win','Mac','Linux','Unix')]
		[String]$LineEnding,
		[Text.Encoding]$Encoding
	)

	$EolWin = "`r`n"
	$EolMac = "`r"
	$EolLinux = "`n"

	Switch ($LineEnding) {
		'Win'  	{ $NewEol = $EolWin }
		'Mac'  	{ $NewEol = $EolMac }
		'Unix' 	{ $NewEol = $EolLinux }
		'Linux' 	{ $NewEol = $EolLinux }
	}

	# Wenn kein Encoding definiert ist, lesen wir es aus
	If ($Encoding -eq $null) {
		$Encoding = Get-File-Encoding -LiteralPath $Filename
	}
	$Content = Get-Content-Fast -LiteralPath $Filename -Encoding $Encoding

	# Standardisieren der EOL
	# -> Win und Mac zu Linux
	$Content = $Content -replace "`r`n|`r", $EolLinux

	If ($NewEol -ne $EolLinux) {
		$Content = $Content -replace $EolLinux, $NewEol
	}

	Write-Content-Fast -LiteralPath $Filename -Content $Content -Encoding $Encoding
}



# Lädt die Files in $Filenames herunter
# Wenn die lokalen Files bereits aktuell sind, wird nichts heruntergeladen
# -Force	Lädt bereits aktuelle Files trotzde, herunter
# -Verbose	Infos werden angezeigt
# Return
#	$False	Keine Files aktualisiert
#	$True		Files aktualisiert
Function Download-LibGist() {
	[CmdletBinding()]
	Param(
		# Wenn Verfügbar, dann können vorhandene GistInfos bereits mitgegeben werden
		[Parameter(Mandatory)]
		[PSCustomObject[]]$GistInfo,
		[String[]]$FileNames,
		[String]$ZielDir,
		[Switch]$Force
	)

	$ResFilesUpdated = $False

	If ($null -eq $GistInfo) {
		Write-Verbose "Gists Files Download: Keine Files gefunden" -ForegroundColor Red
	} Else {
		Write-Verbose "Gists Files Download:" -ForegroundColor Yellow
		ForEach($GistItem in $GistInfo) {
			Write-Verbose "`n Gist Description: '$($GistItem.Description)'" -ForegroundColor Cyan
			ForEach($File in $GistItem.Files) {
				Write-Verbose "  $($File.Filename)" -ForegroundColor Magenta -NoNewline
				# Wenn das Gist einen Pfad hat, nur den Dateinamen nützen
				If (Is-Path $File.Filename) {
					$GistFileName = [IO.Path]::GetFileName($File.Filename)
				} Else {
					$GistFileName = $File.Filename
				}
				$LocalFileName = Join-Path $ZielDir $GistFileName

				# Das File herunterladen?
				$DlFile = Get-Item -LiteralPath $LocalFileName -ErrorAction SilentlyContinue
				If ($Force -eq $True -or `
					$Null -eq $DlFile `
					-or $GistItem.UpdatedLocalTime -gt $DlFile.LastAccessTime) {

					# Download
					$ResFilesUpdated = $True
					$Null = Invoke-WebRequest -Uri $File.raw_url -OutFile $LocalFileName -Verbose:$False
					If ($Null -eq $DlFile) {
						Write-Verbose "   > Heruntergeladen" -ForegroundColor Red
					} Else {
						Write-Verbose "   > Aktualisiert" -ForegroundColor Red
					}

					# Das File von Linux auf Windows EOL konvertieren
					# Für PS1 nützen wir UTF8 BOM
					$Splat = @{}
					If ([IO.Path]::GetExtension($LocalFileName) -eq '.ps1') {
						$Splat += @{ Encoding = (New-Object System.Text.UTF8Encoding $True) }
					}
					Convert-File-EOL -Filename $LocalFileName -LineEnding Win @Splat

					# Das lokale Änderungsdatum der Datei dem Gist angleichen
					$DlFile = Get-Item -LiteralPath $LocalFileName
					$DlFile.LastAccessTime = $GistItem.UpdatedLocalTime
				} Else {
					Write-Verbose "   > Bereits aktuell" -ForegroundColor Green
				}
			}
		}
	}
	Return $ResFilesUpdated
}



### Prepare

If ([String]::IsNullOrEmpty($ZielDir)) {
	$ZielDir = $ScriptDir
}
$GistsFilesPattern = Join-Path $ZielDir $GistsFilesPattern


# Prüfen, ob Github erreichbar ist
If (!(Test-Host-Reachable ([URI]$GistsUrl_ListPublicGists))) {
	Write-Verbose "Host nicht erreichbar: $(([URI]$GistsUrl_ListPublicGists).Host)"
	If ($NoReturnInfo) { Return }
	If ($ReturnErrorCode) {
		Exit 3	# GitHub nicht erreichbar
	} Else {
		Return
	}
}



### Main

# Dbg
# $ListPublicGistsFiles = $true
# $DescriptionFilter = '*#Ex*'
# $DescriptionFilter = '*#DownloadLib*'
# $DescriptionFilter = '*#LibGist*'

# Dbg
# $GetFile_GistInfo = $true
# $FileName = 'File1.txt'


Switch ($PsCmdlet.ParameterSetName) {
	'List' {
		# Listet alle Public Gist Files
		$Res = Get-Gist-List -DescriptionFilter:$DescriptionFilter -Verbose:$False
		If ($ReturnErrorCode) {
			Write-Host ($Res | Out-String)
			Exit 0	# Alles OK
		} Else {
			Return $Res
		}
	}

	'GetFileData' {
		#
		$Res = Get-Gists-FileMetadata -DescriptionFilter $DescriptionFilter -FileNames $FileNames -Verbose:$Verbose
		If ($ReturnErrorCode) {
			Write-Host ($Res | Out-String)
			Exit 0	# Alles OK
		} Else {
			Return $Res
		}
	}

	'UpdateLocalFiles' {
		# Allenfalls die aktuelle GistListe abrufen
		If ($null -eq $GistInfo) {
			$GistInfo = Get-Gists-FileMetadata -DescriptionFilter $DescriptionFilter -FileNames $FileNames -Verbose:$Verbose
		}

		# Die Lokalen Filenamen holen
		$LocalGistLibFileNames = Get-ChildItem $GistsCfg.GistsFilesPattern | Select -ExpandProperty Name

		# Wir arbeiten per Default im ScriptDir
		$ZielDir = Calc-ZielDir -ZielDir $ZielDir -DefaultAbsoluteDir $ScriptDir
		$Res = Download-LibGist -GistInfo $GistInfo -FileNames $LocalGistLibFileNames -ZielDir $ZielDir -Force:$Force

		If ($NoReturnInfo) { Return }
		If ($ReturnErrorCode) {
			#	$True		Files aktualisiert
			#	$False	Keine Files aktualisiert
			If ($Res) {
				Exit 0	# Bereits aktuell
			} Else {
				Exit 1	# Files aktualisiert
			}
		} Else {
			Return $Res
		}
	}

	'DLFiles' {
		# Allenfalls die aktuelle GistListe abrufen
		If ($null -eq $GistInfo) {
			$GistInfo = Get-Gists-FileMetadata -DescriptionFilter $DescriptionFilter -FileNames $FileNames -Verbose:$Verbose
		}

		# Wir arbeiten per Default im Arbeitsverzeichnis
		$ZielDir = Calc-ZielDir -ZielDir $ZielDir -DefaultAbsoluteDir (Get-Location)
		$Res = Download-LibGist -GistInfo $GistInfo -FileNames $FileNames -ZielDir $ZielDir -Force:$Force

		If ($NoReturnInfo) { Return }
		If ($ReturnErrorCode) {
			#	$True		Files aktualisiert
			#	$False	Keine Files aktualisiert
			If ($Res) {
				Exit 0	# Bereits aktuell
			} Else {
				Exit 1	# Files aktualisiert
			}
		} Else {
			Return $Res
		}
	}

	Default {
		Write-Host "Aufruf von: LibGist-Get-Gist.ps1: Keine Funktion angegeben" -ForegroundColor Red
		Write-Host "Keine Funktion angegeben" -ForegroundColor Red
		If ($NoReturnInfo) { Return }
		If ($ReturnErrorCode) {
			Exit 100	# Allgemeiner Fehler
		}
	}
}

