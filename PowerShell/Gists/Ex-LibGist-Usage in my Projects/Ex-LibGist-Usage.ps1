﻿# Beispiel Script,
#   um LibGist Funktionen einzubinen
#
# 001, 220127, tom-agplv3@jig.ch
#

Param(
	[Switch]$LibGistForceUpdate
)


### Config
$LibGistVersion_Ex_LibGist_Usage_ps1 = '001'


## !! LibGist

# Scriptname für Use-Gists berechnen
$LibGistUseGists_ps1 = 'LibGist-Use-Gists.ps1'
$ScriptDir = [IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Path)
$LibGistUseGists_ps1 = Join-Path $ScriptDir $LibGistUseGists_ps1

# Use-Gists einbinden
. $LibGistUseGists_ps1

# Allenfalls alle lokalen LibGist Files aktualisieren
$ResUpdate = LibGist-Update-GistFiles -Now:$LibGistForceUpdate
If ($LibGistForceUpdate) {
	Break Script
}


# Wenn das Script selber aktualisiert wurde, es neu laden
If ($ResUpdate) { . $LibGistUseGists_ps1 }

# Alle lokalen LibGist Files als Funktionen einbinden, muss .sourced aufgerufen werden!
. LibGist-Load-Gists



## Prepare



## Main

# Testen einer LibGist Funktion
Log 0 "Ping 9.9.9.9"
Log 1 (Test-Connection-Fast 9.9.9.9 -TimeoutMs 500 -TestForSuccess -NoOfPings 3)
