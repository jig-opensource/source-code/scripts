@Echo off
Rem Startet das gleichnamige PowerShell Script
Rem 001, 220803, tom-agplv3@jig.ch
Rem 
Rem %AppData%\Microsoft\Windows\Start Menu\Programs\Startup
Rem 
SET ScriptDir=%~dp0
SET ScriptFilename=%~n0
SET StartDir=%~dp0Tools

SET ScriptFilename=LibGist-Get-Gist.ps1

Rem Echo %ScriptDir%
Rem Echo %StartDir%
Rem Echo %ScriptFilename%

Echo.
Echo Starte Download von 'Download-Tom-Chocolatey-Tools.ps1'

REM Script-*Start* im Subdir
REM Start /Wait /B PowerShell.exe -NoLogo -ExecutionPolicy ByPass -NoProfile -Command "& '%StartDir%\%ScriptFilename%' -Verbose -NoReturnInfo -DownloadFile -FileNames 'Download-Tom-Chocolatey-Tools.ps1'"

REM *Download* ins SubDir
Start /Wait /B PowerShell.exe -NoLogo -ExecutionPolicy ByPass -NoProfile -Command "& '%ScriptDir%\%ScriptFilename%' -Verbose -NoReturnInfo -DownloadFile -ZielDir '.\Tools' -FileNames 'Tom-Chocolatey-Tools.ps1'"

Echo.
