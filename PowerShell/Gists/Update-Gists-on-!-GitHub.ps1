﻿# Infos, um die gists zu aktualisieren
# 
# 
# 
# 001, 220127
# 



# Wichitg: ins Verzeichnis wechseln
# sonst überträgt gh gist create auch den Verzeichnisnamen ins gist
cd c:\Scripts\PowerShell\-Gist\


# die Files hochladen
Write-Host "`n`nVorsicht!, diese Befehle erzeugen neue Gists" -ForegroundColor Red
Write-Host "und ersetzen nicht die bestehenen!`n`n" -ForegroundColor Red

gh gist create --public 'LibGist-Get-Gist.ps1' -d 'LibGist: Get-Gist.ps1 #PowerShell #LibGist'
gh gist create --public 'LibGist-Use-Gists.ps1' -d 'LibGist: Use-Gists.ps1 #PowerShell #LibGist'
gh gist create --public 'LibGist-TomBasicTools.ps1' -d 'LibGist: TomsBasicTools.ps1 #PowerShell #LibGist'
gh gist create --public 'Ex-LibGist-Usage.ps1' -d 'LibGist: Ex-LibGist-Usage.ps1 #PowerShell #LibGist'

