﻿# Funktion für die Nutzung des GitLab REST API
#
#	Test
#		PS7
#		# -Func Script erzeugen
#		Convert-ScriptToFunction 'c:\GitWork\GitLab.com\jig-Opensource\Source-Code\Scripts\src\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1' -name GitLab-RestAPI | Out-File 'c:\GitWork\GitLab.com\jig-Opensource\Source-Code\Scripts\src\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data-Func.ps1'
#
#		PS5 / PS7
#		# -Func Script laden
#		. 'c:\GitWork\GitLab.com\jig-Opensource\Source-Code\Scripts\src\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data-Func.ps1'
#		# Funktion aufrufen
#		Gitlab-Restapi -DownloadFiles -GitLabRef 'jig-opensource/source-code/scripts' -Recursive -ZielDir '.\tomtom\220810 200052' -Verbose
#
#
#
#	Aus
#		Get-GitLab-RestAPI-Data.ps1
#	Die -Func Datei erzeugen
#		Get-GitLab-RestAPI-Data-Func.ps1
#
#	Aufruf
#		Convert-ScriptToFunction 'c:\GitWork\GitLab.com\jig-Opensource\Source-Code\Scripts\src\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1' -name GitLab-RestAPI | Out-File 'c:\GitWork\GitLab.com\jig-Opensource\Source-Code\Scripts\src\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data-Func.ps1'
#
#
#	Tools / Hilfsfunktionen
#		In einem Projekt eine Datei suchen
#			-FindProjectRepoFile
#				$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -FindProjectRepoFile -GitLabRef 'jig-opensource/source-code/scripts' -FileNameSrchPtrn '*.ps1'
#		In einem Projekt für Dateien das Modifikationsdatum suchen
#			-GetProjectRepoLastCommit
#				$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetProjectRepoLastCommit -GitLabRef 'jig-opensource/source-code/scripts' -FileNameSrchPtrn '*.ps1'
#		In einem Projekt für Dateien das Modifikationsdatum suchen
#			-DownloadFiles
#				$AnzUpdated = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -DownloadFiles -GitLabRef 'jig-opensource/source-code/scripts' -FileNameSrchPtrn '*.ps1'
#				$AnzUpdated = C:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -DownloadFiles -GitLabRef 'jig-opensource/source-code/scripts' -Recursive -FileNameSrchPtrn '*.ps1' -ZielDir '.\xxx' -Verbose
#
#
#	REST API Funktionen
#		Rückgabe:
#		$ResHttpStatusCode, $ResWebRequest, $ResJson = …
#
#		Suche nach den Projekten eines GitLab Users
#			-GetUserProjects
#				$H,$R,$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetUserProjects -SrchUserName schittli
#		Suche nach allen öffentlichen GitLab Gruppen
#			-GetGroups
#				$H,$R,$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetGroups -GroupSrchNameOrPath 'jig-opensource'
#		Suche nach GitLab Projekten
#			-GetProjects
#				$H,$R,$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetProjects -ProjectSrchName jig -MaxResults 10
#		Suche nach GitLab Usern
#			-GetUsers
#				$H,$R,$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetUsers -UserSrchName schittli
#		Suche nach Projekten einer Gruppe
#			-GetGroupProjects
#				$H,$R,$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetGroupProjects -GitLabRef 'jig-opensource/source-code'
#		Suche nach dem Dateibaum in einem Projekt
#			-GetProjectRepoTree
#				$H,$R,$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetProjectRepoTree -GitLabRef 'jig-opensource/source-code/scripts'
#		Suche nach den Commits in einem Projekt
#			-GetProjectRepoCommits
#				Alle Commits
#				$H,$R,$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetProjectRepoCommits -GitLabRef 'jig-opensource/source-code/scripts'
#				Commits für eine Datei
#				$H,$R,$J = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetProjectRepoCommits -GitLabRef 'jig-opensource/source-code/scripts' -ForExactFileOrPath 'Tom-Chocolatey-Tools.ps1'
#		Suche nach den Branches in einem Projekt-Repo
#			-GetProjectRepoBranches
#				$Branches = c:\Scripts\PowerShell\GitLab-RestAPI\Get-GitLab-RestAPI-Data.ps1 -GetProjectRepoBranches -GitLabRef 'jig-opensource/source-code/scripts'

#
# ToDo
# 	Suche
# 		https://docs.gitlab.com/ee/api/search.html#scope-projects
#		Braucht Authentisierung,
#			müsste aber vorbereitet sein
#
#

# 001
# 002
# 003
# 004
# 005

# Suppress PSScriptAnalyzer Warning
[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSReviewUnusedParameter','')]
[CmdletBinding()]
Param (
	# Suche nach den Projekten eines GitLab Users
	[Parameter(ParameterSetName = 'GetUserProjects')]
	[Switch]$GetUserProjects,
	[Parameter(ParameterSetName = 'GetUserProjects')]
	[String]$SrchUserName = 'schittli',

	# Suche nach allen öffentlichen GitLab Gruppen
	[Parameter(ParameterSetName = 'GetGroups')]
	[Switch]$GetGroups,
	[Parameter(ParameterSetName = 'GetGroups')]
	# 'jig' > 46 GitLab Gruppen
	[String]$GroupSrchNameOrPath = 'jig-opensource',

	# Suche nach GitLab Projekten
	[Parameter(ParameterSetName = 'GetProjects')]
	[Switch]$GetProjects,
	[Parameter(ParameterSetName = 'GetProjects')]
	# e.g. jig
	[String]$ProjectSrchName,

	# Suche nach GitLab Usern
	[Parameter(ParameterSetName = 'GetUsers')]
	[Switch]$GetUsers,
	[Parameter(ParameterSetName = 'GetUsers')]
	# e.g. schittli
	[String]$UserSrchName,

	# Suche nach Projekten einer Gruppe
	[Parameter(ParameterSetName = 'GetGroupProjects')]
	[Switch]$GetGroupProjects,

	[Parameter(ParameterSetName = 'FindProjectRepoFile', Mandatory)]
	[Parameter(ParameterSetName = 'IsUriGitLabGroup', Mandatory)]
	[Parameter(ParameterSetName = 'IsUriGitLabProject', Mandatory)]
	[Parameter(ParameterSetName = 'GetGroupProjects', Mandatory)]
	[Parameter(ParameterSetName = 'GetProjectRepoBranches', Mandatory)]
	[Parameter(ParameterSetName = 'GetProjectRepoTree', Mandatory)]
	[Parameter(ParameterSetName = 'GetProjectRepoCommits', Mandatory)]
	[Parameter(ParameterSetName = 'DownloadFiles', Mandatory)]
	[Parameter(ParameterSetName = 'GetProjectRepoLastCommit')]
	# Irgend eine GitLab-Referenz
	# - Eine Gruppe
	# - Ein Projekt
	# e.g.
	# 	jig-opensource/source-code
	# 	jig-opensource/source-code/scripts
	# Kann auch die ganze URI sein
	[String]$GitLabRef,

	# Suche nach dem Dateibaum in einem Projekt
	[Parameter(ParameterSetName = 'GetProjectRepoTree')]
	[Switch]$GetProjectRepoTree,

	[Parameter(ParameterSetName = 'GetProjectRepoTree')]
	[Parameter(ParameterSetName = 'FindProjectRepoFile')]
	[Parameter(ParameterSetName = 'DownloadFiles')]
	[Switch]$Recursive,

	# Suche nach den Commits in einem Projekt
	[Parameter(ParameterSetName = 'GetProjectRepoCommits')]
	[Switch]$GetProjectRepoCommits,

	[Parameter(ParameterSetName = 'GetProjectRepoCommits')]
	# e.g. '!H jig-Opensource - scripts · GitLab.url'
	[String]$ForExactFileOrPath,

	# Suche nach den Branches in einem Projekt
	[Parameter(ParameterSetName = 'GetProjectRepoBranches')]
	[Switch]$GetProjectRepoBranches,

	# Kann immer genützt werden
	[String]$AccessToken,


	# ••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
	# Tools / Hilfsfunktionen

	# Ist eine URI eine GitLab Gruppe?
	[Parameter(ParameterSetName = 'IsUriGitLabGroup')]
	[Switch]$IsUriGitLabGroup,

	# Ist eine URI eine GitLab Projekt?
	[Parameter(ParameterSetName = 'IsUriGitLabProject')]
	[Switch]$IsUriGitLabProject,

	# In einem Projekt eine Datei suchen
	[Parameter(ParameterSetName = 'FindProjectRepoFile')]
	[Switch]$FindProjectRepoFile,

	# In einem Projekt für eine Datei das Modifikationsdatum suchen
	[Parameter(ParameterSetName = 'GetProjectRepoLastCommit')]
	[Switch]$GetProjectRepoLastCommit,

	# Files herunteraden, wenn sie lokal fehlen oder veraltet sind
	[Parameter(ParameterSetName = 'DownloadFiles')]
	[Switch]$DownloadFiles,


	[Parameter(ParameterSetName = 'FindProjectRepoFile', Mandatory)]
	[Parameter(ParameterSetName = 'GetProjectRepoLastCommit', Mandatory)]
	[Parameter(ParameterSetName = 'DownloadFiles')]
	[String]$FileNameSrchPtrn,

	[Parameter(ParameterSetName = 'DownloadFiles', Mandatory)]
	[String]$ZielDir,

	[Switch]$Force
)



### Config


# !9 Vorsicht!,
# 		Besser Download als Zip nützen!
# 		Das PowerShell 5.x Problem mit Invoke-WebRequest und Sonderzeichen in UTF8 ist gelöst
# 		» ConvertFrom-Misinterpreted-Utf8
#		Aber die vielen einzelnen REST API Aufrufe sind langsam
$DownloadGitLabFilesModeAsZip = $True

# $ThisScriptName = $MyInvocation.MyCommand.Path
# $ScriptDir = [IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Path)
$MyWorkingDirectory = [System.IO.Directory]::GetCurrentDirectory()


Enum eSrchMode { PageBased; KeysetBased }



#Region Console Tools

# Überschreiben für Verbose, weil ich kein Prefix und Farben will
# overridden PSScriptAnalyzer (PSAvoidOverwritingBuiltInCmdlets
# Suppress PSScriptAnalyzer Warning
Function Write-Verbose() {
	# [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidOverwritingBuiltInCmdlets', '')]
	# [CmdletBinding()]
	# Param()
	If ($VerbosePreference) { Write-Host @Args }
}

#EndRegion Console Tools


Function IsNullOrEmpty([String]$Str) {
	[String]::IsNullOrWhiteSpace($Str)
}

Function Has-Value($Test) {
	-not [String]::IsNullOrWhiteSpace($Test)
}



#Region Verzeichnis Tools

# Liefert True, wenn $Path relativ ist
Function Is-Path-Relative($Path) {
	If ([String]::IsNullOrEmpty($Path)) { Return }

	If ($Path.StartsWith('\\')) { Return $False }
	If ($Path.StartsWith('.\')) { Return $True }
	If ($Path.Contains(':')) { Return $False }
	Return $true
}

Function Is-Path($Path) {
	If ([String]::IsNullOrEmpty($Path)) { Return }
	If ($Path.StartsWith('\\')) { Return $True }
	If ($Path.StartsWith('.\')) { Return $True }
	If ($Path.Contains(':')) { Return $True }
	Return $False
}


# berechnet das absolute Zielverzeichnis
# und stellt sicher, dass es existiert
Function Calc-ZielDir($ZielDir, $DefaultAbsoluteDir) {
	# Allenfalls das Zielverzeichnis berechnen
	If (Has-Value $ZielDir) {
		If (Is-Path-Relative $ZielDir) {
			$ZielDir = Join-Path $DefaultAbsoluteDir $ZielDir
		}
	} Else {
		# Das File ins Arbeitsverzeichnis laden
		$ZielDir = $DefaultAbsoluteDir
	}

	# Sicherstellen, dass das Verzeichnis existiert
	New-Item -Path $ZielDir -ItemType Directory -ErrorAction SilentlyContinue | Out-Null
	# Im ZielDir String: relative Pfadangaben auflösen
	$ZielDir = (Get-Item $ZielDir).FullName

	Write-Verbose "Arbeitsverzeichnis:" -ForegroundColor Yellow
	Write-Verbose "$($ZielDir)`n"
	Return $ZielDir
}

#EndRegion Verzeichnis Tools




#Region Class GitLabUri
## Class GitLabUri

# Generische Funktion für Join-Path und Join-URL
# !Q https://stackoverflow.com/a/9593652/4795779
# 220809
Function Join-Parts {
	[CmdletBinding()]
	Param (
		[String[]]$Parts,
		[String] $Seperator
	)

	## Separator erkennen
	# Default
	$Seperator = '\'
	ForEach ($Item in $Parts) {
		# Write-Host "Check: $Item"
		If ($Item.Contains('\')) { $Seperator = '\'; Break; }
		If ($Item.Contains('/')) { $Seperator = '/'; Break; }
	}

	# Regex, um mehrere aufeinanderfolgende Seperatoren durch einen zu ersetzen
	$Search = '(?<!:)' + [regex]::Escape($Seperator) + '+'
   Return ($Parts | ? { -Not [String]::IsNullOrWhiteSpace($_) } ) -Join $Seperator -Replace $Search, $Seperator
}


# Abstraktion für GitLab URI mit Hilfsfunktionen
Class GitLabUri {
	[String]$OriUri
	[Nullable[Bool]]$IsGitLabGroup = $Null
	[Nullable[Bool]]$IsGitLabProject = $Null
	[Bool]$IsGitLabUri = $False

	#          ┌─ Hostname ─┐     ┌ UriPart_UsrPrjGrpInfo ┐   ┌ UriPart_Repo ─────────────────────┐
	#          │            │     │                       │   ┌ RepoDomain ┐
	#          │            │     │                       │   │         ┌──┘
	#          │            │     │                       │   │         │ ┌──── RepoFilePath ─────┐
	#  https://www.gitlab.com:443/Schittli/GitxxxxxxLab-API/-/blob/master/Add-GitLabProjectStar.ps1
	#                             └─ User  └─Project        │ └─ Repository
	#                                                       └─ Delimiter

	[String]$UriPart_UsrPrjGrpInfo	# Mit führendem /
	[String]$UriPart_Repo				# Mit führendem /
	[String]$RepoDomain					# Mit führendem /
	[String]$RepoFilePath				# Mit führendem /

	[String]$Scheme
	[String]$Host
	[Int]$Port
	[String]$AbsolutePath				# Mit führendem /
	[String]$Query

	## Config
	[String]$GitLabUriSeparator = '/-/'

	# Dekodiert eine GitLab URI
	[Void] DecodeGitLabUri($Uri) {
		If ([String]::IsNullOrWhiteSpace($Uri)) { Return }
		$This.OriUri = $Uri

		# Annahmen / Anatomie
		#
		#          ┌─ Hostname ─┐    ┌─ UriPart_UsrPrjGrpInfo ┐  ┌ UriPart_Repo ─────────────────────┐
		#          │            │    │                        │  ┌ RepoDomain ┐
		#          │            │    │                        │  │           ┌┘
		#          │            │    │                        │  │           ┌──── RepoFilePath ─────┐
		#  https://www.gitlab.com:443/Schittli/GitxxxxxxLab-API/-/blob/master/Add-GitLabProjectStar.ps1
		#  │       │   │          │   └─ User  └─Project        │ └─ Repository
		#  │       │   └─ Domain  └─ Port                       └─ Delimiter
		#  │       └─ Subdomain
		#  └─ Protocol
		#
		#                    ┌─────── UriPart_UsrPrjGrpInfo ───┐
		# 	https://gitlab.com/jig-opensource/source-code/scripts
		#                     └─ Group       └─ Group    └─ Project
		#                                       (Subgroup)
		#
		#	https://gitlab.com/jig-opensource/medizin
		#                     └─ Group       └─ Project
		#
		#  https://gitlab.com/jig-opensource/medizin/-/tree/master/Vitamin-D3
		#  https://gitlab.com/jig-opensource/medizin/-/blob/master/README.md
		#                                            │ └─ Repository
		#                                            └─ Delimiter

		# $RgxUri = '((?<scheme>https?|ftp):\/)?\/?((?<username>.*?)(:(?<password>.*?)|)@)?(?<hostname>[^:\/\s]+)(?<port>:([^\/]*))?(?<path>(\/\w+)*\/)(?<filename>[-\w.]+[^#?\s]*)?(?<query>\?([^#]*))?(?<fragment>#(.*))?$'
		# 	$RgxUri = (@'
		# 	((?<scheme>https?|ftp):\/)?
		# 	\/?
		# 	((?<username>.*?)(:(?<password>.*?)|)@)?
		# 	(?<hostname>[^:\/\s]+)
		# 	(?<port>:([^\/]*))?
		# 	(?<path>(\/\w+)*\/)
		# 	(?<filename>[-\w.]+[^#?\s]*)?
		# 	(?<query>\?([^#]*))?
		# 	(?<fragment>#(.*))?$
		# '@ -split "`r`n|`r|`n" | % { $_.Trim() }) -join ''

		# http ergänzen
		If ($Uri.StartsWith('http') -eq $false) { $Uri = 'https://{0}' -f $Uri }
		# parsen
		[URI]$oUri = $Uri

		$This.Scheme			= $oUri.Scheme
		$This.Host           = $oUri.Host
		$This.Port           = $oUri.Port
		$This.AbsolutePath   = $oUri.AbsolutePath
		$This.Query          = $oUri.Query

		#          ┌─ Hostname ─┐    ┌─ UriPart_UsrPrjGrpInfo ┐  ┌ UriPart_Repo ──────────────────────┐
		#          │            │    │                        │  ┌ RepoDomain ┐
		#          │            │    │                        │  │           ┌┘
		#          │            │    │                        │  │           ┌───── RepoFilePath ─────┐
		#  https://www.gitlab.com:443/Schittli/GitxxxxxxLab-API/-/blob/master/Add-GitLabProjectStar.ps1
		#                             └─ User  └─Project        │ └─ Repository
		#                                                       └─ Delimiter

		$This.UriPart_UsrPrjGrpInfo, $This.UriPart_Repo = $oUri.AbsolutePath -split ($This.GitLabUriSeparator)
		If ($This.UriPart_Repo) {
			$This.UriPart_Repo = '/{0}' -f $This.UriPart_Repo
			$This.RepoDomain = ($This.UriPart_Repo -split ('/'))[0..2] -join '/'
			$This.RepoFilePath = '/{0}' -f $This.UriPart_Repo.SubString($This.RepoDomain.Length + 1)
		} Else {
			$This.RepoDomain = $null
			$This.RepoFilePath = $null
		}
	}


	# Prüft, ob die UriPart_UsrPrjGrpInfo Uri eine Gruppe oder ein Projekt referenziert
	# !9 Das Script ruft sich selber auf.
	#		Weil die GitLab REST API FFunktion diese Klasse braucht,
	#		deshalb muss die Klasse im Code vorher sein und deshalb kann die Klasse die Funktion nicht aufrufen
	[Void]CheckUriType() {
		$GetUriPart_UsrPrjGrpInfoFull = $This.GetUriPart_UsrPrjGrpInfoFull()
		$This.IsGitLabGroup = & $Script:ThisScriptName -IsUriGitLabGroup -GitLabRef $GetUriPart_UsrPrjGrpInfoFull
		$This.IsGitLabProject = & $Script:ThisScriptName -IsUriGitLabProject -GitLabRef $GetUriPart_UsrPrjGrpInfoFull
	}


	# Konstruktor
	GitLabUri([String]$Uri) {
		If ([String]::IsNullOrWhiteSpace($Uri)) { Return }

		# Sicherstellen, dass wir eine https://gitlab.com Uri haben
		# Ist die Uri von gitlab.com?
		If ( $uri.Contains('gitlab.com') ) {
			# Ja, gitlab.com, aber https fehlt
			If (-not($Uri.StartsWith('http'))) {
				$Uri = 'https://{0}' -f $Uri
			}
		} Else {
			# Die Domäne ist nicht gitlab.com
			# … aber http ist vorhanden
			If ($Uri.StartsWith('http')) {
				# welche Domäne haben wir??
				[Uri]$oUri = $Uri
				Write-Host "Die URI ist nicht von GitLab: $($oUri.Host)"
			} Else {
				# http und gitlab fehlen, also ergänzen
				$Uri = Join-Parts 'https://gitlab.com', $Uri
			}
		}

		$This.DecodeGitLabUri($Uri)
		$This.IsGitLabUri = $This.Host.EndsWith('gitlab.com')
	}


	# Codiert UriPart_UsrPrjGrpInfo
	# jig-opensource/Fsource-code
	# >> jig-opensource%2Fsource-code
	# !M https://docs.gitlab.com/ee/api/index.html#namespaced-path-encoding
	[String]EncodeStr($Str) {
		# [URI]::EscapeDataString()
		Return [System.Net.WebUtility]::UrlEncode( $Str )
	}

	# Liefert GetUriPart_UsrPrjGrpInfo codiert,
	# um es im REST API als Parameter zu nützen
	#
	# jig-opensource%2Fsource-code%2Fchocolatey
	[String]GetUriPart_UsrPrjGrpInfoEncoded() {
		Return $This.EncodeStr( $This.UriPart_UsrPrjGrpInfo.Trim('/').Trim('\'))
	}

	# Liefert die ganze URI für UriPart_UsrPrjGrpInfo
	#
	# https://gitlab.com:443/jig-opensource/source-code/chocolatey
	[String]GetUriPart_UsrPrjGrpInfoFull() {
		# Zusammenstellen: https://host:port
		$ThisUri = ('{0}://{1}{2}' -f $This.Scheme, $This.Host, `
			$(If ($This.Port -ne 0) { ":$($This.Port)" }Else { '' } ))

		Return Join-Parts $ThisUri, $This.UriPart_UsrPrjGrpInfo
	}

	# https://gitlab.com:443/jig-opensource/source-code/chocolatey/-/
	[String]GetUri() {
		Return Join-Parts ($This.GetUriPart_UsrPrjGrpInfoFull()), $This.GitLabUriSeparator, $This.RepoDomain, $This.RepoFilePath
	}

	# [String]ToString() { return "$($This.Value): $($This.Text)" }
}

#EndRegion Class GitLabUri



#Region Json Merge Tools
## Region Json Merge Tools
## Merge mit Hilfe der Textverarbeitung

# True, wenn $JsonText ein Array darstellt
Function Is-JsonArray($JsonText) {
	Return $JsonText.TrimStart().StartsWith('[')
}

# Liefert True, wenn JsonText ein leeres Element ist, z.B. [] oder {}
Function Is-JsonData-Empty($JsonText) {
	# Alle white spaces entfernen
	Return (($JsonText -Replace "\s+", '').Length -eq 2)
}

# Ergänzt ein JsonArray um ein Element
Function Add-JsonDataToJsonArray() {
	[CmdletBinding()]
	Param (
		[String]$JsonTextArr,
		[String]$JsonTextItem,
		[Switch]$InsertBefore,
		[Switch]$AddToEnd
	)

	If ($InsertBefore -eq $False -and $AddToEnd -eq $False) {
		Write-Error 'Add-JsonDataToJsonArray(): Zwinged: entweder -InsertBefore oder -AddToEnd'
	}

	If ($InsertBefore) {
		# Beim Array erstes [ entfernen
		$TextArr_Ende = $JsonTextArr.TrimStart().TrimStart('[').TrimStart()
		# Merge
		Return ('[{0},{1}' -f $JsonTextItem, $TextArr_Ende)
	}
	If ($AddToEnd) {
		# Beim Array letztes ] entfernen
		$TextArr_Start = $JsonTextArr.TrimEnd().TrimEnd(']').TrimEnd()
		# Merge
		Return ('{0},{1}]' -f $TextArr_Start, $JsonTextItem)
	}
}

# Merge zweier Json Objekte,
# Es können zwei Json Arrays oder auch Json Elemente sein
Function Merge-JsonArray($JsonTextArr1, $JsonTextArr2) {

	# Ein leeres JSON Element hat nur zwei Chars, e.g. [] oder {}
	$Item1IsEmptyJsonItem = Is-JsonData-Empty $JsonTextArr1
	$Item2IsEmptyJsonItem = Is-JsonData-Empty $JsonTextArr2

	$Item1IsArr = Is-JsonArray $JsonTextArr1
	$Item2IsArr = Is-JsonArray $JsonTextArr2

	# Leere Element bearbeiten
	If ($Item1IsEmptyJsonItem -and $Item2IsEmptyJsonItem) {
		# beides sind leere JSON-Elemente - ein leeres JSON Array zurückgeben
		Return '[]'
	} ElseIf ($Item1IsEmptyJsonItem) {
		Return $JsonTextArr2
	} ElseIf ($Item2IsEmptyJsonItem) {
		Return $JsonTextArr1
	}

	Switch ('{0}-{1}' -f $Item1IsArr, $Item2IsArr) {
		'False-False' {
			# Keines ist ein Array
			Return ('[{0},{1}]' -f $JsonTextArr1, $JsonTextArr2)
		}

		'True-False' {
			# Erstes ist ein Array
			Return Add-JsonDataToJsonArray -JsonTextArr $JsonTextArr1 -JsonTextItem $JsonTextArr2 -AddToEnd
		}

		'False-True' {
			# Letztes ist ein Array
			Return Add-JsonDataToJsonArray -JsonTextArr $JsonTextArr2 -JsonTextItem $JsonTextArr1 -InsertBefore
		}

		'True-True' {
			# Beim Array 1 letztes ] entfernen
			$TextArr1_Start = $JsonTextArr1.TrimEnd().TrimEnd(']').TrimEnd()

			# Beim Array 2 erstes [ entfernen
			$TextArr2_End = $JsonTextArr2.TrimStart().TrimStart('[')

			# Merge
			Return ('{0},{1}' -f $TextArr1_Start, $TextArr2_End)
		}
	}
}

#EndRegion Json Merge Tools



# Das GitLab Keyset Based Paging
# hat im zurückgegebenen Link in Wahrheit vier mögliche Links
Enum eKeysetBasedPagingLinkType { Prev; Next; First; Last }

# Decodiert beim GitLab Keyset Based Paging den Link-Text
Function Decode-GitLab-KeysetBasedPaging-Link() {
	# Das GitLab KeysetBased Paging liefert leider in Header.Link
	# unstrukturierte Informationen zur Navigation
	#
	# E.g.
	# 	<https://gitlab.com/api/v4/groups/jig-opensource%2Fsource-code/projects?id=jig-opensource%2Fsource-code&include_ancestor_groups=false&include_subgroups=false&order_by=id&owned=false&page=2&pagination=keyset&per_page=1&simple=false&sort=asc&starred=false&username=&with_custom_attributes=false&with_issues_enabled=false&with_merge_requests_enabled=false&with_security_reports=false&with_shared=true>; rel="next", <https://gitlab.com/api/v4/groups/jig-opensource%2Fsource-code/projects?id=jig-opensource%2Fsource-code&include_ancestor_groups=false&include_subgroups=false&order_by=id&owned=false&page=1&pagination=keyset&per_page=1&simple=false&sort=asc&starred=false&username=&with_custom_attributes=false&with_issues_enabled=false&with_merge_requests_enabled=false&with_security_reports=false&with_shared=true>; rel="first", <https://gitlab.com/api/v4/groups/jig-opensource%2Fsource-code/projects?id=jig-opensource%2Fsource-code&include_ancestor_groups=false&include_subgroups=false&order_by=id&owned=false&page=4&pagination=keyset&per_page=1&simple=false&sort=asc&starred=false&username=&with_custom_attributes=false&with_issues_enabled=false&with_merge_requests_enabled=false&with_security_reports=false&with_shared=true>; rel="last"
	#
	# In einem String, gekürzt:
	#		<https://gitlab.com/api/v4/groups/jig-opensource%2Fsource-code/projects?id=jig-opensource%2Fsource-code&incl…>; rel="next",
	#		<https://gitlab.com/api/v4/groups/jig-opensource%2Fsource-code/projects?id=jig-opensource%2Fsource-code&incl…>; rel="first",
	#		<https://gitlab.com/api/v4/groups/jig-opensource%2Fsource-code/projects?id=jig-opensource%2Fsource-code&incl…>; rel="last"
	#
	[CmdletBinding()]
	Param (
		[String]$GitLabKeysetBasedPagingLinkText
	)

	# Regex, um den $GitLabKeysetBasedPagingLinkText zu parsen
	$Rgx = @'
(?<Item>
<(?<Link>.*?)>
;\s*rel=(?<Rel>[^,]+)
)
'@

	# $RgxTextZwischenKlammern = '<(?<Link>.*?)>'
	$Res = @()
	$Res = @{}
	$AllMatches = [Regex]::Matches($GitLabKeysetBasedPagingLinkText, $Rgx, [Text.RegularExpressions.RegexOptions]::IgnorePatternWhitespace)
	If ($AllMatches.Count -gt 0) {
		ForEach ($Match In $AllMatches) {
			$RelStr = $Match.Groups['Rel'].Value.Trim('"')
			Try {
				$LinkType = [eKeysetBasedPagingLinkType]$RelStr
			} Catch {
				Write-Host "Kann $($RelStr) nicht in [eKeysetBasedPagingLinkType] konvertieren!"
			}
			$Res.Add($LinkType, $Match.Groups['Link'].Value)
		}
	}
	Return $Res
}


# Eigene Klasse um ein [Net.HttpStatusCode] abzubilden
# Kann mit Konsturiert und einem besseren Text erzeugt werden
Class JigHttpStatusCode {
	[Int]$Value
	[String]$Text
	[String]$BetterText = '' # Ori

	JigHttpStatusCode([Int]$Value, [String]$Text) {
		$This.Value = $Value; $This.Text = $Text
	}

	JigHttpStatusCode([Net.HttpStatusCode]$HttpStatusCode) {
		$This.Value = [Int]$HttpStatusCode; $This.Text = $HttpStatusCode
	}

	# Konstruktor: Der Text aus HttpStatusCode wird übernommen
	# ausser, wenn BetterText definiert ist
	JigHttpStatusCode([Net.HttpStatusCode]$HttpStatusCode, [String]$BetterText) {
		If ([String]::IsNullOrWhiteSpace($BetterText)) {
			$This.Value = [Int]$HttpStatusCode
			$This.Text = $HttpStatusCode
		} Else {
			$This.Value = [Int]$HttpStatusCode
			$This.BetterText = $BetterText
			# Führende Zahl: entfernen
			$This.Text = $BetterText -replace '\d{1,3}:*\s*', ''
		}
	}

	[String]ToString() { return "$($This.Value): $($This.Text)" }
}



# Ruft das GitLab REST API auf
# Res:
# 	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI …
Function Call-GitLab-RestAPI() {
	[CmdletBinding()]
	Param (
		[Hashtable]$Body,
		[Hashtable]$Header,
		[Hashtable]$WebRequestSplat,

		[eSrchMode]$SrchMode,
		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults
	)

	## Config
	# 20 = Default, max 100
	$GitlabRestApiMaxSrchResults = 100

	## Prepare
	If ($MaxResults -eq $null) { $MaxResults = [Int]::MaxValue }
	$Body.per_page	= [Math]::Min($GitlabRestApiMaxSrchResults, $MaxResults)
	If ($Body.per_page -lt 5) {
		Write-Host "MaxResults=5, weil MaxResults<5 den Fehler erzeugt: 500 Internal Server Error" -ForegroundColor Cyan
		$Body.per_page = 5
	}


	# Debug
	# $Body.per_page = 1

	Switch ($SrchMode) {
		# https://docs.gitlab.com/ee/api/index.html#pagination
		##
		## V1	Offset-based pagination
		# https://docs.gitlab.com/ee/api/index.html#offset-based-pagination
		([eSrchMode]::PageBased) {
			$ResJson = $Null; $Ende = $False
			# Write-Verbose 'Lade Resultatseite 1'
			Do {
				Try {
					# Geht, liefert aber keinen Response Header,
					# ohne den das Paging nicht klappt
					# Invoke-RestMethod @Params
					$ResLastNetHttpStatusCode = [JigHttpStatusCode]::New([Net.HttpStatusCode]::OK)

					# !9 Vorsicht!,
					# 		Besser Download als Zip nützen!
					# 		Das PowerShell 5.x Problem mit Invoke-WebRequest und Sonderzeichen in UTF8 ist gelöst
					# 		» ConvertFrom-Misinterpreted-Utf8
					#		Aber die vielen einzelnen REST API Aufrufe sind langsam
					$ResWebRequest = Invoke-WebRequest @WebRequestSplat -Body $Body -Verbose:$False

					If ($Null -eq $ResJson) {
						$ResJson = $ResWebRequest.Content
					} Else {
						$ResJson = Merge-JsonArray $ResJson $ResWebRequest.Content
					}

					# Zählen
					$AnzRecords = If ($ResJson) { ($ResJson | ConvertFrom-Json).Count } Else { 0 }
					# Haben wir die Anz. Records erreicht?
					If ($AnzRecords -ge $MaxResults) {
						$Ende = $True
					} Else {
						If ([String]::IsNullOrEmpty($ResWebRequest.Headers.'X-Next-Page')) {
							$Ende = $True
						} Else {
							Write-Verbose "Rufe Seite $($ResWebRequest.Headers.'X-Next-Page' | select -First 1)"
							$Body.Page = ($ResWebRequest.Headers.'X-Next-Page' | select -First 1)
						}
					}
				} Catch {
					If ($_.Exception.Response) {
						$ThisHttpStatusCode = $_.Exception.Response.StatusCode
						$ErrorDetailsMsg = $_.ErrorDetails.Message | ConvertFrom-Json | select -ExpandProperty Message
						$ResLastNetHttpStatusCode = [JigHttpStatusCode]::New($ThisHttpStatusCode, $ErrorDetailsMsg)
						# Write-Host "`n$($ResLastNetHttpStatusCode.ToString())`n" -ForegroundColor Magenta
						$Ende = $True
					}
					Else {
						$Ende = $True
						$MessageId = ('{0:x}' -f $_.Exception.HResult).Trim([char]0)
						$ErrorMessage = ($_.Exception.Message).Trim([char]0) # The network path was not found.
						Throw
					}
				}
			} While (!$Ende)

			SetDisplaySet -Obj $ResLastNetHttpStatusCode -DisplaySetName 'Tom.HttpStatusCode'  `
								-DefaultProperties @('Value', 'Text')

			Return @($ResLastNetHttpStatusCode, $ResWebRequest, $(If ($ResJson) { $ResJson | ConvertFrom-Json } Else { $Null } ))

		}

		## V2 Keyset-based pagination
		## https://docs.gitlab.com/ee/api/index.html#keyset-based-pagination
		##
		## !9 Keyset-pagination allows for more efficient retrieval of pages and
		## 	- in contrast to offset-based pagination -
		##		runtime is independent of the size of the collection.
		##
		## !9
		## 	Keyset-based pagination is supported only
		##		for selected resources and ordering options:
		##
		##			Projects	:	order_by=id only
		##			Groups	:	order_by=name, sort=asc only

		([eSrchMode]::KeysetBased) {
			$ResJson = $Null; $Ende = $False
			# Write-Verbose 'Lade Resultatseite 1'
			Do {

				Try {
					# Geht, liefert aber keinen Response Header,
					# ohne den das Paging nicht klappt
					# Invoke-RestMethod @Params
					$ResLastNetHttpStatusCode = [JigHttpStatusCode]::New([Net.HttpStatusCode]::OK)

					# !9 Vorsicht!,
					# 		Besser Download als Zip nützen!
					# 		Das PowerShell 5.x Problem mit Invoke-WebRequest und Sonderzeichen in UTF8 ist gelöst
					# 		» ConvertFrom-Misinterpreted-Utf8
					#		Aber die vielen einzelnen REST API Aufrufe sind langsam
					$ResWebRequest = Invoke-WebRequest @WebRequestSplat -Body $Body -Verbose:$False -EA Stop

					If ($Null -eq $ResJson) {
						$ResJson = $ResWebRequest.Content
					} Else {
						$ResJson = Merge-JsonArray $ResJson $ResWebRequest.Content
					}

					# Zählen
					$AnzRecords = If ($ResJson) { ($ResJson | ConvertFrom-Json).Count } Else { 0 }
					# Haben wir die Anz. Records erreicht?
					If ($AnzRecords -ge $MaxResults) {
						$Ende = $True
					}
					Else {
						If ([String]::IsNullOrEmpty($ResWebRequest.Headers.Link)) {
							$Ende = $True
						} Else {
							# Write-Verbose "Rufe Seite $($ResWebRequest.Headers.'X-Next-Page' | select -First 1)"
							# $Body.Page = ($ResWebRequest.Headers.Link | select -First 1)
							$LinkData = ($ResWebRequest.Headers.Link | select -First 1)

							# Parsen
							$oLinks = Decode-GitLab-KeysetBasedPaging-Link $LinkData
							# Haben wir einen next Link?
							If ($oLinks.Contains([eKeysetBasedPagingLinkType]::Next)) {
								$NextLink = $oLinks[([eKeysetBasedPagingLinkType]::Next)]
								If (-Not([String]::IsNullOrWhiteSpace($NextLink))) {
									$Header.Uri = $NextLink
								} Else {
									Write-Error "Konnte Link nicht parsen:`n$($ResWebRequest.Headers.Link)"
								}
							} Else {
								$Ende = $True
							}
						}
					}

				} Catch {
					If ($_.Exception.Response) {
						$ThisHttpStatusCode = $_.Exception.Response.StatusCode
						$ErrorDetailsMsg = $_.ErrorDetails.Message | ConvertFrom-Json | select -ExpandProperty Message
						$ResLastNetHttpStatusCode = [JigHttpStatusCode]::New($ThisHttpStatusCode, $ErrorDetailsMsg)
						# Write-Host "`n$($ResLastNetHttpStatusCode.ToString())`n" -ForegroundColor Magenta
						$Ende = $True
					} Else {
						$Ende = $True
						$MessageId = ('{0:x}' -f $_.Exception.HResult).Trim([char]0)
						$ErrorMessage = ($_.Exception.Message).Trim([char]0) # The network path was not found.
						Throw
					}
				}

			} While (!$Ende)

			SetDisplaySet -Obj $ResLastNetHttpStatusCode -DisplaySetName 'Tom.HttpStatusCode'  `
				-DefaultProperties @('Value', 'Text')

			Return @($ResLastNetHttpStatusCode, $ResWebRequest, $(If ($ResJson) { $ResJson | ConvertFrom-Json } Else { $Null } ))
		}
	}
}


#Region Fix-Utf8-Chars
## Region Fix-Utf8-Chars

# Invoke-Request hat in PowerShell 5 Fehler beim Decodieren von manchen Zeichen / Chars
# Diese Funktion korrigiert diese UTF8 Fehler
# !Q https://stackoverflow.com/a/47961370/4795779
Function ConvertFrom-Misinterpreted-Utf8([string] $String) {
	[System.Text.Encoding]::UTF8.GetString(
		[System.Text.Encoding]::GetEncoding(28591).GetBytes($String)
	)
}

#Endregion Fix-Utf8-Chars


#Region GitLab REST API Calls

# GetUserProjects
# 	Suche nach den Projekten eines GitLab Users
#	Returns: $ResHttpStatusCode, $ResWebRequest, $ResJson =
Function Call-GitLab-RestApi-GetUserProjects() {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('JigHttpStatusCode', 'HtmlWebResponseObject', 'System.Object[]')]
	[CmdletBinding()]
	Param (
		# Func: GetUserProjects
		#  'schittli'
		[String]$SrchUserName,

		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults,

		# Kann immer genützt werden
		[String]$AccessToken
	)

	$Header = @{}
	# https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
	If (-Not([String]::IsNullOrWhiteSpace($AccessToken))) {
		$Header.Add('PRIVATE-TOKEN', $AccessToken)
	}

	$WebRequestSplat = @{
		Method      = 'Get'
		ContentType = 'application/json; charset=utf-8'
	}

	# !M https://docs.gitlab.com/ee/api/users.html
	# !M https://docs.gitlab.com/ee/api/projects.html#list-user-projects
	# https://gitlab.com/api/v4/users/schittli/projects
	$SrchMode = [eSrchMode]::PageBased

	$WebRequestSplat += @{
		Uri = "https://gitlab.com/api/v4/users/$($SrchUserName)/projects"
	}

	Switch ($SrchMode) {
		([eSrchMode]::PageBased) {
			$Body = @{
				page = 1
			}
		}
		([eSrchMode]::KeysetBased) {
			$Body = @{
				# Für Keyset-based pagination zwingend
				pagination	= 'keyset'
				# !9 Sortierung wird *nicht* für jeden GitLab Datentyp unterstützt!
				# order_by   = 'id'
				# sort       = 'asc' # asc or desc
			}
		}
	}

	# REST API call
	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI -Body $Body -Header $Header `
																		-WebRequestSplat $WebRequestSplat `
																		-SrchMode $SrchMode -MaxResults $MaxResults
	# GitHub Restultat-Typ ergänzen
	$ResHttpStatusCode, $ResWebRequest, $ResJson | ? { $_ -ne $Null } | % { $_ | Add-Member -MemberType NoteProperty -Name 'GitLabObjType' -Value 'Project' }
	Return @($ResHttpStatusCode, $ResWebRequest, $ResJson)
}


# GetGroups
# 	Suche nach allen öffentlichen GitLab Gruppen
#	Returns: $ResHttpStatusCode, $ResWebRequest, $ResJson =
Function Call-GitLab-RestApi-GetGroups() {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('JigHttpStatusCode', 'HtmlWebResponseObject', 'System.Object[]')]
	[CmdletBinding()]
	Param (
		# Func: GetGroups
		# 'jig' > 46 GitLab Gruppen
		# 'jig-opensource'
		[String]$GroupSrchNameOrPath,

		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults,

		# Kann immer genützt werden
		[String]$AccessToken
	)

	$Header = @{}
	# https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
	If (-Not([String]::IsNullOrWhiteSpace($AccessToken))) {
		$Header.Add('PRIVATE-TOKEN', $AccessToken)
	}

	$WebRequestSplat = @{
		Method      = 'Get'
		ContentType = 'application/json; charset=utf-8'
	}

	# !M https://docs.gitlab.com/ee/api/users.html
	# !M https://docs.gitlab.com/ee/api/projects.html#list-user-projects
	# https://gitlab.com/api/v4/users/schittli/projects

	$SrchMode = [eSrchMode]::PageBased
	$WebRequestSplat += @{
		Uri	= 'https://gitlab.com/api/v4/groups'
	}
	Switch ($SrchMode) {
		([eSrchMode]::PageBased) {
			$Body = @{
				page   = 1
				search = $GroupSrchNameOrPath
			}
		}
		([eSrchMode]::KeysetBased) {
			$Body = @{
				## Für Keyset-based pagination zwingend
				pagination	= 'keyset'
				## !9 Sortierung wird *nicht* für jeden GitLab Datentyp unterstützt!
				# order_by   = 'id'
				# sort			= 'asc' # asc or desc

				## Für die Funktion
				search     = $GroupSrchNameOrPath
			}
		}
	}

	# REST API call
	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI -Body $Body -Header $Header `
																		-WebRequestSplat $WebRequestSplat -SrchMode $SrchMode `
																		-MaxResults $MaxResults
	# GitHub Restultat-Typ ergänzen
	$ResHttpStatusCode, $ResWebRequest, $ResJson | ? { $_ -ne $Null } | % { $_ | Add-Member -MemberType NoteProperty -Name 'GitLabObjType' -Value 'Group' }
	Return @($ResHttpStatusCode, $ResWebRequest, $ResJson)

}


# GetProjects
# 	Suche nach GitLab Projekten
# 	Returns: $ResHttpStatusCode, $ResWebRequest, $ResJson =
Function Call-GitLab-RestApi-GetProjects() {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('JigHttpStatusCode', 'HtmlWebResponseObject', 'System.Object[]')]
	[CmdletBinding()]
	Param (
		# Func: GetProjects
		# e.g. jig
		[String]$ProjectSrchName,

		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults,

		# Kann immer genützt werden
		[String]$AccessToken
	)

	$Header = @{}
	# https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
	If (-Not([String]::IsNullOrWhiteSpace($AccessToken))) {
		$Header.Add('PRIVATE-TOKEN', $AccessToken)
	}

	$WebRequestSplat = @{
		Method      = 'Get'
		ContentType = 'application/json; charset=utf-8'
	}

	# !M https://docs.gitlab.com/ee/api/projects.html#search-for-projects-by-name
	# https://gitlab.com/api/v4/projects
	# https://gitlab.com/api/v4/projects?search=jig

	## !9^9 Vorsicht, GitLab REST API Bug!:
	# 	https://gitlab.com/gitlab-org/gitlab/-/issues/370068
	# 	Die Suche liefert 500 Internal Server Error
	#	wenn gleichzeitig der Parameter per_page=2 angegeben wird!
	#  per_page=20 und =100 funktioniert.

	$SrchMode = [eSrchMode]::PageBased
	$WebRequestSplat += @{
		Uri	= 'https://gitlab.com/api/v4/projects'
	}
	Switch ($SrchMode) {
				([eSrchMode]::PageBased) {
			$Body = @{
				page   = 1
				search = $ProjectSrchName
			}
		}
				([eSrchMode]::KeysetBased) {
			$Body = @{
				## Für Keyset-based pagination zwingend
				pagination	= 'keyset'
				## !9 Sortierung wird *nicht* für jeden GitLab Datentyp unterstützt!
				# order_by		= 'id'
				# sort			= 'asc' # asc or desc

				## Für die Funktion
				search     = $ProjectSrchName
			}
		}
	}

	# REST API call
	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI -Body $Body -Header $Header `
																		-WebRequestSplat $WebRequestSplat -SrchMode $SrchMode `
																		-MaxResults $MaxResults
	# GitHub Restultat-Typ ergänzen
	$ResHttpStatusCode, $ResWebRequest, $ResJson | ? { $_ -ne $Null } | % { $_ | Add-Member -MemberType NoteProperty -Name 'GitLabObjType' -Value 'Project' }
	Return @($ResHttpStatusCode, $ResWebRequest, $ResJson)
}



# GetUsers
# 	Suche nach GitLab Usern
# 	Returns: $ResHttpStatusCode, $ResWebRequest, $ResJson =
Function Call-GitLab-RestApi-GetUsers() {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('JigHttpStatusCode', 'HtmlWebResponseObject', 'System.Object[]')]
	[CmdletBinding()]
	Param (
		# Func: GetUsers
		# e.g. schittli
		[String]$UserSrchName,

		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults,

		# Kann immer genützt werden
		[String]$AccessToken
	)

	$Header = @{}
	# https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
	If (-Not([String]::IsNullOrWhiteSpace($AccessToken))) {
		$Header.Add('PRIVATE-TOKEN', $AccessToken)
	}

	$WebRequestSplat = @{
		Method      = 'Get'
		ContentType = 'application/json; charset=utf-8'
	}

	# !M https://docs.gitlab.com/ee/api/users.html
	# https://gitlab.com/api/v4/users
	# https://gitlab.com/api/v4/users?username=schittli

	$SrchMode = [eSrchMode]::PageBased
	$WebRequestSplat += @{
		Uri	= 'https://gitlab.com/api/v4/users'
	}
	Switch ($SrchMode) {
				([eSrchMode]::PageBased) {
			$Body = @{
				page     = 1
				username = $UserSrchName
			}
		}
				([eSrchMode]::KeysetBased) {
			$Body = @{
				## Für Keyset-based pagination zwingend
				pagination	= 'keyset'
				## !9 Sortierung wird *nicht* für jeden GitLab Datentyp unterstützt!
				# order_by   = 'id'
				# sort			= 'asc' # asc or desc

				## Für die Funktion
				username   = $UserSrchName
			}
		}
	}

	# REST API call
	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI -Body $Body -Header $Header `
																			-WebRequestSplat $WebRequestSplat -SrchMode $SrchMode `
																			-MaxResults $MaxResults
	# GitHub Restultat-Typ ergänzen
	$ResHttpStatusCode, $ResWebRequest, $ResJson | ? { $_ -ne $Null } | % { $_ | Add-Member -MemberType NoteProperty -Name 'GitLabObjType' -Value 'User' }
	Return @($ResHttpStatusCode, $ResWebRequest, $ResJson)
}



# GetGroupProjects
# 	Suche nach Projekten einer Gruppe
# 	Returns: $ResHttpStatusCode, $ResWebRequest, $ResJson =
Function Call-GitLab-RestApi-GetGroupProjects() {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('JigHttpStatusCode', 'HtmlWebResponseObject', 'System.Object[]')]
	[CmdletBinding()]
	Param (
		# Func: GetGroupProjects
		# e.g.
		# 	jig-opensource/source-code
		# 	jig-opensource/source-code/scripts
		[GitLabUri]$oGitLabRef,

		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults,

		# Kann immer genützt werden
		[String]$AccessToken
	)

	$Header = @{}
	# https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
	If (-Not([String]::IsNullOrWhiteSpace($AccessToken))) {
		$Header.Add('PRIVATE-TOKEN', $AccessToken)
	}

	$WebRequestSplat = @{
		Method      = 'Get'
		ContentType = 'application/json; charset=utf-8'
	}

	# !M https://docs.gitlab.com/ee/api/groups.html

	# https://gitlab.com/jig-opensource/source-code
	# https://gitlab.com/jig-opensource
	# jig-opensource/source-code
	# jig-opensource%2Fsource-code
	# https://gitlab.com/api/v4/groups/jig-opensource%2Fsource-code/projects

	$SrchMode = [eSrchMode]::KeysetBased

	$WebRequestSplat += @{
		Uri	= "https://gitlab.com/api/v4/groups/$($oGitLabRef.GetUriPart_UsrPrjGrpInfoEncoded())/projects"
	}
	Switch ($SrchMode) {
				([eSrchMode]::PageBased) {
			$Body = @{
				page = 1
			}
		}
				([eSrchMode]::KeysetBased) {
			$Body = @{
				## Für Keyset-based pagination zwingend
				pagination	= 'keyset'
				## !9 Sortierung wird *nicht* für jeden GitLab Datentyp unterstützt!
				# order_by   = 'id'
				# sort			= 'asc' # asc or desc

				## Für die Funktion
			}
		}
	}

	# REST API call
	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI -Body $Body -Header $Header `
																		-WebRequestSplat $WebRequestSplat -SrchMode $SrchMode `
																		-MaxResults $MaxResults

	# GitHub Restultat-Typ ergänzen
	$ResHttpStatusCode, $ResWebRequest, $ResJson | ? { $_ -ne $Null } | % { $_ | Add-Member -MemberType NoteProperty -Name 'GitLabObjType' -Value 'Project' }
	Return @($ResHttpStatusCode, $ResWebRequest, $ResJson)
}



# GetProjectRepoTree
# 	Suche nach dem Dateibaum in einem Projekt
# 	Returns: $ResHttpStatusCode, $ResWebRequest, $ResJson =
Function Call-GitLab-RestApi-GetProjectRepoTree() {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('JigHttpStatusCode', 'HtmlWebResponseObject', 'System.Object[]')]
	[CmdletBinding()]
	Param (
		# Func: GetProjectRepoTree
		# Func: GetProjectRepoCommits
		# Func: FindProjectRepoFile
		# e.g.
		# 	jig-opensource/source-code
		# 	jig-opensource/source-code/scripts
		[GitLabUri]$oGitLabRef,

		# Func: GetProjectRepoTree
		[Switch]$Recursive,

		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults,

		# Kann immer genützt werden
		[String]$AccessToken
	)

	$Header = @{}
	# https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
	If (-Not([String]::IsNullOrWhiteSpace($AccessToken))) {
		$Header.Add('PRIVATE-TOKEN', $AccessToken)
	}

	$WebRequestSplat = @{
		Method      = 'Get'
		ContentType = 'application/json; charset=utf-8'
	}

	# Listet alle Files eines Projekts

	# !M https://docs.gitlab.com/ee/api/repositories.html
	# 		/projects/:id/repository/tree

	# jig-opensource/source-code/scripts
	# jig-opensource%2Fsource-code%2Fscripts
	# https://gitlab.com/api/v4/projects/jig-opensource%2Fsource-code%2Fscripts/repository/tree

	$SrchMode = [eSrchMode]::KeysetBased

	$WebRequestSplat += @{
		Uri	= "https://gitlab.com/api/v4/projects/$($oGitLabRef.GetUriPart_UsrPrjGrpInfoEncoded())/repository/tree"
	}
	Switch ($SrchMode) {
				([eSrchMode]::PageBased) {
			$Body = @{
				page      = 1
				recursive = $Recursive
			}
		}
				([eSrchMode]::KeysetBased) {
			$Body = @{
				## Für Keyset-based pagination zwingend
				pagination	= 'keyset'
				## !9 Sortierung wird *nicht* für jeden GitLab Datentyp unterstützt!
				# order_by   = 'id'
				# sort			= 'asc' # asc or desc

				## Für die Funktion
				recursive  = $Recursive
			}
		}
	}

	# REST API call
	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI -Body $Body -Header $Header `
																		-WebRequestSplat $WebRequestSplat -SrchMode $SrchMode `
																		-MaxResults $MaxResults
	# GitHub Restultat-Typ ergänzen
	$ResHttpStatusCode, $ResWebRequest, $ResJson | ? { $_ -ne $Null } | % { $_ | Add-Member -MemberType NoteProperty -Name 'GitLabObjType' -Value 'File' }

	# Wenn alles OK
	If ($ResHttpStatusCode.Value -eq [Int][Net.HttpStatusCode]::OK) {
		# UTF8 bugs in PowerShell5
		# 	Zeichen werden falsch in Stirngs gespeichert,
		#	Anstatt · z.B. Â·
		#
		# Problematik & Liste: https://www.i18nqa.com/debug/utf8-debug.html

		# In Dateinamen / Pfaden die falschen UTF8 Chars flicken
		If ($PSVersionTable.PSVersion.Major -eq 5) {
			$ResJson | % {
				$_.name = ConvertFrom-Misinterpreted-Utf8 $_.name
				$_.path = ConvertFrom-Misinterpreted-Utf8 $_.path
			}
		}
	}

	Return @($ResHttpStatusCode, $ResWebRequest, $ResJson)
}



# GetProjectRepoCommits
# 	Suche nach den Commits in einem Projekt
# 	Returns: $ResHttpStatusCode, $ResWebRequest, $ResJson =
Function Call-GitLab-RestApi-GetProjectRepoCommits() {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('JigHttpStatusCode', 'HtmlWebResponseObject', 'System.Object[]')]
	[CmdletBinding()]
	Param (
		# e.g.
		# 	jig-opensource/source-code
		# 	jig-opensource/source-code/scripts
		[Parameter(Mandatory)]
		[GitLabUri]$oGitLabRef,

		# e.g. '!H jig-Opensource - scripts · GitLab.url'
		[Parameter(Mandatory)]
		[String]$FilePath,

		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults,

		# Kann immer genützt werden
		[String]$AccessToken
	)

	$Header = @{}
	# https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
	If (-Not([String]::IsNullOrWhiteSpace($AccessToken))) {
		$Header.Add('PRIVATE-TOKEN', $AccessToken)
	}

	$WebRequestSplat = @{
		Method      = 'Get'
		ContentType = 'application/json; charset=utf-8'
	}

	# !M https://docs.gitlab.com/ee/api/commits.html
	# 		GET /projects/:id/repository/commits

	# Alle Commits
	# https://gitlab.com/api/v4/projects/38296218/repository/commits
	# Commits einer Datei
	# https://gitlab.com/api/v4/projects/38296218/repository/commits?path=Tom-Chocolatey-Tools.ps1
	#
	#
	#
	# jig-opensource/source-code/scripts
	# jig-opensource%2Fsource-code%2Fscripts
	# https://gitlab.com/api/v4/projects/jig-opensource%2Fsource-code%2Fscripts/repository/tree

	$SrchMode = [eSrchMode]::KeysetBased

	$WebRequestSplat += @{
		Uri	= "https://gitlab.com/api/v4/projects/$($oGitLabRef.GetUriPart_UsrPrjGrpInfoEncoded())/repository/commits"
	}
	Switch ($SrchMode) {
		([eSrchMode]::PageBased) {
			$Body = @{
				page = 1
			}
			If (-Not([String]::IsNullOrWhiteSpace($FilePath))) {
				$Body += @{ path	= $FilePath }
			}
		}
		([eSrchMode]::KeysetBased) {
			$Body = @{
				## Für Keyset-based pagination zwingend
				pagination	= 'keyset'
				## !9 Sortierung wird *nicht* für jeden GitLab Datentyp unterstützt!
				# order_by  = 'id'
				# order_by  = 'committed_date'
				# sort		= 'desc' # asc or desc
			}
			## Für die Funktion
			If (-Not([String]::IsNullOrWhiteSpace($FilePath))) {
				$Body += @{ path	= $FilePath }
			}
		}
	}

	# REST API call
	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI -Body $Body -Header $Header `
																			-WebRequestSplat $WebRequestSplat -SrchMode $SrchMode `
																			-MaxResults $MaxResults
	$ResJson | ? { $_ -ne $Null } | % {
		$_ | Add-Member -MemberType NoteProperty -Name CommitedLocalTime -Value ((Get-Date $_.committed_date).ToLocalTime())
	}

	# GitHub Restultat-Typ ergänzen
	$ResHttpStatusCode, $ResWebRequest, $ResJson | ? { $_ -ne $Null } | % { $_ | Add-Member -MemberType NoteProperty -Name 'GitLabObjType' -Value 'Commit' }
	Return @($ResHttpStatusCode, $ResWebRequest, $ResJson)
}



# GetProjectRepoBranches
# 	Suche nach den Branches eines Repositories
# 	Returns: $Branches =
Function Call-GitLab-RestApi-GetProjectRepoBranches() {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('JigHttpStatusCode', 'HtmlWebResponseObject', 'System.Object[]')]
	[CmdletBinding()]
	Param (
		# e.g.
		# 	jig-opensource/source-code
		# 	jig-opensource/source-code/scripts
		[GitLabUri]$oGitLabRef,

		# Optional: Begrenzt die Resultate
		[Nullable[Int]]$MaxResults,

		# Kann immer genützt werden
		[String]$AccessToken
	)

	$Header = @{}
	# https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
	If (-Not([String]::IsNullOrWhiteSpace($AccessToken))) {
		$Header.Add('PRIVATE-TOKEN', $AccessToken)
	}

	$WebRequestSplat = @{
		Method      = 'Get'
		ContentType = 'application/json; charset=utf-8'
	}

	# !M https://docs.gitlab.com/ee/api/branches.html
	# 		GET /projects/:id/repository/branches

	# Alle Branches
	# https://gitlab.com/api/v4/projects/jig-opensource%2Fsource-code%2Fscripts/repository/branches
	$SrchMode = [eSrchMode]::KeysetBased

	$WebRequestSplat += @{
		Uri	= "https://gitlab.com/api/v4/projects/$($oGitLabRef.GetUriPart_UsrPrjGrpInfoEncoded())/repository/branches"
	}
	Switch ($SrchMode) {
				([eSrchMode]::PageBased) {
			$Body = @{ page = 1 }
		}
				([eSrchMode]::KeysetBased) {
			$Body = @{
				## Für Keyset-based pagination zwingend
				pagination	= 'keyset'
				## !9 Sortierung wird *nicht* für jeden GitLab Datentyp unterstützt!
				# order_by  = 'id'
				# order_by  = 'committed_date'
				# sort		= 'desc' # asc or desc
			}
		}
	}

	# REST API call
	$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestAPI -Body $Body -Header $Header `
																		-WebRequestSplat $WebRequestSplat -SrchMode $SrchMode `
																		-MaxResults $MaxResults
	# GitHub Restultat-Typ ergänzen
	$ResHttpStatusCode, $ResWebRequest, $ResJson | ? { $_ -ne $Null } | % { $_ | Add-Member -MemberType NoteProperty -Name 'GitLabObjType' -Value 'Branch' }
	# Nur die Branches zurückgeben
	Return , $ResJson
}


#EndRegion GitLab REST API Calls


# Liefert True, wenn ein Element null oder leer ist
Function Is-AnyNullOrWhiteSpace() {
	[CmdletBinding()]
	Param ([String[]]$Items)
	$Items | % {
		If ([String]::IsNullOrWhiteSpace($_)) {
			Return $True
		}
	}
	Return $False
}

# Liefert True, wenn alle Elemente Daten haben
Function All-HasValue() {
	[CmdletBinding()]
	Param ([String[]]$Items)
	Return (-not(Is-AnyNullOrWhiteSpace $Items))
}


#Region GitLab Links
##Region GitLab Links



# Erzeugt ein temporäres Verzeichnis
# 220810
Function Get-TmpDir {
	Param([String] $root = "$env:temp")
	$TempDir =  $(get-date -f 'yyyyMMdd-HHmmss')
	MD "$root\$TempDir" | Out-Null
	Return "$root\$TempDir"
}


# untar, unzip
Function UnPack-File($ZipFile, $ZielDirAbs) {
	# tar kann nicht mit '/ am Ende umgehen
	$ZielDirAbs = $ZielDirAbs.TrimEnd('\\/')
	$Null = New-Item -Path $ZielDirAbs -ItemType Directory -Force -EA SilentlyContinue
	tar -xf $ZipFile -C $ZielDirAbs
}



Enum eUrlType { ViewOnlineLink; DownloadRaw }

# Aus dem Link eines GitLab-Commits
# 	kann der Link zur Anzeige oder zum Download der Raw-Datei angeleitet werden
#
# Dieses Script generiert aus einer Commit-URL exakt diese Links
#  für eine gewünschte Datei
#
#
# Commit URL
# https://gitlab.com/jig-opensource/source-code/scripts/-/commit/0024042c6fad3146849ece0f1d149d25497680d3
#
# File View online
# https://gitlab.com/jig-opensource/source-code/scripts/-/blob/0024042c6fad3146849ece0f1d149d25497680d3/Tom-Chocolatey-Tools.ps1
#
# Download Raw
# https://gitlab.com/jig-opensource/source-code/scripts/-/raw/0024042c6fad3146849ece0f1d149d25497680d3/Tom-Chocolatey-Tools.ps1?inline=false
Function Get-GitLabCommit-Link() {
	[CmdletBinding()]
	Param ($CommitUrl, [String]$FilePath, [eUrlType]$GetLinkType)


	# Dekodierung
	#
	# ┌────────────────  $ItemProjectUrl  ────────────────┐   ┌─────  $ItemCommit ───────┐
	# │                                                   │   │                          │
	# https://gitlab.com/jig-opensource/source-code/scripts/-/commit/0024042c6fad31…6180d3
	#                                                         │     │                    │
	#                                                         │     └───────┐            │
	#                                                         │ $CommitKey: │ $CommitId  │
	#                                                         │  - commit
	#                                                         │  - blob
	#                                                         │  - raw


	$ItemProjectUrl, $ItemCommit = $CommitUrl -split '/-/|\\-\\'
	# $ItemProjectUrl
	# 	https://gitlab.com/jig-opensource/source-code/scripts
	#
	# $ItemCommit
	# 	commit/0024042c6fad3146849ece0f1d149d25497680d3

	$CommitKey, $CommitId = $ItemCommit -split '/|\\'
	# $CommitKey
	#	commit
	# $CommitId
	#	0024042c6fad3146849ece0f1d149d25497680d3

	If (All-HasValue @($ItemProjectUrl, $CommitId, $FilePath)) {
		$FilePathEncoded = ($FilePath.Split('/') | % { [URI]::EscapeDataString($_) }) -join '/'
		Switch ($GetLinkType) {
			([eUrlType]::ViewOnlineLink) {
				# Der Link, um die Datei online zu betrachten
				Return ('{0}/-/blob/{1}/{2}' -f $ItemProjectUrl, $CommitId, $FilePathEncoded)
			}
			([eUrlType]::DownloadRaw) {
				# Der Link, um die Datei als ray herunterzuladen
				Return ('{0}/-/raw/{1}/{2}?inline=false' -f $ItemProjectUrl, $CommitId, $FilePathEncoded)
			}
			Default {
				Write-Error 'Get-GitLabCommit-Link(): Unbekannter Link-Typ'
			}
		}
	} Else {
		Write-Error 'Get-GitLabCommit-Link(): Decoder-Fehler'
	}
}


# Berechnet den Download-Link einer Datei für die neuste Version
Function Get-NewestRelease-Link() {
	[CmdletBinding()]
	Param (
		[GitLabUri]$oGitLabRef,
		[String]$FilePath,
		[eUrlType]$GetLinkType
	)

	# Dekodierung
	# 	Open Raw
	#
	#
	#          ┌────────────────  $ItemProjectUrl  ────────┐            ┌──────  [URI]::EscapeDataString($FilePath) ───────┐
	#          │                                           │            │                                                  │
	#	https://gitlab.com/jig-opensource/source-code/scripts/-/raw/main/PowerShell/Gists/!H%20Schittli%E2%80%99s%20gists.url
	#                                                          │   └─ Branch
	#                                                          └─ Keybword

	## Config
	$DlSuffix = '?inline=false'

	## Prepare
	# https://gitlab.com/jig-opensource/source-code/scripts/-/
	$ProjectUrl = $oGitLabRef.GetUri()
	$MainName = Get-GitLab-Main-SingleBanch $oGitLabRef

	# !9 Vorsicht!,
	# 		Besser Download als Zip nützen!
	# 		Das PowerShell 5.x Problem mit Invoke-WebRequest und Sonderzeichen in UTF8 ist gelöst
	# 		» ConvertFrom-Misinterpreted-Utf8
	#		Aber die vielen einzelnen REST API Aufrufe sind langsam
	$FilePathEncoded = ($FilePath.Split('/') | % { [URI]::EscapeDataString($_) }) -join '/'
	$ViewUrl = Join-Parts $ProjectUrl, 'raw', $MainName, $FilePathEncoded

	Switch ($GetLinkType) {
		([eUrlType]::ViewOnlineLink) {
			Return $ViewUrl
		}
		([eUrlType]::DownloadRaw) {
			# Der Link, um die Datei als raw herunterzuladen
			Return ('{0}{1}' -f $ViewUrl, $DlSuffix)
		}
		Default {
			Write-Error 'Get-NewestRelease-Link(): Unbekannter Link-Typ'
		}
	}
}

#EndRegion GitLab Links



#Region GitLab Zip

# Holt den Namen des Main / Master Branches
# WWenn mehrere vorhanden sind, wird getestet, ob einer der Üblichen Branches existiert
Function Get-GitLab-Main-SingleBanch() {
	[CmdletBinding()]
	Param ([GitLabUri]$oGitLabRef)

	# Config
	$TypicalMainBranches = @('master', 'main')

	# Die Branchnamen suchen
	$AllBranches = Call-GitLab-RestApi-GetProjectRepoBranches -oGitLabRef $oGitLabRef

	Switch($AllBranches.Count) {
		0 { Write-Error "Keine Branches gefunden!"; Return $Null; }
		1 { Return $AllBranches[0].name }
		Default {
			$Candidates = @($AllBranches | ? { $TypicalMainBranches -contains $_ })
			If ($Candidates.Count -eq 1) {
				Return $Candidates[0].name
			} Else {
				Write-Host 'Mehrere Branches gefunden:' -ForegroundColor Red
				$AllBranches | % {
					Write-Host ('  {0}' -f $_.name)
				}
				Return $Null
			}
		}
	}
}


## Liefert den Link, um das Repo als Zip herunterzuladen
Function Get-GitLabCommit-ZipLink() {
	[CmdletBinding()]
	Param ([GitLabUri]$oGitLabRef)

	# Die Branchnamen suchen
	$AllBranches = Call-GitLab-RestApi-GetProjectRepoBranches -oGitLabRef $oGitLabRef

	Switch($AllBranches.Count) {
		0 {
			Write-Error "Keine Branches gefunden: "
		}
		1 {
			# https://gitlab.com/jig-opensource/source-code/scripts/-/archive/main/scripts-main.zip
			# https://gitlab.com/jig-opensource/source-code/scripts/-/archive/main/scripts-main.zip
			$BranchName = $AllBranches[0].name
			$ProjectName = [IO.Path]::GetFileName( $oGitLabRef.UriPart_UsrPrjGrpInfo )
			Return Join-Parts ($oGitLabRef.GetUri()), 'archive', $BranchName, ('{0}-{1}.zip' -f $ProjectName, $BranchName)
		}
		Default {
			Write-Host 'Mehrere Branches gefunden:' -ForegroundColor Red
			$AllBranches | % {
				Write-Host ('  {0}' -f $_.name)
			}
		}
	}
}


# Lädt vom Gitlab Projekt Repo das zip herunter
# Entpackt es in ein temp dir
# und liefert das erste Verzeichnis zurück, das beim Entpacken erzeugt wurde,
# also am folgenden Beispiel: c:\…\Temp-Dir\scripts-main\
#
# c:\…\Temp-Dir\tester.zip
# Entpackt:
# 	c:\…\Temp-Dir\scripts-main\
# 	c:\…\Temp-Dir\scripts-main\README.md
#
#
# $DstTempDir, $oUnpackedContent1stDir = DownloadAndUnpack-Gitlab-Zip
Function DownloadAndUnpack-Gitlab-Zip() {
	[OutputType('System.Object[]')]
	[CmdletBinding()]
	Param ([GitLabUri]$oGitLabRef)

	$DstTempDir = Get-TmpDir
	$DstTempZipFileName = '{0}\{1}.zip' -f $DstTempDir, $(get-date -f 'yyyyMMdd-HHmmss')
	$DownloadLink = Get-GitLabCommit-ZipLink $oGitLabRef
	# Download
	WGet $DownloadLink -OutFile $DstTempZipFileName -Verbose:$False
	# Entpacken
	UnPack-File $DstTempZipFileName $DstTempDir
	# Zip wieder löschen
	Start-Sleep -MilliS 750
	Remove-Item -LiteralPath $DstTempZipFileName -Force -EA SilentlyContinue
	# das erste Verzeichnis, das beim Entpacken erzeugt wurde
	$oUnpackedContent1stDir = Get-ChildItem -Path $DstTempDir -Directory | select -First 1
	Return @($DstTempDir, $oUnpackedContent1stDir.FullName)
}

#EndRegion GitLab Zip



# Definiert die Standard-Anzeige für ein Objekt
function SetDisplaySet ($Obj, $DisplaySetName, $DefaultProperties) {

	# Give this object a unique typename
	$Obj.PSObject.TypeNames.Insert(0, $DisplaySetName)
	# $Cmd = "$Obj.PSObject.TypeNames.Insert(0, $DisplaySetName)"
	# Invoke-Expression $Cmd

	# Configure a default display set
	$defaultDisplaySet = $DefaultProperties
	# Create the default property display set
	$defaultDisplayPropertySet = New-Object System.Management.Automation.PSPropertySet('DefaultDisplayPropertySet', [string[]]$defaultDisplaySet)
	$PSStandardMembers = [System.Management.Automation.PSMemberInfo[]]@($defaultDisplayPropertySet)
	$Obj | Add-Member MemberSet PSStandardMembers $PSStandardMembers
}


# Sucht in einem Projekt nach dem letzten Commit einer Datei
Function Get-ProjectRepo-LastCommit() {
	[OutputType('System.Object[]')]
	[CmdletBinding()]
	Param(
		# 	jig-opensource/source-code
		# 	jig-opensource/source-code/scripts
		[Parameter(Mandatory)]
		[GitLabUri]$oGitLabRef,
		# Kann Dateimuster, Dateinamen oder auch Pfad\Dateiname erhalten
		[String[]]$FileNameFilter,
		[Switch]$Recursive,
		[Nullable[Int]]$MaxResults
	)
	# Alle Files suchen um den exakten Pfad zu finden
	$ResHttpStatusCode, $ResWebRequest, $ResJsonFiles = Call-GitLab-RestApi-GetProjectRepoTree -oGitLabRef $oGitLabRef `
																			-Recursive:$Recursive -MaxResults $MaxResults

	# Die File-Liste filtern?
	$MyFiles = @()
	If ($FileNameFilter) {
		ForEach ($Filter in $FileNameFilter) {
			$MyFiles += @($ResJsonFiles | ? path -like $Filter)
		}
	} Else {
		$MyFiles = $ResJsonFiles
	}

	If ($MyFiles.Count -eq 0) {
		Write-Verbose "GitLab File Download: Keine Files gefunden" -ForegroundColor Red
		Return $null
	} Else {
		$Res = @()
		$Cnt = 0; $CntMax = ($MyFiles | ? type -eq blob).Count
		# Nur die Dateien (blob), keine Verzeichnisse (tree)
		$MyFiles | ? type -eq blob | % {
			$ThisFile = $_
			$Cnt++; Write-Progress -Activity "Search in Progress" -Status "$Cnt% Complete:" -PercentComplete ($Cnt / $CntMax * 100)
			$ResHttpStatusCode, $ResWebRequest, $ResJsonCommits = Call-GitLab-RestApi-GetProjectRepoCommits -oGitLabRef $oGitLabRef `
																							-FilePath $ThisFile.path -MaxResults $MaxResults

			If ($ResJsonCommits.Count -gt 0) {
				# Das neuste Commit wählen
				$NewestCommit = $ResJsonCommits | Sort CommitedLocalTime -Descending | Select -First 1

				# Dem Commit File Name & Path zufügen
				$NewestCommit | Add-Member -MemberType NoteProperty -Name 'FileName' -Value $ThisFile.name
				$NewestCommit | Add-Member -MemberType NoteProperty -Name 'FilePath' -Value $ThisFile.path
				# Dem Commit den DL Link zufügen
				Add-Member -InputObject $NewestCommit -MemberType NoteProperty -Name 'DLLink' `
					-Value (Get-NewestRelease-Link -oGitLabRef $oGitLabRef `
															-FilePath $ThisFile.path -GetLinkType DownloadRaw)

				SetDisplaySet -Obj $NewestCommit -DisplaySetName 'Tom.GitLab.NewestCommit'  `
					-DefaultProperties @('GitLabObjType', 'CommitedLocalTime', 'committer_name', 'FileName', 'FilePath', 'DLLink')

				$Res += $NewestCommit
			} Else {
				Write-Host "Kein Commit gefunden für:`n$($ThisFile.path)" -ForegroundColor Red
			}
		}
		Return $Res
	}
}



#Region Local File update
## Region Local File update



# Lädt die Files in $Filenames herunter
#
# !9 	Lädt das GitLab Projekt als Zip herunter
# 		und verteilt dann fehlende / veraltete Files zum Ziel
#
# Wenn die lokalen Files bereits aktuell sind, wird nichts heruntergeladen
# -Force	Lädt bereits aktuelle Files trotzde, herunter
# -Verbose	Infos werden angezeigt
# Return
#	Anzahl der aktualisierten Files
Function Download-GitLab-Files-AsZip() {
	# Suppress PSScriptAnalyzer Warning
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('System.Int32')]
	[CmdletBinding()]
	Param(
		# 	jig-opensource/source-code
		# 	jig-opensource/source-code/scripts
		[Parameter(Mandatory)]
		[GitLabUri]$oGitLabRef,
		# Kann Dateimuster, Dateinamen oder auch Pfad\Dateiname erhalten
		[String[]]$FileNameFilter,
		[String]$ZielDir,
		[Switch]$Recursive,
		[Nullable[Int]]$MaxResults,
		[Switch]$Force
	)

	$ResFilesUpdated = 0

	## Dateiliste mit den letzten Commits / Versions-Info holen
	# Alle Files suchen um den exakten Pfad zu finden
	$GitlabFilesInfo = Get-ProjectRepo-LastCommit -oGitLabRef $oGitLabRef -FileNameFilter $FileNameFilter -Recursive:$Recursive -MaxResults $MaxResults

	If ($GitlabFilesInfo.Count -eq 0) {
		Write-Verbose "GitLab File Download: Keine Files gefunden" -ForegroundColor Red
	} Else {
		Write-Verbose "GitLab Files Download:" -ForegroundColor Yellow

		# Das Verzeichnis, in dem das Gitlab zip entpackt wurde
		$DstTempDir, $SrcUnpackedZipTempDir = $null

		$Cnt = 0; $CntMax = $GitlabFilesInfo.Count
		ForEach ($GitlabFileInfo in $GitlabFilesInfo) {
			$Cnt++; Write-Progress -Activity "Download in Progress" -Status "$Cnt% Complete:" -PercentComplete ($Cnt / $CntMax * 100)
			Write-Verbose "  $($GitlabFileInfo.FilePath)" -ForegroundColor Magenta -NoNewline
			$DstLocalFileName = Join-Path $ZielDir $GitlabFileInfo.FilePath

			# Existiert das lokale File und ist es aktuell?
			$oDstLocalFileName = Get-Item -LiteralPath $DstLocalFileName -EA SilentlyContinue
			If ($Force -eq $True -or `
					$Null -eq $oDstLocalFileName `
					-or $GitlabFileInfo.CommitedLocalTime -gt $oDstLocalFileName.LastWriteTime) {

				# Sicherstellen, dass das Verzeichnis existiert
				New-Item -Path ([IO.Path]::GetDirectoryName( $DstLocalFileName )) -ItemType Directory -EA SilentlyContinue | Out-Null

				# Zip allenfalls herunterladen
				If ($null -eq $SrcUnpackedZipTempDir) {
					$DstTempDir, $SrcUnpackedZipTempDir = DownloadAndUnpack-Gitlab-Zip $oGitLabRef
				}

				# Die Zieldatei löschen
				If (Test-Path -LiteralPath $DstLocalFileName -PathType Leaf) {
					Remove-Item -LiteralPath $DstLocalFileName -Force -EA Stop
				}

				# Die Quelldatei bestimmen: Download-Pfad + Relativer GitLab FilePath
				$SrcLocalFileName = Join-Path $SrcUnpackedZipTempDir $GitlabFileInfo.FilePath
				$oSrcLocalFileName = Get-Item -LiteralPath $SrcLocalFileName -EA SilentlyContinue

				# Vom entpackten Zip die Datei ans Ziel verschieben
				$ResFilesUpdated++
				$oSrcLocalFileName.MoveTo($DstLocalFileName)

				# Wenn die Datei vorher nicht existierte, wurde sie heruntergeladen
				If ($Null -eq $oDstLocalFileName) {
					Write-Verbose "   > Heruntergeladen" -ForegroundColor Red
				}
				Else {
					Write-Verbose "   > Aktualisiert" -ForegroundColor Red
				}
				# Das lokale Änderungsdatum der Datei dem Gitlbab angleichen
				$oDstLocalFileName = Get-Item -LiteralPath $DstLocalFileName
				$oDstLocalFileName.LastWriteTime = $GitlabFileInfo.CommitedLocalTime
			}
			Else {
				Write-Verbose "   > Bereits aktuell" -ForegroundColor Green
			}
		}

		# Aufräumen
		If ($DstTempDir) { Remove-Item -LiteralPath $DstTempDir -Recurse -Force -EA SilentlyContinue }
	}

	Return $ResFilesUpdated
}


# Lädt die Files in $Filenames herunter
#
# !9 	Jede Datei wird einzeln heruntergeladen
#
# Wenn die lokalen Files bereits aktuell sind, wird nichts heruntergeladen
# -Force	Lädt bereits aktuelle Files trotzde, herunter
# -Verbose	Infos werden angezeigt
# Return
#	Anzahl der aktualisierten Files
Function Download-GitLab-Files-AsSingleFiles() {
	# Suppress PSScriptAnalyzer Warning
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('System.Int32')]
	[CmdletBinding()]
	Param(
		# 	jig-opensource/source-code
		# 	jig-opensource/source-code/scripts
		[Parameter(Mandatory)]
		[GitLabUri]$oGitLabRef,
		# Kann Dateimuster, Dateinamen oder auch Pfad\Dateiname erhalten
		[String[]]$FileNameFilter,
		[String]$ZielDir,
		[Switch]$Recursive,
		[Nullable[Int]]$MaxResults,
		[Switch]$Force
	)

	$ResFilesUpdated = 0

	## Dateiliste mit den letzten Commits / Versions-Info holen
	# Alle Files suchen um den exakten Pfad zu finden
	$GitlabFilesInfo = Get-ProjectRepo-LastCommit -oGitLabRef $oGitLabRef -FileNameFilter $FileNameFilter -Recursive:$Recursive -MaxResults $MaxResults

	If ($GitlabFilesInfo.Count -eq 0) {
		Write-Verbose "GitLab File Download: Keine Files gefunden" -ForegroundColor Red
	} Else {
		Write-Verbose "GitLab Files Download:" -ForegroundColor Yellow

		$Cnt = 0; $CntMax = $GitlabFilesInfo.Count
		ForEach ($GitlabFileInfo in $GitlabFilesInfo) {
			$Cnt++; Write-Progress -Activity "Download in Progress" -Status "$Cnt% Complete:" -PercentComplete ($Cnt / $CntMax * 100)

			Write-Verbose "  $($GitlabFileInfo.FilePath)" -ForegroundColor Magenta -NoNewline
			$DstLocalFileName = Join-Path $ZielDir $GitlabFileInfo.FilePath

			# Existiert das lokale File und ist es aktuell?
			$oDstLocalFileName = Get-Item -LiteralPath $DstLocalFileName -EA SilentlyContinue
			If ($Force -eq $True -or `
					$Null -eq $oDstLocalFileName `
					-or $GitlabFileInfo.CommitedLocalTime -gt $oDstLocalFileName.LastWriteTime) {

				# Sicherstellen, dass das Verzeichnis existiert
				New-Item -Path ([IO.Path]::GetDirectoryName( $DstLocalFileName )) -ItemType Directory -EA SilentlyContinue | Out-Null

				# Download
				$ResFilesUpdated++

				# !9 Vorsicht!,
				# 		Besser Download als Zip nützen!
				# 		Das PowerShell 5.x Problem mit Invoke-WebRequest und Sonderzeichen in UTF8 ist gelöst
				# 		» ConvertFrom-Misinterpreted-Utf8
				#		Aber die vielen einzelnen REST API Aufrufe sind langsam
				$Null = Invoke-WebRequest -Uri $GitlabFileInfo.DLLink -OutFile $DstLocalFileName -Verbose:$False
				# Wenn die Datei vorher nicht existierte, wurde sie heruntergeladen
				If ($Null -eq $oDstLocalFileName) {
					Write-Verbose "   > Heruntergeladen" -ForegroundColor Red
				}
				Else {
					Write-Verbose "   > Aktualisiert" -ForegroundColor Red
				}
				# Das lokale Änderungsdatum der Datei dem Gitlbab angleichen
				$oDstLocalFileName = Get-Item -LiteralPath $DstLocalFileName
				$oDstLocalFileName.LastWriteTime = $GitlabFileInfo.CommitedLocalTime
			} Else {
				Write-Verbose "   > Bereits aktuell" -ForegroundColor Green
			}
		}
	}

	Return $ResFilesUpdated
}


Function Download-GitLab-Files() {
	# Suppress PSScriptAnalyzer Warning
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseSingularNouns', '')]
	[OutputType('System.Int32')]
	[CmdletBinding()]
	Param(
		# 	jig-opensource/source-code
		# 	jig-opensource/source-code/scripts
		[Parameter(Mandatory)]
		[GitLabUri]$oGitLabRef,
		# Kann Dateimuster, Dateinamen oder auch Pfad\Dateiname erhalten
		[String[]]$FileNameFilter,
		[String]$ZielDir,
		[Switch]$Recursive,
		[Nullable[Int]]$MaxResults,
		[Switch]$Force,
		[Switch]$AsZip
	)

	$Splat = @{
		oGitLabRef = $oGitLabRef
		FileNameFilter = $FileNameFilter
		ZielDir = $ZielDir
		Recursive = $Recursive
		MaxResults = $MaxResults
		Force = $Force
	}

	If ($AsZip) {
		# Lädt das GitLab Projekt als Zip herunter
		# und verteilt dann fehlende / veraltete Files zum Ziel
		Return Download-GitLab-Files-AsZip @Splat
	} Else {
		# Lädt jedes fehlende / veraltete File einzeln herunter
		Return Download-GitLab-Files-AsSingleFiles @Splat
	}
}

#EndRegion Local File update



### Prepare

## URI aufbereiten
$oGitLabRef = [GitLabUri]::New($GitLabRef)



### Main

# Nur die Zusatzfunktionen
Switch ($PsCmdlet.ParameterSetName) {
	'IsUriGitLabGroup' {
		# Liefert $True, wenn eine Uri eine GitLab Gruppe ist
		$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestApi-GetGroupProjects -oGitLabRef $oGitLabRef
		Return $ResHttpStatusCode.Value -eq 200
	}

	'IsUriGitLabProject' {
		# Liefert $True, wenn eine Uri ein GitLab Projekt ist
		$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestApi-GetProjectRepoTree -oGitLabRef $oGitLabRef `
																			-Recursive:$False
		Return $ResHttpStatusCode.Value -eq 200
	}

	'FindProjectRepoFile' {
		# Sucht in einem Projekt nach einer Datei
		# Return $ResJson, die dem Suchmuster entsprechen
		$ResHttpStatusCode, $ResWebRequest, $ResJson = Call-GitLab-RestApi-GetProjectRepoTree -oGitLabRef $oGitLabRef `
			-Recursive:$Recursive `
			-MaxResults $MaxResults
		Return ($ResJson | ? path -like $FileNameSrchPtrn)
	}

	'GetProjectRepoLastCommit' {
		# Sucht in einem Projekt nach dem letzten Commit einer Datei
		Return Get-ProjectRepo-LastCommit -oGitLabRef $oGitLabRef -FileNameFilter $FileNameSrchPtrn -Recursive:$Recursive -MaxResults $MaxResults
	}

	'DownloadFiles' {
		# Das absolute Ziel-Verzeichnis berechnen
		$ZielDirAbs = Calc-ZielDir $ZielDir $MyWorkingDirectory

		# Files herunteraden, wenn sie lokal fehlen oder veraltet sind
		$Res = Download-GitLab-Files -oGitLabRef $oGitLabRef -FileNameFilter $FileNameSrchPtrn `
			-Recursive:$Recursive -MaxResults $MaxResults `
			-ZielDir $ZielDirAbs -AsZip:$DownloadGitLabFilesModeAsZip -Force:$Force
		Return $Res
	}

	'GetUserProjects' {
		Return Call-GitLab-RestApi-GetUserProjects -SrchUserName $SrchUserName -MaxResults $MaxResults
	}

	'GetGroups' {
		Return Call-GitLab-RestApi-GetGroups -GroupSrchNameOrPath $GroupSrchNameOrPath -MaxResults $MaxResults
	}

	'GetProjects' {
		Return Call-GitLab-RestApi-GetProjects -ProjectSrchName $ProjectSrchName -MaxResults $MaxResults
	}

	'GetUsers' {
		Return Call-GitLab-RestApi-GetUsers -UserSrchName $UserSrchName -MaxResults $MaxResults
	}

	'GetGroupProjects' {
		Return Call-GitLab-RestApi-GetGroupProjects -oGitLabRef $oGitLabRef -MaxResults $MaxResults
	}

	'GetProjectRepoTree' {
		Return Call-GitLab-RestApi-GetProjectRepoTree -oGitLabRef $oGitLabRef -Recursive:$Recursive -MaxResults $MaxResults
	}

	'GetProjectRepoCommits' {
		Return Call-GitLab-RestApi-GetProjectRepoCommits -oGitLabRef $oGitLabRef -FilePath $ForExactFileOrPath -MaxResults $MaxResults
	}

	'GetProjectRepoBranches' {
		Return Call-GitLab-RestApi-GetProjectRepoBranches -oGitLabRef $oGitLabRef -ForExactFileOrPath $ForExactFileOrPath -MaxResults $MaxResults
	}

}

