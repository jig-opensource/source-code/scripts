
# Idee

- PowerShell-Scripts und Dateien sollen:
  - einfach in GitLab versioniert werden können
  - es soll einfach möglich sein, 
		auszugsweise lokale Kopien zu erstellen
		- die lokalen Kopien zu aktualisieren
			d.h. die lokalen Dateien müssen den Modification Timestamp haben,
			der der letzten Änderung auf GitLab entspricht



# Probleme

## Bestimmen des Modifikations-Datum einer Datei

- Download des GitLab Projekts als zip
	Die Dateien haben den Modification Timestamp des letzten Commits
	- auch, wenn die Datei im Commit selber nicht verändert wurde :-(
	
- Gitlab REST API
	- Man kann *alle* commits abrufen, 
		hat aber keine Informationen zu den beim Commit betroffenen Dateien :-(
		
	- Man kann für jede Datei einzeln deren commits abrufen,
		aber es werden auch Commits aufgelistet, in denen die Datei gar nicht verändert wurde :-(

### Fazit
Das Modifikations-Datum einer Datei ist im Web zwar ersichtlich,
	aber es scheint nicht abgerufen werden zu können.


# Status

- Supportanfrage bei GitLab eröffnet



